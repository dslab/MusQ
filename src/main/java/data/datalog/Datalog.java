package data.datalog;

import java.util.List;
import java.util.Map;

public class Datalog {
	private Head head;
	private Body body;
	
	public Head getHead() {
		return head;
	}

	public void setHead(String table, List<String> columns, Map<String, String> aggregation) {
		Head head = new Head(table, columns, aggregation);
		this.head = head;
	}
	
	public Body getBody() {
		return body;
	}
	
	public void setBody(List<Predicate> predicates, List<FilterPredicate> filters) {
		Body body = new Body(predicates, filters);		
		this.body = body;
	}
	
	public String toString(){
		String s = "";
		
		s += getHead().toString() + ":- \n";
		s += getBody().toString();
		return s;
	}
	
}

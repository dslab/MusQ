package data.datalog;

public class FilterPredicate {
	private String column;
	private String operator;
	private String condition;
	
	public FilterPredicate(String column, String operator, String condition){
		this.column = column;
		this.operator = operator;
		this.condition = condition;
	}
	
	public String getColumn() {
		return column;
	}
	
	public void setColumn(String column) {
		this.column = column;
	}
	
	public String getOperator() {
		return operator;
	}
	
	public void setOperator(String operator) {
		this.operator = operator;
	}
	
	public String getCondition() {
		return condition;
	}
	
	public void SetCondition(String condition) {
		this.condition = condition;
	}
	
	public String toString(){
		return (column.concat(" ").concat(operator).concat(" ").concat(condition));
	}
}

package data.datalog;

import java.util.List;

public class Predicate {
	private String table;
	private List<String> columns;
	
	public Predicate(){}
	
	public Predicate(String table, List<String> columns) {
		this.table = table;
		this.columns = columns;
	}

	public String getTable() {
		return table;
	}
	
	public void setTable(String table) {
		this.table = table;
	}
	
	public List<String> getColumns() {
		return columns;
	}
	
	public void setColumns(List<String> columns) {
		this.columns = columns;
	}
	
	public String toString(){
		return (table.concat("(").concat(String.join(",", columns)).concat(")"));
	}
}

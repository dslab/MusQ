package data.datalog;

import java.util.List;

public class Body {
//	private String bodyStr;
	private List<Predicate> predicates;
	private List<FilterPredicate> filters;
	
	public Body(){}
	public Body(List<Predicate> predicates, List<FilterPredicate> filters){
//		this.bodyStr = bodyStr;
		this.predicates = predicates;
		this.filters = filters;
	}
	
//	public String getBodyStr() {
//		return bodyStr;
//	}
//	
//	public void setBodyStr(String bodyStr) {
//		this.bodyStr = bodyStr;
//	}
	
	public List<Predicate> getPredicate() {
		return predicates;
	}
	
	public void setPredicate(List<Predicate> predicates) {
		this.predicates = predicates;
	}
	
	public List<FilterPredicate> getFilters() {
		return filters;
	}
	
	public void setFilters(List<FilterPredicate> filters) {
		this.filters = filters;
	}
	
	public String toString(){
		String s = "";
		
		for(Predicate pred: predicates){
			s += pred.toString() + "\n";
		}
		
		for(FilterPredicate fpred: filters){
			s += fpred.toString() + "\n";
		}
		
		return s;
	}
}

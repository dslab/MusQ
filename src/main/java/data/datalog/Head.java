package data.datalog;

import java.util.List;
import java.util.Map;

public class Head extends Predicate{
//	private String headStr;
	private Map<String, String> aggregation;
	
	public Head(String table, List<String> columns, Map<String, String> aggregation){		
		super(table, columns);
		this.aggregation =  aggregation;
//		this.headStr = headStr;
	}

	public Head() {	}

	public Map<String, String> getAggregation() {
		return aggregation;
	}

	public void setAggregation(Map<String, String> aggregation) {
		this.aggregation = aggregation;
	}

//	public String getHeadStr() {
//		return headStr;
//	}
//
//	public void setHeadStr(String headStr) {
//		this.headStr = headStr;
//	}
}
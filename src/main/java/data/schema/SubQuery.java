package data.schema;

import java.util.List;

import data.datalog.FilterPredicate;

public class SubQuery {
	private Schema view;
	private Schema source;
	private List<FilterPredicate> filter;
	
	public Schema getView() {
		return view;
	}
	
	public void setView(Schema view) {
		this.view = view;
	}

	public Schema getSource() {
		return source;
	}

	public void setSource(Schema source) {
		this.source = source;
	}
	
	public List<FilterPredicate> getFilter() {
		return filter;
	}

	public void setFilter(List<FilterPredicate> filter) {
		this.filter = filter;
	}
	
	public String toString(){
		String s = "";
		s += source.toString() + "\n";
		
		if(filter != null){
			for(FilterPredicate f : filter){
				s += f.toString() + "\n";
			}
		}
		
		return view.toString().concat(":-\n").concat(s);
	}
}

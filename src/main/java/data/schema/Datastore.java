package data.schema;

public final class Datastore {
	public static final String MySQL = "MySQL";
	public static final String CASSANDRA = "Cassandra";
	public static final String DFS = "HDFS";
	public static final String Mongo = "MongoDB";
	
	private String ID;
	private String address;
	private String dbName;
	private String database;
	private String user;
	private String pass;
	
	public String getID() {
		return ID;
	}
	
	public void setID(String ID) {
		this.ID = ID;
	}
	
	public String getAddress() {
		return address;
	}
	
	public void setAddress(String address) {
		this.address = address;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}

	public String getDatabase() {
		return database;
	}

	public void setDatabase(String database) {
		this.database = database;
	}
	

	public String getUser() {
		return user;
	}

	public void setUser(String user) {
		this.user = user;
	}

	public String getPass() {
		return pass;
	}

	public void setPass(String pass) {
		this.pass = pass;
	}
	
	public String toString(){
		return ID.concat(" ").concat(address).concat(" ").concat(dbName).concat(" ").concat(database);
	}
}

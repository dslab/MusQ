package data.schema;

import java.util.List;
import java.util.Map;

public class GlobalSchema {
	private Schema view;
	private List<Schema> relations;
	private Matches match;
	private List<Matches> matches;
	private Map<String, List<String>> allMatches;
	
	public Schema getView() {
		return view;
	}
	
	public void setView(Schema view) {
		this.view = view;
	}
	
	public List<Schema> getRelations() {
		return relations;
	}
	
	public void setRelations(List<Schema> relations) {
		this.relations = relations;
	}

	public Matches getMatch() {
		return match;
	}

	public void setMatch(Matches match) {
		this.match = match;
	}	

	public List<Matches> getMatches() {
		return matches;
	}

	public void setMatches(List<Matches> matches) {
		this.matches = matches;
	}	

	public Map<String, List<String>> getAllMatches() {
		return allMatches;
	}

	public void setAllMatches(Map<String, List<String>> allMatches) {
		this.allMatches = allMatches;
	}
	
	public String toString(){
		String s = "";
		
		s += view.toString() + ":-\n";
		
		for(Schema relation: relations){
			s += "\t" + relation.toString() + ",\n";
		}
		
		for(Matches m : matches){
			s += "\t" + m.toString() + "\n";
		}
		
		
		
		return s;		
	}
}

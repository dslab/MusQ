package data.schema;

import java.util.List;

public class DatastoreConfig {
	private List<Datastore> dataStore;

	public List<Datastore> getDataStore() {
		return dataStore;
	}

	public void setDataStore(List<Datastore> dataStore) {
		this.dataStore = dataStore;
	}
	
	public String toString(){
		return dataStore.toString();
	}
}

package data.schema;

import java.util.List;

public class Schema {
	private String table;
	private List<String> columns;
	private String datastore;
	
	public String getTable() {
		return table;
	}
	
	public void setTable(String table) {
		this.table = table;
	}
	
	public List<String> getColumns() {
		return columns;
	}
	
	public void setColumns(List<String> columns) {
		this.columns = columns;
	}

	public String getDatastore() {
		return datastore;
	}

	public void setDatastore(String datastore) {
		this.datastore = datastore;
	}
	
	public String toString(){
		if(datastore != null)
			return table.concat("(").concat(String.join(",", columns)).concat(")@").concat(datastore);
		else
			return table.concat("(").concat(String.join(",", columns)).concat(")");
	}
}

package data.schema;

import java.util.List;

import data.datalog.FilterPredicate;

public class Query {
	private Schema view;
	private List<Schema> source;
	private List<FilterPredicate> filter;
	
	public Schema getView() {
		return view;
	}
	
	public void setView(Schema view) {
		this.view = view;
	}

	public List<Schema> getSource() {
		return source;
	}

	public void setSource(List<Schema> source) {
		this.source = source;
	}
	
	public List<FilterPredicate> getFilter() {
		return filter;
	}

	public void setFilter(List<FilterPredicate> filter) {
		this.filter = filter;
	}
	
	public String toString(){
		String s = "";
		for(Schema sc: source){
			s += sc.toString() + "\n";
		}
		
		for(FilterPredicate f : filter){
			s += f.toString() + "\n";
		}
		
		return view.toString().concat(":-\n").concat(s);
	}
	
}

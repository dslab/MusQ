package data.schema;

public class Matches {
	private String table1;
	private String col1;
	private String table2;
	private String col2;
	
	public String getTable1() {
		return table1;
	}
	
	public void setTable1(String table1) {
		this.table1 = table1;
	}
	
	public String getCol1() {
		return col1;
	}
	
	public void setCol1(String col1) {
		this.col1 = col1;
	}
	
	public String getTable2() {
		return table2;
	}
	
	public void setTable2(String table2) {
		this.table2 = table2;
	}
	
	public String getCol2() {
		return col2;
	}
	
	public void setCol2(String col2) {
		this.col2 = col2;
	}
	
	public String toString(){
		return table1.concat(".").concat(col1).concat(" = ").concat(table2).concat(".").concat(col2);
	}
}

package processor.gui;

import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.EventQueue;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultListModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JList;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.ListSelectionModel;
import javax.swing.SwingConstants;
import javax.swing.border.EmptyBorder;
import javax.swing.border.LineBorder;
import javax.swing.event.ListSelectionEvent;
import javax.swing.event.ListSelectionListener;
import javax.swing.table.DefaultTableModel;

import org.json.JSONArray;
import org.json.JSONException;

import app.QueryHelper;
import mediator.MergerHash;
import mediator.QueryExecutorNoSort;

import com.fasterxml.jackson.annotation.JsonAnyGetter;
import com.fasterxml.jackson.annotation.JsonAnySetter;
import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import java.awt.FlowLayout;
import java.awt.BorderLayout;
import java.awt.CardLayout;
import javax.swing.JSeparator;

public class ProcessorGui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = -6512279831942591585L;
	private JPanel contentPane;
	private JTable tableResultSet;
	private JList<String> listSelectQuery;
	private JList<String> listSelectSchema;
	private JLabel lblSelectedSchema;
	private JButton btnSelectSchema;
	private JLabel lblSelectedQuery;
	private JButton btnSelectQuery;
	private JButton btnExecuteQuery;
	private JPanel panelRight;
	private JTextArea textAreaSchemaTextViewer;
	private JTextArea textAreaQueryEditor;
	private JButton btnResetQuery;
	private JLabel lblQuery;
	private JLabel lblSchema;
	private JButton btnEditOrSaveQuery;
	private JLabel lblDurationInfo;

	private JLabel lblResultSet;
	
	private static final int QUERY_ON_EDIT = 1;
	private static final int QUERY_ON_VIEW = 0;
	private static final int NEW_QUERY = 1;
	private static final int OLD_QUERY = 0;
	private static final int MEDIATOR_TIME_INDEX = 0;
	private static final int WRAPPER_TIME_INDEX = 1;
	private static final int EXECUTION_TIME_INDEX = 3;
	
	
	private int stateQueryEditor = QUERY_ON_VIEW;
	private int editingQuery = -1;
	
	private boolean schemaSelected = false;
	private boolean querySelected = false;
	
	private String queryResetText="";
	private JSONArray result = null;
	private List<String> tableColumns = null;
	private JPanel panelExecute;
	private JSeparator separator;
	private JPanel panelLeft;
	private JPanel panelSelectSchema;
	private JPanel panelSelectQuery;
	private JPanel panelResultSet;
	private JPanel panelSchemaTextViewer;
	private JPanel panelQueryEditor;
	private JPanel panelSchemaQuery;
	private JPanel panelQueryEditorButtons;
	private JSeparator separator_1;
	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					ProcessorGui frame = new ProcessorGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	
	//The query processor GUI is separated into three main areas:
	//1. schema explorer, query explorer, and query processing button (leftside)
	//   location: leftPanel
	//2. Schema Text Viewer and Query Editor (upper-right side)
	//   location: rightPanel->panelSchemaQuery
	//3. Result set Viewer (lower-right side)
	//   location: rightPanel->panelResultSet
	//Initialization of each main area is separated by InitPanelLeft, InitPanelSchemaQuery, and InitPanelResultSet
	
	public ProcessorGui() {
		setTitle("MusQ - Query Processing");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(400, 200, 960, 560);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BoxLayout(contentPane, BoxLayout.X_AXIS));
		setIconImage(new ImageIcon("img/Logo2.jpg").getImage());
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				refreshPanels();
			}
		});
		
		InitPanelLeft();
		
		panelRight = new JPanel();
		contentPane.add(panelRight);
		panelRight.setLayout(new BoxLayout(panelRight, BoxLayout.Y_AXIS));

		InitPanelSchemaQuery();
		InitPanelExecutionButton();
		InitResultSet();

		repaint();
		//refreshPanels();
	}
	
	private void InitPanelLeft() {
		//Initialization of PanelLeft Components
		//Panel left is separated into three areas:
		//1. Schema explorer area: lets user choose appropriate schema or create new schema for query processing
		//2. Query explorer area: lets user choose query or create new query for query processing
		//3. Query execution area: lets user to execute query
		//Flow:
		//0. ALL areas are initially disabled except schema explorer
		//- unimplemented
		//0. Schema explorer will load all existing .schema files in conf directory to listSelectSchema, btnSelectSchema disabled
		//- unimplemented
		//0* and add <create new Schema> to listSelectSchema which will fire event to show new IntegratorDsMatcher window
		//- unimplemented
		//0** will refresh if the IntegratorDsMatcher window closed
		//- unimplemented
		//1. If any schema other than <create new schema> is selected on the listSelectSchema,
		//...btnSelectSchema will be enabled
		//...textAreaSchemaTextViewer will be enabled and display the schema text
		//...btnSelectSchema will set text as "Select Schema"
		//- unimplemented
		//1.a. If <create new schema> is selected on the listSelectSchema,
		//...btnSelectSchema will be enabled
		//...textAreaSchemaTextViewer will be disabled and display nothing
		//...btnSelectSchema will set text as "Create New Schema"
		//- unimplemented
		//2. Clicking btnSelectSchema will fire:
		//...set text lblSelectedSchema to "Selected Schema: xxx" xxx = selected schema name on listSelectSchema 

		
		panelLeft = new JPanel();
		panelLeft.setBorder(new LineBorder(new Color(0, 0, 0), 1, true));
		contentPane.add(panelLeft);
		panelLeft.setLayout(new BoxLayout(panelLeft, BoxLayout.Y_AXIS));
		
		panelSelectSchema = new JPanel();
		panelSelectSchema.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		panelLeft.add(panelSelectSchema);
		panelSelectSchema.setLayout(new BoxLayout(panelSelectSchema, BoxLayout.Y_AXIS));
		
		lblSelectedSchema = new JLabel("Selected Schema: -");
		panelSelectSchema.add(lblSelectedSchema);
		
		JScrollPane scrollPaneSelectedSchema = new JScrollPane();
		scrollPaneSelectedSchema.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelSelectSchema.add(scrollPaneSelectedSchema);
		
		listSelectSchema = new JList<String>();
		listSelectSchema.setAlignmentX(Component.RIGHT_ALIGNMENT);
		listSelectSchema.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		listSelectSchema.setMaximumSize(new Dimension((int)(0.2*contentPane.getWidth()), (int)(0.4*contentPane.getHeight())));
		LoadAllSchema();
		listSchemaAddSelectionListener();
		scrollPaneSelectedSchema.setViewportView(listSelectSchema);
		
		btnSelectSchema = new JButton("Select and View Schema");
		btnSelectSchema.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				LoadSchemaToSchemaTextViewer();
				lblSelectedSchema.setText("Selected Schema: "+listSelectSchema.getSelectedValue());
				lblSchema.setText("Schema: "+listSelectSchema.getSelectedValue());
				textAreaSchemaTextViewer.setEnabled(true);
				schemaSelected = true;
				if(schemaSelected&&querySelected)
					btnExecuteQuery.setEnabled(true);
			}
		});
		btnSelectSchema.setMaximumSize(new Dimension(Integer.MAX_VALUE, btnSelectSchema.getMinimumSize().height));
		btnSelectSchema.setEnabled(false);
		panelSelectSchema.add(btnSelectSchema);
		
		panelSelectQuery = new JPanel();
		panelSelectQuery.setBorder(new LineBorder(Color.LIGHT_GRAY, 1, true));
		panelLeft.add(panelSelectQuery);
		panelSelectQuery.setLayout(new BoxLayout(panelSelectQuery, BoxLayout.Y_AXIS));
		
		lblSelectedQuery = new JLabel("Selected Query: -");
		panelSelectQuery.add(lblSelectedQuery);

		JScrollPane scrollPaneSelectedQuery = new JScrollPane();
		scrollPaneSelectedQuery.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelSelectQuery.add(scrollPaneSelectedQuery);
		
		listSelectQuery = new JList<String>();
		listSelectQuery.setMaximumSize(new Dimension((int)(0.2*contentPane.getWidth()), (int)(0.4*contentPane.getHeight())));
		LoadAllQuery();
		listQueryAddSelectionListener();
		listSelectQuery.setSelectionMode(ListSelectionModel.SINGLE_SELECTION);
		scrollPaneSelectedQuery.setViewportView(listSelectQuery);
		
		btnSelectQuery = new JButton("Select and View Query");
		btnSelectQuery.setMaximumSize(new Dimension(Integer.MAX_VALUE, btnSelectQuery.getMinimumSize().height));
		btnSelectQuery.setEnabled(false);
		btnSelectQuery.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				if(CheckIfNewQuery()) {
					LoadQueryDefaultToQueryEditor();
					refreshSelectedQueryLabelsNewQuery();
					stateQueryEditor = QUERY_ON_EDIT;
					editingQuery = NEW_QUERY;
					querySelected = false;
				}
				else {
					LoadQueryToQueryEditor();
					refreshSelectedQueryLabels();
					stateQueryEditor = QUERY_ON_VIEW;
					editingQuery = OLD_QUERY;
					querySelected = true;
				}
				repaintQueryEditorArea();
				textAreaQueryEditor.setEnabled(true);
				if(schemaSelected&&querySelected)
					btnExecuteQuery.setEnabled(true);
			}
		});
		panelSelectQuery.add(btnSelectQuery);
		
	}

	private void LoadAllSchema() {
		DefaultListModel<String> model = new DefaultListModel<String>();
		List<String> allSchemaNames = ProcessorFileHelper.getAllSchemaNames();
		
		for(String schemaName:allSchemaNames)
			model.addElement(schemaName);
		model.addElement("<Create New Schema>");

		listSelectSchema.setModel(model);
	}
	private void LoadAllQuery() {

		DefaultListModel<String> model = new DefaultListModel<String>();
		
		List<String> allQueryNames = ProcessorFileHelper.getAllQueryNames();
		
		for(String queryName:allQueryNames)
			model.addElement(queryName);
		model.addElement("<Create New Query>");

		listSelectQuery.setModel(model);
	}


	private boolean CheckIfNewSchema() {
		//New Schema Index always be last element on model
		int lastElement = listSelectSchema.getModel().getSize()-1;
		return listSelectSchema.getSelectedIndex()==lastElement;
	}
	
	private boolean CheckIfNewQuery() {
		//New Query Index always be last element on model
		int lastElement = listSelectQuery.getModel().getSize()-1;
		return listSelectQuery.getSelectedIndex()==lastElement;
	}
	
	private void listSchemaAddSelectionListener() {
		ListSelectionListener listSelectionListener = new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent listSelectionEvent) {
				if(CheckIfNewSchema())
					btnSelectSchema.setText("Create New Schema");
				else
					btnSelectSchema.setText("Select and View Schema");
				btnSelectSchema.setEnabled(true);
			}
		};
	    listSelectSchema.addListSelectionListener(listSelectionListener);
	}
	
	private void listQueryAddSelectionListener() {
		ListSelectionListener listSelectionListener = new ListSelectionListener() {
			public void valueChanged(ListSelectionEvent listSelectionEvent) {
				if(CheckIfNewQuery()) {
					btnSelectQuery.setText("Create New Query");
				}
				else {
					btnSelectQuery.setText("Select and View Query");
				}
				btnSelectQuery.setEnabled(true);
			}
		};
	    listSelectQuery.addListSelectionListener(listSelectionListener);
	}
	
	private void LoadSchemaToSchemaTextViewer() {
		String selectedSchemaString = listSelectSchema.getSelectedValue();
		String schema = ProcessorFileHelper.getSchemaContent(selectedSchemaString);
		textAreaSchemaTextViewer.setText(schema);
	}
	
	private void LoadQueryToQueryEditor() {
		String selectedQuery = listSelectQuery.getSelectedValue();
		String query = ProcessorFileHelper.getQueryContent(selectedQuery);
		
		textAreaQueryEditor.setText(query);
		queryResetText = query;

		stateQueryEditor = QUERY_ON_VIEW;
	}
	
	private void LoadQueryDefaultToQueryEditor() {		
		String defaultQuery = ProcessorFileHelper.getDefaultQueryContent();
		
		textAreaQueryEditor.setText(defaultQuery);
		queryResetText = defaultQuery;
		stateQueryEditor = QUERY_ON_EDIT;
	}
	
	private void refreshSelectedQueryLabels(){
		lblSelectedQuery.setText("Selected Query: "+listSelectQuery.getSelectedValue());
		lblQuery.setText("Query: "+listSelectQuery.getSelectedValue());
	}
	
	private void refreshSelectedQueryLabelsNewQuery(){
		lblSelectedQuery.setText("Selected Query: "+"[Untitled]");
		lblQuery.setText("Query: "+"[Untitled]");
	}

	
	private void InitPanelSchemaQuery() {
		panelSchemaQuery = new JPanel();
		panelSchemaQuery.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelRight.add(panelSchemaQuery);
		panelSchemaQuery.setLayout(new BoxLayout(panelSchemaQuery, BoxLayout.X_AXIS));
		
		panelSchemaTextViewer = new JPanel();
		panelSchemaTextViewer.setAlignmentY(Component.TOP_ALIGNMENT);
		panelSchemaTextViewer.setBorder(new EmptyBorder(0, 5, 0, 5));
		panelSchemaQuery.add(panelSchemaTextViewer);
		panelSchemaTextViewer.setLayout(new BoxLayout(panelSchemaTextViewer, BoxLayout.Y_AXIS));
		
		lblSchema = new JLabel("Schema:");
		panelSchemaTextViewer.add(lblSchema);
		lblSchema.setHorizontalAlignment(SwingConstants.LEFT);
		
		JScrollPane scrollPaneSchemaTextViewer= new JScrollPane();
		scrollPaneSchemaTextViewer.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelSchemaTextViewer.add(scrollPaneSchemaTextViewer);
		
		textAreaSchemaTextViewer = new JTextArea();
		textAreaSchemaTextViewer.setEnabled(false);
		textAreaSchemaTextViewer.setEditable(false);
		scrollPaneSchemaTextViewer.setViewportView(textAreaSchemaTextViewer);
		
		panelQueryEditor = new JPanel();
		panelQueryEditor.setAlignmentY(Component.TOP_ALIGNMENT);
		panelQueryEditor.setBorder(new EmptyBorder(5, 5, 0, 5));
		panelSchemaQuery.add(panelQueryEditor);
		panelQueryEditor.setLayout(new BoxLayout(panelQueryEditor, BoxLayout.Y_AXIS));
		
		lblQuery = new JLabel("Query:");
		panelQueryEditor.add(lblQuery);
		
		JScrollPane scrollPaneQueryEditor = new JScrollPane();
		scrollPaneQueryEditor.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelQueryEditor.add(scrollPaneQueryEditor);
		
		textAreaQueryEditor = new JTextArea();
		textAreaQueryEditor.setRows(10);
		textAreaQueryEditor.setEnabled(false);
		textAreaQueryEditor.setEditable(false);
		scrollPaneQueryEditor.setViewportView(textAreaQueryEditor);
		
		panelQueryEditorButtons = new JPanel();
		panelQueryEditorButtons.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelQueryEditor.add(panelQueryEditorButtons);
		panelQueryEditorButtons.setLayout(new BoxLayout(panelQueryEditorButtons, BoxLayout.X_AXIS));
		
		Component horizontalGlue = Box.createHorizontalGlue();
		panelQueryEditorButtons.add(horizontalGlue);
		
		btnResetQuery = new JButton("Reset Query");
		panelQueryEditorButtons.add(btnResetQuery);
		btnResetQuery.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				textAreaQueryEditor.setText(queryResetText);
			}
		});
		btnResetQuery.setVisible(false);
		
		Component rigidArea = Box.createRigidArea(new Dimension(5,0));
		panelQueryEditorButtons.add(rigidArea);
		
		btnEditOrSaveQuery = new JButton("Edit Query");
		panelQueryEditorButtons.add(btnEditOrSaveQuery);
		btnEditOrSaveQuery.setEnabled(false);
		btnEditOrSaveQuery.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				//Save Query
				if(CheckIfOnEditQuery()) {
					String query = textAreaQueryEditor.getText();
					String queryName = "";
					if(editingQuery==NEW_QUERY)
						queryName = AcquireNewQueryName();
					else
						queryName = listSelectQuery.getSelectedValue();
					saveQueryAndViewUpdate(query,queryName);
				}
				else {
				//Edit Query
					stateQueryEditor = QUERY_ON_EDIT;	
				}
				repaintQueryEditorArea();
				if(schemaSelected&&querySelected)
					btnExecuteQuery.setEnabled(true);
			}
		});
	}
	
	private boolean CheckIfOnEditQuery() {
		return stateQueryEditor == QUERY_ON_EDIT;
	}
	
	private void repaintQueryEditorArea() {
		btnEditOrSaveQuery.setEnabled(true);
		if(CheckIfOnEditQuery()) {
			textAreaQueryEditor.setEditable(true);
			btnResetQuery.setVisible(true);
			btnEditOrSaveQuery.setText("Save Query");
		}
		else {
			textAreaQueryEditor.setEditable(false);
			btnResetQuery.setVisible(false);
			btnEditOrSaveQuery.setText("Edit Query");
		}
	}
	
	private String AcquireNewQueryName() {
		String newQueryName = JOptionPane.showInputDialog("Please input your query name:");
		boolean isValid = ProcessorFileHelper.checkNewQueryFile(newQueryName);

		while(!isValid) {
			JOptionPane.showConfirmDialog(null, "File already exists or does not have valid name!");
			newQueryName = JOptionPane.showInputDialog("Please input your query name:");
			isValid = ProcessorFileHelper.checkNewQueryFile(newQueryName);
		}
		return newQueryName;
	}
	
	private void saveQueryAndViewUpdate(String query, String queryName) {
		if(saveQueryToFile(query, queryName)) {
			LoadAllQuery();
			setSelectedListQuerybyName(queryName);
			stateQueryEditor = QUERY_ON_VIEW;
			if(editingQuery==NEW_QUERY) {
				refreshSelectedQueryLabels();
				editingQuery=OLD_QUERY;
				querySelected = true;
			}
		}
		else{
			if(editingQuery==NEW_QUERY)
				ProcessorFileHelper.DeleteQueryFile(queryName);
		}
	}
	
	private boolean saveQueryToFile(String query, String queryName) {
		boolean success = false;
		String saveQueryConfirmString = "Do you want to save this ["+queryName+"] query?";
		int saveQueryAnswer = JOptionPane.showConfirmDialog(null,saveQueryConfirmString, "Save Query", JOptionPane.YES_NO_OPTION);
		if(saveQueryAnswer==JOptionPane.YES_OPTION) {
			success = ProcessorFileHelper.SaveQuery(query, queryName);
			if(success)
				JOptionPane.showMessageDialog(null, "["+queryName+"] query was successfully saved");
			else
				JOptionPane.showMessageDialog(null, "Saving ["+queryName+"] query failed, please check your filesystem or query");
		}
		return success;
	}
	
	private void setSelectedListQuerybyName(String value) {
		DefaultListModel<String> model = (DefaultListModel<String>) listSelectQuery.getModel();
		int index = model.indexOf(value);
		listSelectQuery.setSelectedIndex(index);
		
	}
	

	private void doExecuteQuery() {
		String schemaPath = ProcessorFileHelper.getSchemaPath(listSelectSchema.getSelectedValue());
		String queryPath = ProcessorFileHelper.getQueryPath(listSelectQuery.getSelectedValue());
		try {
			QueryHelper qHelper = new QueryHelper();
			result = qHelper.executeWithTimeLog(schemaPath, queryPath, new MergerHash(), new QueryExecutorNoSort());
			tableColumns = qHelper.getQueryColumns(queryPath);
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	private void setJsonArrayOnJTable() {
		if(result==null){
			JOptionPane.showMessageDialog(null, "Execution returned NULL results");
			return;
		}
		String[] columns = new String[tableColumns.size()];
		columns = tableColumns.toArray(columns);
		DefaultTableModel model = new DefaultTableModel(columns, 0);
		
		ObjectMapper jsonmapper = new ObjectMapper();
        POJO p;
		try {
			for(Object resultRow:result)
			{
		        String[] row = new String[columns.length];
				p = jsonmapper.readValue(resultRow.toString(), POJO.class);
				
				for(int i=0;i<columns.length;i++)
					row[i]=p.map.get(columns[i]);
				model.addRow(row);
			};
			tableResultSet.setModel(model);
		} catch (JSONException | IOException e) {
			e.printStackTrace();
		}  
			
	}
	


	protected void updateLabelsResult() {
		if(result==null) {
			lblResultSet.setText("Result set: 0 rows");
			lblDurationInfo.setText("Execution information: Mediator Time: - Wrapper Time: - Execution Time: -");
			return;
		}
		
		lblResultSet.setText("Result set: "+result.length()+" rows");
		
		String[] times = ProcessorFileHelper.getPerformanceTime(listSelectQuery.getSelectedValue());
		String mediatorDuration = times[MEDIATOR_TIME_INDEX] + "ms";
		String wrapperDuration = times[WRAPPER_TIME_INDEX] + "ms";
		String executionDuration = times[EXECUTION_TIME_INDEX] + "ms";
		
		lblDurationInfo.setText("Execution information: "+
				"Mediator Time: "+mediatorDuration+" "+
				"Wrapper Time: "+wrapperDuration+" "+
				"Execution Time: "+executionDuration+" ");
	}
	
	private void InitPanelExecutionButton() {
		panelExecute = new JPanel();
		panelExecute.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelRight.add(panelExecute);
		
		separator = new JSeparator();
		panelExecute.add(separator);
		
		btnExecuteQuery = new JButton("Execute Query");
		btnExecuteQuery.setMaximumSize(new Dimension(Integer.MAX_VALUE,btnExecuteQuery.getMinimumSize().height));
		panelExecute.add(btnExecuteQuery);
		btnExecuteQuery.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				doExecuteQuery();
				setJsonArrayOnJTable();
				updateLabelsResult();
			}
		

		});
		panelExecute.setMaximumSize(new Dimension(Integer.MAX_VALUE,panelExecute.getMinimumSize().height));
		
		separator_1 = new JSeparator();
		panelExecute.add(separator_1);
	}
	private void InitResultSet() {
		

		btnExecuteQuery.setMaximumSize(new Dimension(Integer.MAX_VALUE, btnExecuteQuery.getMinimumSize().height));
		btnExecuteQuery.setEnabled(false);
		panelResultSet = new JPanel();
		panelResultSet.setBorder(new EmptyBorder(5, 5, 5, 5));
		panelRight.add(panelResultSet);
		panelResultSet.setLayout(new BoxLayout(panelResultSet, BoxLayout.Y_AXIS));
		
		lblResultSet = new JLabel("Result set: 0 rows");
		panelResultSet.add(lblResultSet);
		lblResultSet.setHorizontalAlignment(SwingConstants.LEFT);
		
		JScrollPane scrollPaneResultSet = new JScrollPane();
		scrollPaneResultSet.setAlignmentX(Component.LEFT_ALIGNMENT);
		panelResultSet.add(scrollPaneResultSet);
		
		tableResultSet = new JTable();
		scrollPaneResultSet.setViewportView(tableResultSet);
		JTable rowTable = new RowNumberTable(tableResultSet);
		scrollPaneResultSet.setRowHeaderView(rowTable);
		scrollPaneResultSet.setCorner(JScrollPane.UPPER_LEFT_CORNER,
		    rowTable.getTableHeader());
		lblDurationInfo = new JLabel("Execution information: Mediator Time: - Wrapper Time: - Execution Time: -");
		panelResultSet.add(lblDurationInfo);
	}
	
	private void refreshPanels() {
		panelRight.setMinimumSize(new Dimension(contentPane.getWidth()*80/100, contentPane.getHeight()));
		panelRight.setMaximumSize(new Dimension(contentPane.getWidth()*80/100, contentPane.getHeight()));
		panelLeft.setMinimumSize(new Dimension(contentPane.getWidth()*20/100, contentPane.getHeight()));
		panelLeft.setMaximumSize(new Dimension(contentPane.getWidth()*20/100, contentPane.getHeight()));
		
		panelSelectQuery.setMaximumSize(new Dimension(panelLeft.getWidth(), panelLeft.getHeight()*5/10));
		panelSelectQuery.setMinimumSize(new Dimension(panelLeft.getWidth(), panelLeft.getHeight()*5/10));
		panelSelectSchema.setMaximumSize(new Dimension(panelLeft.getWidth(), panelLeft.getHeight()*5/10));
		panelSelectSchema.setMinimumSize(new Dimension(panelLeft.getWidth(), panelLeft.getHeight()*5/10));
		
		panelExecute.setMinimumSize(new Dimension(panelRight.getWidth(),
				40));
		panelExecute.setMaximumSize(new Dimension(panelRight.getWidth(),
				40));
		
		panelSchemaQuery.setMinimumSize(new Dimension(panelRight.getWidth(),
				(panelRight.getHeight()*5/10)-40));
		panelSchemaQuery.setMaximumSize(new Dimension(panelRight.getWidth(),
				(panelRight.getHeight()*5/10)-40));

		
		
		panelResultSet.setMinimumSize(new Dimension(panelRight.getWidth(),panelRight.getHeight()*5/10));
		panelResultSet.setMaximumSize(new Dimension(panelRight.getWidth(),panelRight.getHeight()*5/10));

		textAreaSchemaTextViewer.setMaximumSize(new Dimension(panelSchemaQuery.getWidth()*5/10,Integer.MAX_VALUE));
		
		panelSchemaTextViewer.setMinimumSize(new Dimension(panelSchemaQuery.getWidth()*5/10,
				(int)textAreaSchemaTextViewer.getMaximumSize().getHeight()));
		panelSchemaTextViewer.setMaximumSize(new Dimension(panelSchemaQuery.getWidth()*5/10,
				(int)textAreaSchemaTextViewer.getMaximumSize().getHeight()));
		panelQueryEditorButtons.setMinimumSize(new Dimension(panelQueryEditor.getWidth(),
				40));
		panelQueryEditorButtons.setMaximumSize(new Dimension(panelQueryEditor.getWidth(),
				40));
		
		panelQueryEditor.setMinimumSize(new Dimension(panelSchemaQuery.getWidth()*5/10,
				panelSchemaQuery.getHeight()));
		panelQueryEditor.setMaximumSize(new Dimension(panelSchemaQuery.getWidth()*5/10,
				panelSchemaQuery.getHeight()));
	}
    public static class POJO{

        private Map<String,String> map = new TreeMap<String,String>();

        @JsonAnyGetter
        public Map<String, String> get() {
            return map;
        }

        @JsonAnySetter
        public void set(String name, String value) {
            map.put(name, value);
        }

    }	
	
}
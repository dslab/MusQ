package processor.gui;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.FilenameFilter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import org.apache.commons.io.FilenameUtils;



public class ProcessorFileHelper {
	

	public final static int NEW_QUERY = 1;
	public final static int EXISTING_QUERY = 0;
	public final static int NUM_TIMES_ELEMENT = 4;

	public static List<String> getAllSchemaNames() {
		File dir = new File("conf/");
		File[] files = dir.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.endsWith(".schema");
		    }
		});
		
		List<String> schemaNames = new ArrayList<String>();
		
		for(File f:files)
			schemaNames.add(FilenameUtils.removeExtension(f.getName().toString()));
		
		return schemaNames;
	}

	public static List<String> getAllQueryNames(){
		File dir = new File("experiment/query/");
		File[] files = dir.listFiles(new FilenameFilter() {
		    public boolean accept(File dir, String name) {
		        return name.endsWith(".txt");
		    }
		});
		
		List<String> queryNames = new ArrayList<String>();
		
		for(File f:files)
			queryNames.add(FilenameUtils.removeExtension(f.getName().toString()));
		
		return queryNames;
	}
	
	public static String getSchemaContent(String schemaName) {
		String filePath = getSchemaPath(schemaName);
		String schema = "";
		try {
			schema = readFile(filePath);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return schema;
	}
	
	public static String getQueryContent(String queryName) {
		String query = "";
		String filePath = getQueryPath(queryName);
		try {
			query = readFile(filePath);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return query;
	}
	
	public static String getDefaultQueryContent() {
		String defaultQuery = "";
		String filePath = "experiment/default.txt";
		try {
			defaultQuery = readFile(filePath);
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return defaultQuery;
	}
	
	private static String readFile(String path) throws IOException {
		byte[] encoded = Files.readAllBytes(Paths.get(path));
		return new String(encoded, Charset.defaultCharset());
	}
	
	public static boolean checkNewQueryFile(String newQueryName) {
		String newQueryFilePath = getQueryPath(newQueryName);
		File newQueryFile = new File(newQueryFilePath);
		try {
			return newQueryFile.createNewFile();
		} catch (IOException e) {
			e.printStackTrace();
		}
		return false;
	}
	
	public static boolean SaveQuery(String query, String queryName) {
		boolean success = false;
		String queryFilePath = getQueryPath(queryName);
		File queryFile = new File(queryFilePath);
		FileWriter fw;
		try {
			fw = new FileWriter(queryFile);
			fw.write(query);
			fw.close();
			success=true;
		} catch (IOException e) {
			e.printStackTrace();
		}
		return success;
		
	}

	public static void DeleteQueryFile(String queryName) {
		String queryFilePath = getQueryPath(queryName);
		File queryFile = new File(queryFilePath);
		queryFile.delete();
	}
	
	public static String getSchemaPath(String schemaName) {
		return "conf/"+schemaName+".schema";
	}
	
	public static String getQueryPath(String queryName) {
		return "experiment/query/"+queryName+".txt";
	}
	
	public static String getQueryTimePath(String queryName) {
		return "experiment/result/"+queryName+".txt";
	}
	
	public static String[] getPerformanceTime(String queryName) {
		String[] durationArray = new String[NUM_TIMES_ELEMENT];
	    String lastLine = "";
	    String sCurrentLine = "";
	    BufferedReader br;
		try {
			br = new BufferedReader(new FileReader(getQueryTimePath(queryName)));
			while ((sCurrentLine = br.readLine()) != null) 
		        lastLine = sCurrentLine;
			br.close();
			durationArray = lastLine.split("\\t");
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		} catch (IOException e) {
			e.printStackTrace();
		}
	    
		return durationArray;
	}
}
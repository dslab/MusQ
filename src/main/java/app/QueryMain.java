package app;

import java.io.IOException;

import mediator.MergerLoopMapping;
import mediator.QueryExecutorNoSort;

public class QueryMain {
	public static void main(String[] args) throws IOException{
		QueryHelper.execute(args, new MergerLoopMapping(), new QueryExecutorNoSort());
	}
}

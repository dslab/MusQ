package app;

import java.io.IOException;

import mediator.MergerHash;
import mediator.QueryExecutorNoSort;

public class QueryHash {
	public static void main(String[] args) throws IOException{
		QueryHelper.execute(args, new MergerHash(), new QueryExecutorNoSort());
	}
}

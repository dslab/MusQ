package app;

import java.io.IOException;

import mediator.MergerSort;
import mediator.QueryExecutorSort;

public class QuerySort {
	public static void main(String[] args) throws IOException{
		QueryHelper.execute(args, new MergerSort(), new QueryExecutorSort());
	}
}

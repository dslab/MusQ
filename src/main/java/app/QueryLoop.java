package app;

import java.io.IOException;

import mediator.MergerLoopNaive;
import mediator.QueryExecutorNoSort;

public class QueryLoop {
	public static void main(String[] args) throws IOException{
		QueryHelper.execute(args, new MergerLoopNaive(), new QueryExecutorNoSort());
	}
}

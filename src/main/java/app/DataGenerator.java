package app;

import java.io.FileWriter;
import java.io.IOException;
import java.text.DecimalFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import org.bson.Document;

import com.datastax.driver.core.ResultSet;
import com.mongodb.BasicDBObject;
import com.mongodb.DBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.util.JSON;

import wrapper.cassandra.CassandraDriver;
import wrapper.mongo.MongoDriver;

public class DataGenerator {
	private void generateCassandra(){
		List<Integer> rand = randPos();
		CassandraDriver driver = CassandraDriver.getInstance();
		driver.connect("164.125.37.223", "sensor");
		
		String[] sid = {"S0001","S0002","S0003","S0004","S0005"};
		
			int counter = 0;
			String myTime = "09:00:00";
			String endTime = "23:59:00";
			SimpleDateFormat df = new SimpleDateFormat("HH:mm:ss");
			try {
				Date d = df.parse(myTime);
				Date d2 = df.parse(endTime);
				while(d.before(d2)){
					Calendar cal = Calendar.getInstance();
					cal.setTime(d);
					cal.add(Calendar.SECOND, 10);
					String newTime = df.format(cal.getTime());
//					System.out.println(newTime);
					d = df.parse(newTime);
					for(int i = 0 ; i < sid.length ;  i++){
						int randint = 0;
						if(!rand.contains(counter)){
							randint = randInt(80,105);
						}
						String query = "insert into readings (SID, dt, tm, heartrate, pos) " + 
								" values('" + sid[i] + "', '2017-04-14','" + newTime + "', " + randint + ", {'lat': '" + randDouble(35.113270, 36.113270) + "', 'lon': '" + randDouble(128.124718, 129.124718) + "'});";
						System.out.println(query);
						ResultSet rs = driver.session.execute(query);
						counter++;
					}
				}
				
			} catch (ParseException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		System.out.println(counter);
		driver.close();
	}
	
	private void generateMongo(){
		MongoDriver driver = new MongoDriver();
		MongoCollection<Document> coll = driver.getCollection("batchwearable");
		
		String myTime = "2017-01-01";
		String endTime = "2017-03-31";
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		try {
			Date d = df.parse(myTime);
			Date d2 = df.parse(endTime);
//			System.out.println(d + "  " + d2);
			while(d.before(d2)){
				Calendar cal = Calendar.getInstance();
				cal.setTime(d);
				cal.add(Calendar.DATE, 1);
				String newTime = df.format(cal.getTime());
//				System.out.println(newTime);
				d = df.parse(newTime);
				
				String[] sid = {"S0001", "S0002", "S0003"};
				
				for(int i = 0; i < sid.length; i++){
					double avg = randDouble(93.0,97.0);
					double min = randDouble(78.0,84.0);
					double max = randDouble(99.0,105.0);
					
					Document document = new Document();
					document.put("SID", sid[i]);
					document.put("dt", newTime);
					document.put("avgRate", avg);
					document.put("minRate", min);
					document.put("maxRate", max);
					
					System.out.println(sid[i] + "," + newTime + "," + avg + "," + min + "," + max);
					
					coll.insertOne(document);
				}
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	private void generateAll() throws IOException{
		MongoDriver mdriver = new MongoDriver();
		MongoCollection<Document> coll = mdriver.getCollection("batchwearable");
		CassandraDriver cdriver = new CassandraDriver();
		FileWriter fw = new FileWriter("conf/batchwearable.csv");
		FileWriter fw2 = new FileWriter("conf/batchwearable.cql");
		
		String myTime = "2016-01-01";
		String endTime = "2016-12-31";
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		
		int dataCtr = 0;
		
		try {
			Date d = df.parse(myTime);
			Date d2 = df.parse(endTime);
//			System.out.println(d + "  " + d2);
			while(d.before(d2)){
				Calendar cal = Calendar.getInstance();
				cal.setTime(d);
				cal.add(Calendar.DATE, 1);
				String newTime = df.format(cal.getTime());
//				System.out.println(newTime);
				d = df.parse(newTime);
				
				//String[] sid = {"S0001", "S0002", "S0003", "S0004", "S0005"};
				String sid = "S0001";
				
				for(int i = 0; i < 1000; i++){					
					double avg = randDouble(93.0,97.0);
					double min = randDouble(78.0,84.0);
					double max = randDouble(99.0,105.0);
					
					Document document = new Document();
					document.put("SID", sid);
					document.put("dt", newTime);
					document.put("avgRate", avg);
					document.put("minRate", min);
					document.put("maxRate", max);
					
					String data = sid + "," + newTime + "," + avg + "," + min + "," + max;
					coll.insertOne(document);
					
					String query = "insert into batchwearable (SID, dt, avgRate, minRate, maxRate) " + 
							" values('" + sid + "', '" + newTime + "'," + avg + ", " + min + ", " + max + ");";
//					System.out.println(query);
					fw2.append(query + "\n");
//					ResultSet rs = cdriver.session.execute(query);
					
					System.out.println(data);
					fw.append(data + "\n");
					
					sid = increment(sid);
					dataCtr++;
				}
				
				cdriver.close();
			}
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		fw.flush();
		fw.close();
		
		fw2.flush();
		fw2.close();
		
		System.out.println(dataCtr + " data generated");
	}
	
	static final Pattern NUMBER_PATTERN = Pattern.compile("\\d+");

	 static String increment(String s) {
	     Matcher m = NUMBER_PATTERN.matcher(s);
	     if (!m.find())
	         throw new NumberFormatException();
	     String num = m.group();
	     int inc = Integer.parseInt(num) + 1;
	     String incStr = String.format("%0" + num.length() + "d", inc);
	     return  m.replaceFirst(incStr);
	 }
	 
	private static void insertMongo(){
//		MongoDriver driver = new MongoDriver();
//		driver.connect("164.125.37.225", 27017);
//		MongoCollection<Document> coll = driver.getCollection("test");
		 
		SimpleDateFormat df = new SimpleDateFormat("yyyy-MM-dd");
		String string = "S0004,2017-04-13,09:00:10.000000000,88,\"{'lat': '35.136', 'lon': '128.584'}\"";
		String[] str = string.split(",");
//		Document document = new Document();
//		document.put("sid", sid[i]);
//		document.put("dt", newTime);
//		document.put("tm", avg);
//		document.put("heartrate", min);
//		document.put("pos", max);
		String sid = str[0];
		Date dt;
		try {
			dt = df.parse(str[1]);
		} catch (ParseException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		
	}
	
	public static void main(String[] args){
		DataGenerator gen = new DataGenerator();
		insertMongo();
		try {
			gen.generateAll();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		gen.generateCassandra();
		gen.generateMongo();
	}
	
	private List<Integer> randPos(){
		List<Integer> ints = new ArrayList<>();
		while(ints.size() < 30){
			int rand = randInt(0, 30000);
			if(!ints.contains(rand)){
				ints.add(rand);
			}
		}
		return ints;
	}
	
	private int randInt(int min, int max) {
	    Random rand = new Random();
	    int randomNum = rand.nextInt((max - min) + 1) + min;

	    return randomNum;
	}
	
	private double randDouble(double min, double max) {
	    Random rand = new Random();
	    double randomNum = min + (max - min) * rand.nextDouble();
	    DecimalFormat df = new DecimalFormat("#.###");
	    return Double.parseDouble(df.format(randomNum));
	}
}

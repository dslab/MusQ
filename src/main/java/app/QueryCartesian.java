package app;

import java.io.IOException;

import mediator.MergerCartesian;
import mediator.QueryExecutorNoSort;


public class QueryCartesian {
	public static void main(String[] args) throws IOException{
		QueryHelper.execute(args, new MergerCartesian(), new QueryExecutorNoSort());
	}
}

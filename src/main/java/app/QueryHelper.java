package app;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;

import javax.swing.JOptionPane;

import mediator.Executor;
import mediator.Merger;
import mediator.QueryExecutor;
import mediator.QueryParser;
import mediator.QueryPlanner;
import mediator.QuerySplitter;
import mediator.ResultMerger;
import mediator.SchemaReader;

import org.apache.commons.io.FilenameUtils;
import org.json.JSONArray;

import utils.JSONAggregator;
import wrapper.cassandra.CassandraDriver;

public class QueryHelper {
	private static long duration;
	private static FileWriter fw;
	
	public static void execute(String[] args, Merger m, Executor exec) throws IOException{
		if(args.length < 1){
			System.out.println("No query input file specified");
			return;
		}
		
		
		String inpFile = "exp/query/" + args[0];
		File qFile = new File(inpFile);
		
		String outDir = "exp/result/";
		String outFile = outDir + args[0];
		
		File oDir = new File(outDir);
		
		if(!oDir.exists()){
			oDir.mkdir();
		}
		
		FileWriter fw = new FileWriter(outFile, true);
		
		System.out.println("Input Query: " + inpFile + " > " + outFile);
		
		List<String> times = new ArrayList<>();
		
		long startTime = System.nanoTime();
		//Query parsing
		QueryParser parser = QueryParser.getInstance();
		parser.parse(qFile);
		
		//read global schema
		SchemaReader reader = SchemaReader.getInstance();
		try {
			String schemaFile = "conf/traffic_sc.schema";
			if(args[1] != null){
				schemaFile = args[1];
			}
			reader.read(schemaFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		
		//split query
		QuerySplitter splitter = QuerySplitter.getInstance();
		splitter.split();
		
		//query plan
		QueryPlanner planner = QueryPlanner.getInstance();
		planner.plan();
		
		long endTime = System.nanoTime();			
		duration = (endTime - startTime)/1000000;
		times.add(Long.toString(duration));
		
		startTime = System.nanoTime();
		QueryExecutor executor = new QueryExecutor(exec);
		executor.execute();
		endTime = System.nanoTime();			
		duration = (endTime - startTime)/1000000;			
		times.add(Long.toString(duration));
		
		startTime = System.nanoTime();
				
		ResultMerger merger = new ResultMerger(m);				
		JSONArray result  = merger.merge();

		if(parser.datalog.getHead().getAggregation().size() > 0){
			result = JSONAggregator.aggregate(result, parser.datalog.getHead().getAggregation());
		}


		
		endTime = System.nanoTime();			
		duration = (endTime - startTime)/1000000;
		times.add(1, Long.toString(duration));
		times.add(2, Long.toString(Long.parseLong(times.get(0)) + (Long.parseLong(times.get(1)))));
		
		fw.append(String.join("\t", times) + "\n");
		fw.close();
		
		CassandraDriver driver = CassandraDriver.getInstance();
		if(driver.cluster != null && !driver.cluster.isClosed())
			driver.close();


	
	}
	
	private void InitTimeExperimentFiles(String queryName) throws IOException{
		
		
		String queryTimeExpResultDirPath = "experiment/result/";
		String queryTimeExpResultFilePath = queryTimeExpResultDirPath + queryName;
		
		File queryTimeExpResultDir = new File(queryTimeExpResultDirPath);
		
		if(!queryTimeExpResultDir.exists()){
			queryTimeExpResultDir.mkdir();
		}
		
		fw = new FileWriter(queryTimeExpResultFilePath, true);
	}
	
	public JSONArray executeWithTimeLog(String schemaPath, String queryPath, Merger m, Executor exec) throws IOException {
		long mediatorDuration = 0;
		long wrapperDuration = 0;
		long executionDuration = 0;
		
		
		InitTimeExperimentFiles(FilenameUtils.getName(queryPath));
		
		List<String> times = new ArrayList<>();
		
		//Mediator Time
		long startTime = System.nanoTime();
		QueryParser parser = getQueryParser(queryPath);
		readSchemaSplitQueryAndPlanQuery(schemaPath);
		long endTime = System.nanoTime();			
		
		mediatorDuration = (endTime - startTime)/1000000;
		times.add(Long.toString(mediatorDuration ));

		//Execution Time
		startTime = System.nanoTime();
		executeQuery(exec);
		endTime = System.nanoTime();	
		
		executionDuration= (endTime - startTime)/1000000;			
		times.add(Long.toString(executionDuration));
		

		//Wrapper Time
		startTime = System.nanoTime();
		JSONArray result = mergeQueryResults(m,parser);
		endTime = System.nanoTime();			
		
		wrapperDuration = (endTime - startTime)/1000000;
		times.add(1, Long.toString(wrapperDuration));
		times.add(2, Long.toString(mediatorDuration + wrapperDuration));
		
		fw.append(String.join("\t", times) + "\n");
		fw.close();
		checkAndCloseCassandraDriver();
		
		return result;
	}
	
	private void checkAndCloseCassandraDriver() {
		
		CassandraDriver driver = CassandraDriver.getInstance();
		if(driver.cluster != null && !driver.cluster.isClosed())
			driver.close();
	}
	
	private QueryParser getQueryParser(String queryPath) {
		//Query parsing
		File queryFile = new File(queryPath);
		QueryParser parser = QueryParser.getInstance();
		parser.parse(queryFile);
		return parser;
	}
	
	private void readSchemaSplitQueryAndPlanQuery(String schemaPath) throws IOException {

		//read global schema
		SchemaReader reader = SchemaReader.getInstance();
		reader.read(schemaPath);
	

		//split query
		QuerySplitter splitter = QuerySplitter.getInstance();
		splitter.split();
		
		//query plan
		QueryPlanner planner = QueryPlanner.getInstance();
		planner.plan();
	}
	private void executeQuery(Executor exec) {
		QueryExecutor executor = new QueryExecutor(exec);
		executor.execute();
	}
	
	private JSONArray mergeQueryResults(Merger m,QueryParser parser) {
		
		ResultMerger merger = new ResultMerger(m);
		
		JSONArray result  = merger.merge();

		if(parser.datalog.getHead().getAggregation().size() > 0){
			result = JSONAggregator.aggregate(result, parser.datalog.getHead().getAggregation());
		}
		return result;
		
	}
	
	public List<String> getQueryColumns(String queryPath){

		File queryFile = new File(queryPath);
		QueryParser parser = QueryParser.getInstance();
		parser.parse(queryFile);
		return parser.datalog.getHead().getColumns();
		
	}
}

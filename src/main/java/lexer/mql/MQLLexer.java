// Generated from MQL.g4 by ANTLR 4.4
package lexer.mql;
import org.antlr.v4.runtime.Lexer;
import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.Token;
import org.antlr.v4.runtime.TokenStream;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.misc.*;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class MQLLexer extends Lexer {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		CONDITION=1, SUM=2, COUNT=3, AVG=4, COMMA=5, PERIOD=6, LEFT_PAREN=7, RIGHT_PAREN=8, 
		COLON=9, COLON_DASH=10, MULTIPLY=11, ADD=12, AND=13, OP=14, NUMBER=15, 
		STRING=16, ID=17, WS=18, COMMENT=19;
	public static String[] modeNames = {
		"DEFAULT_MODE"
	};

	public static final String[] tokenNames = {
		"'\\u0000'", "'\\u0001'", "'\\u0002'", "'\\u0003'", "'\\u0004'", "'\\u0005'", 
		"'\\u0006'", "'\\u0007'", "'\b'", "'\t'", "'\n'", "'\\u000B'", "'\f'", 
		"'\r'", "'\\u000E'", "'\\u000F'", "'\\u0010'", "'\\u0011'", "'\\u0012'", 
		"'\\u0013'"
	};
	public static final String[] ruleNames = {
		"CONDITION", "SUM", "COUNT", "AVG", "COMMA", "PERIOD", "LEFT_PAREN", "RIGHT_PAREN", 
		"COLON", "COLON_DASH", "MULTIPLY", "ADD", "AND", "OP", "NUMBER", "STRING", 
		"ID", "WS", "COMMENT"
	};


	public MQLLexer(CharStream input) {
		super(input);
		_interp = new LexerATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}

	@Override
	public String getGrammarFileName() { return "MQL.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public String[] getModeNames() { return modeNames; }

	@Override
	public ATN getATN() { return _ATN; }

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\2\25\u0086\b\1\4\2"+
		"\t\2\4\3\t\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\4\n\t\n\4"+
		"\13\t\13\4\f\t\f\4\r\t\r\4\16\t\16\4\17\t\17\4\20\t\20\4\21\t\21\4\22"+
		"\t\22\4\23\t\23\4\24\t\24\3\2\3\2\5\2,\n\2\3\3\3\3\3\3\3\3\3\3\3\3\5\3"+
		"\64\n\3\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\3\4\5\4@\n\4\3\5\3\5\3\5\3"+
		"\5\3\5\3\5\5\5H\n\5\3\6\3\6\3\7\3\7\3\b\3\b\3\t\3\t\3\n\3\n\3\13\3\13"+
		"\3\13\3\f\3\f\3\r\3\r\3\16\3\16\3\16\3\16\3\17\3\17\3\17\3\17\3\17\5\17"+
		"d\n\17\3\20\6\20g\n\20\r\20\16\20h\3\21\3\21\3\21\7\21n\n\21\f\21\16\21"+
		"q\13\21\3\21\3\21\3\22\3\22\7\22w\n\22\f\22\16\22z\13\22\3\23\3\23\3\23"+
		"\3\23\3\24\3\24\7\24\u0082\n\24\f\24\16\24\u0085\13\24\2\2\25\3\3\5\4"+
		"\7\5\t\6\13\7\r\b\17\t\21\n\23\13\25\f\27\r\31\16\33\17\35\20\37\21!\22"+
		"#\23%\24\'\25\3\2\7\5\2\62;C\\c|\t\2\"\"--//\62<C\\aac|\7\2//\62;C\\a"+
		"ac|\5\2\13\f\17\17\"\"\4\2\f\f\16\17\u008f\2\3\3\2\2\2\2\5\3\2\2\2\2\7"+
		"\3\2\2\2\2\t\3\2\2\2\2\13\3\2\2\2\2\r\3\2\2\2\2\17\3\2\2\2\2\21\3\2\2"+
		"\2\2\23\3\2\2\2\2\25\3\2\2\2\2\27\3\2\2\2\2\31\3\2\2\2\2\33\3\2\2\2\2"+
		"\35\3\2\2\2\2\37\3\2\2\2\2!\3\2\2\2\2#\3\2\2\2\2%\3\2\2\2\2\'\3\2\2\2"+
		"\3+\3\2\2\2\5\63\3\2\2\2\7?\3\2\2\2\tG\3\2\2\2\13I\3\2\2\2\rK\3\2\2\2"+
		"\17M\3\2\2\2\21O\3\2\2\2\23Q\3\2\2\2\25S\3\2\2\2\27V\3\2\2\2\31X\3\2\2"+
		"\2\33Z\3\2\2\2\35c\3\2\2\2\37f\3\2\2\2!j\3\2\2\2#t\3\2\2\2%{\3\2\2\2\'"+
		"\177\3\2\2\2),\5\37\20\2*,\5!\21\2+)\3\2\2\2+*\3\2\2\2,\4\3\2\2\2-.\7"+
		"U\2\2./\7W\2\2/\64\7O\2\2\60\61\7u\2\2\61\62\7w\2\2\62\64\7o\2\2\63-\3"+
		"\2\2\2\63\60\3\2\2\2\64\6\3\2\2\2\65\66\7E\2\2\66\67\7Q\2\2\678\7W\2\2"+
		"89\7P\2\29@\7V\2\2:;\7e\2\2;<\7q\2\2<=\7w\2\2=>\7p\2\2>@\7v\2\2?\65\3"+
		"\2\2\2?:\3\2\2\2@\b\3\2\2\2AB\7C\2\2BC\7X\2\2CH\7I\2\2DE\7c\2\2EF\7x\2"+
		"\2FH\7i\2\2GA\3\2\2\2GD\3\2\2\2H\n\3\2\2\2IJ\7.\2\2J\f\3\2\2\2KL\7\60"+
		"\2\2L\16\3\2\2\2MN\7*\2\2N\20\3\2\2\2OP\7+\2\2P\22\3\2\2\2QR\7<\2\2R\24"+
		"\3\2\2\2ST\7<\2\2TU\7/\2\2U\26\3\2\2\2VW\7,\2\2W\30\3\2\2\2XY\7-\2\2Y"+
		"\32\3\2\2\2Z[\7C\2\2[\\\7P\2\2\\]\7F\2\2]\34\3\2\2\2^d\4>@\2_`\7>\2\2"+
		"`d\7?\2\2ab\7@\2\2bd\7?\2\2c^\3\2\2\2c_\3\2\2\2ca\3\2\2\2d\36\3\2\2\2"+
		"eg\4\62;\2fe\3\2\2\2gh\3\2\2\2hf\3\2\2\2hi\3\2\2\2i \3\2\2\2jk\7)\2\2"+
		"ko\t\2\2\2ln\t\3\2\2ml\3\2\2\2nq\3\2\2\2om\3\2\2\2op\3\2\2\2pr\3\2\2\2"+
		"qo\3\2\2\2rs\7)\2\2s\"\3\2\2\2tx\t\2\2\2uw\t\4\2\2vu\3\2\2\2wz\3\2\2\2"+
		"xv\3\2\2\2xy\3\2\2\2y$\3\2\2\2zx\3\2\2\2{|\t\5\2\2|}\3\2\2\2}~\b\23\2"+
		"\2~&\3\2\2\2\177\u0083\7%\2\2\u0080\u0082\n\6\2\2\u0081\u0080\3\2\2\2"+
		"\u0082\u0085\3\2\2\2\u0083\u0081\3\2\2\2\u0083\u0084\3\2\2\2\u0084(\3"+
		"\2\2\2\u0085\u0083\3\2\2\2\f\2+\63?Gchox\u0083\3\b\2\2";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}
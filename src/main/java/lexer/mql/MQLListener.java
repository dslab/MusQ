// Generated from MQL.g4 by ANTLR 4.4
package lexer.mql;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link MQLParser}.
 */
public interface MQLListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link MQLParser#head}.
	 * @param ctx the parse tree
	 */
	void enterHead(@NotNull MQLParser.HeadContext ctx);
	/**
	 * Exit a parse tree produced by {@link MQLParser#head}.
	 * @param ctx the parse tree
	 */
	void exitHead(@NotNull MQLParser.HeadContext ctx);
	/**
	 * Enter a parse tree produced by {@link MQLParser#schema}.
	 * @param ctx the parse tree
	 */
	void enterSchema(@NotNull MQLParser.SchemaContext ctx);
	/**
	 * Exit a parse tree produced by {@link MQLParser#schema}.
	 * @param ctx the parse tree
	 */
	void exitSchema(@NotNull MQLParser.SchemaContext ctx);
	/**
	 * Enter a parse tree produced by {@link MQLParser#filter}.
	 * @param ctx the parse tree
	 */
	void enterFilter(@NotNull MQLParser.FilterContext ctx);
	/**
	 * Exit a parse tree produced by {@link MQLParser#filter}.
	 * @param ctx the parse tree
	 */
	void exitFilter(@NotNull MQLParser.FilterContext ctx);
	/**
	 * Enter a parse tree produced by {@link MQLParser#agg}.
	 * @param ctx the parse tree
	 */
	void enterAgg(@NotNull MQLParser.AggContext ctx);
	/**
	 * Exit a parse tree produced by {@link MQLParser#agg}.
	 * @param ctx the parse tree
	 */
	void exitAgg(@NotNull MQLParser.AggContext ctx);
	/**
	 * Enter a parse tree produced by {@link MQLParser#col}.
	 * @param ctx the parse tree
	 */
	void enterCol(@NotNull MQLParser.ColContext ctx);
	/**
	 * Exit a parse tree produced by {@link MQLParser#col}.
	 * @param ctx the parse tree
	 */
	void exitCol(@NotNull MQLParser.ColContext ctx);
	/**
	 * Enter a parse tree produced by {@link MQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void enterPredicate(@NotNull MQLParser.PredicateContext ctx);
	/**
	 * Exit a parse tree produced by {@link MQLParser#predicate}.
	 * @param ctx the parse tree
	 */
	void exitPredicate(@NotNull MQLParser.PredicateContext ctx);
	/**
	 * Enter a parse tree produced by {@link MQLParser#colList}.
	 * @param ctx the parse tree
	 */
	void enterColList(@NotNull MQLParser.ColListContext ctx);
	/**
	 * Exit a parse tree produced by {@link MQLParser#colList}.
	 * @param ctx the parse tree
	 */
	void exitColList(@NotNull MQLParser.ColListContext ctx);
	/**
	 * Enter a parse tree produced by {@link MQLParser#query}.
	 * @param ctx the parse tree
	 */
	void enterQuery(@NotNull MQLParser.QueryContext ctx);
	/**
	 * Exit a parse tree produced by {@link MQLParser#query}.
	 * @param ctx the parse tree
	 */
	void exitQuery(@NotNull MQLParser.QueryContext ctx);
	/**
	 * Enter a parse tree produced by {@link MQLParser#idList}.
	 * @param ctx the parse tree
	 */
	void enterIdList(@NotNull MQLParser.IdListContext ctx);
	/**
	 * Exit a parse tree produced by {@link MQLParser#idList}.
	 * @param ctx the parse tree
	 */
	void exitIdList(@NotNull MQLParser.IdListContext ctx);
	/**
	 * Enter a parse tree produced by {@link MQLParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(@NotNull MQLParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link MQLParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(@NotNull MQLParser.BodyContext ctx);
}
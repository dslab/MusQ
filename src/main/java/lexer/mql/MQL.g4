grammar MQL;

/********************************************************
 * Token definitions conform to subset of Datalog Grammar
 ********************************************************/

query			: 	head COLON_DASH body;
head		 	: 	ID LEFT_PAREN colList RIGHT_PAREN;
body			: 	predicate (AND body)* ;
colList			: 	col (COMMA colList)*;
col				: 	ID | agg;
schema   		: 	ID LEFT_PAREN idList RIGHT_PAREN;
idList			: 	ID (COMMA idList)*;
predicate		:	schema | filter;
filter			: 	ID OP CONDITION;
CONDITION		:	(NUMBER | STRING);

agg				:	( SUM 
					| COUNT 
					| AVG )
					LEFT_PAREN ID RIGHT_PAREN;
SUM				: 'SUM' | 'sum';
COUNT			: 'COUNT' | 'count';
AVG				: 'AVG' | 'avg';

COMMA 			:	','		;
PERIOD			:	'.'		;
LEFT_PAREN		:	'(' 	;
RIGHT_PAREN		:	')' 	;
COLON			:	':' 	;
COLON_DASH		:	':-'	;
MULTIPLY		:	'*'		;
ADD				: 	'+'		;
AND				:	'AND'	;
OP				: 	'=' | '<' | '>' | '<=' | '>=';
NUMBER			: 	'0'..'9'+ ;
STRING			:	'\'' ('a'..'z'|'A'..'Z'|'0'..'9') ('a'..'z'|'A'..'Z'|'0'..'9'|'-'|'_'|' '|':'|'+')* '\'';
ID  :	('a'..'z'|'A'..'Z'|'0'..'9') ('a'..'z'|'A'..'Z'|'0'..'9'|'-'|'_')*
    ;
WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) -> skip
    ;


COMMENT
    : '#' ~[\r\n\f]*
    ; 


/* 
scheme   	: 	ID LEFT_PAREN ID idList RIGHT_PAREN;
schemeList	:	scheme schemeList  ;
idList  	:	COMMA ID idList ;
rules    	:	headPredicate COLON_DASH predicate predicateList PERIOD;
ruleList	:	rules ruleList;
headPredicate	:	ID LEFT_PAREN idList RIGHT_PAREN; 
predicate	:	ID LEFT_PAREN parameter parameterList RIGHT_PAREN;	
predicateList	:	COMMA predicate predicateList;
parameter	:	STRING | ID | expression;
parameterList	: 	COMMA parameter parameterList;
expression	: 	LEFT_PAREN parameter operator parameter RIGHT_PAREN;
operator	:	ADD | MULTIPLY;

STRING
    :  '\'' ( ESC_SEQ | ~('\\'|'\'') )* '\''
    ;

COMMENT
    : '#' ~[\r\n\f]*
    ; 

WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) -> channel(HIDDEN)
    ;

fragment
HEX_DIGIT : ('0'..'9'|'a'..'f'|'A'..'F') ;

fragment
ESC_SEQ
    :   '\\' ('b'|'t'|'n'|'f'|'r'|'\''|'\\')
    |   UNICODE_ESC
    |   OCTAL_ESC
    ;

fragment
OCTAL_ESC
    :   '\\' ('0'..'3') ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7') ('0'..'7')
    |   '\\' ('0'..'7')
    ;

fragment
UNICODE_ESC
    :   '\\' 'u' HEX_DIGIT HEX_DIGIT HEX_DIGIT HEX_DIGIT
    ;
*/
// Generated from MQL.g4 by ANTLR 4.7
package lexer.mql;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;


import java.util.Map;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import data.datalog.Body;
import data.datalog.Datalog;
import data.datalog.FilterPredicate;
import data.datalog.Predicate;

/**
 * This class provides an empty implementation of {@link MQLListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
public class MQLBaseListener implements MQLListener {
	public Datalog datalog;
	
	private Body body;
	private List<String> cols;	
	private Predicate predicate;
	private List<Predicate> predicates = new ArrayList<>();
	
	private FilterPredicate filter;
	private List<FilterPredicate> filters = new ArrayList<>();
	
	private Map<String, String> aggregation = new HashMap<>();
	
	@Override public void enterQuery(MQLParser.QueryContext ctx) {
		datalog = new Datalog();
	}
	
	@Override public void exitQuery(MQLParser.QueryContext ctx) {
		datalog.setBody(predicates, filters);
	}
	
	@Override public void enterHead(MQLParser.HeadContext ctx) {
		cols = new ArrayList<>();
	}
	
	@Override public void exitHead(MQLParser.HeadContext ctx) { 
		datalog.setHead(ctx.ID().getText(), cols, aggregation);
	}
	
	@Override public void enterColList(MQLParser.ColListContext ctx) {
		if(ctx.col().agg() != null){
			cols.add(ctx.col().agg().ID().getText());
			if(ctx.col().agg().SUM() != null){
				aggregation.put(ctx.col().agg().SUM().getText(), ctx.col().agg().ID().getText());
			}
			else if(ctx.col().agg().COUNT() != null){
				aggregation.put(ctx.col().agg().COUNT().getText(), ctx.col().agg().ID().getText());
			}
			else if(ctx.col().agg().AVG() != null){
				aggregation.put(ctx.col().agg().AVG().getText(), ctx.col().agg().ID().getText());
			}
		}
		else{
			cols.add(ctx.col().getText());
		}
	}
	
	@Override public void exitColList(MQLParser.ColListContext ctx) { }
	
	@Override public void enterCol(MQLParser.ColContext ctx) { }
	
	@Override public void exitCol(MQLParser.ColContext ctx) { }
	
	@Override public void enterBody(MQLParser.BodyContext ctx) {
		body = new Body();	
	}
	
	@Override public void exitBody(MQLParser.BodyContext ctx) {
			
	}
	
	@Override public void enterSchema(MQLParser.SchemaContext ctx) { 
		predicate = new Predicate();
		predicate.setTable(ctx.ID().getText());
	}
	
	@Override public void exitSchema(MQLParser.SchemaContext ctx) {
		predicate.setColumns(cols);
		predicates.add(predicate);
	}
	
	@Override public void enterIdList(MQLParser.IdListContext ctx) {
		cols.add(ctx.ID().getText());
		
	}
	
	@Override public void exitIdList(MQLParser.IdListContext ctx) { 
		
	}
	
	@Override public void enterPredicate(MQLParser.PredicateContext ctx) { 
		cols = new ArrayList<>();
	}
	
	@Override public void exitPredicate(MQLParser.PredicateContext ctx) { 
		body.setPredicate(predicates);	
	}
	
	@Override public void enterFilter(MQLParser.FilterContext ctx) { 
		filter = new FilterPredicate(ctx.ID().getText(), ctx.OP().getText(), ctx.CONDITION().getText());
	}
	
	@Override public void exitFilter(MQLParser.FilterContext ctx) { 
		filters.add(filter);
	}
	
	@Override public void enterAgg(MQLParser.AggContext ctx) { }
	
	@Override public void exitAgg(MQLParser.AggContext ctx) { }
	
	@Override public void enterEveryRule(ParserRuleContext ctx) { }
	
	@Override public void exitEveryRule(ParserRuleContext ctx) { }
	
	@Override public void visitTerminal(TerminalNode node) { }
	
	@Override public void visitErrorNode(ErrorNode node) { }
}

//Q(a,c,e,f) :- A(a,b,c) AND B(d,e) AND C(f) AND b > 50 AND d < 'test'
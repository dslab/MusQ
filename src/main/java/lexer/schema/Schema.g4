grammar Schema;

/********************************************************
 * Token definitions conform to subset of Datalog Grammar
 ********************************************************/

schema			: 	head COLON_DASH body COMMA matches ';';
head		 	: 	ID LEFT_PAREN colList RIGHT_PAREN;
body			: 	table (COMMA body)*;
colList			: 	col (COMMA colList)*;
col				: 	ID;
table   		: 	ID LEFT_PAREN colList RIGHT_PAREN '@' ID;
matches			: 	match (COMMA matches)*;
match			: 	ID PERIOD ID '=' ID PERIOD ID;

COMMA 			:	','		;
PERIOD			:	'.'		;
LEFT_PAREN		:	'(' 	;
RIGHT_PAREN		:	')' 	;
COLON_DASH		:	':-'	;
NUMBER			: 	'0'..'9'+ ;
STRING			:	'\'' ID '\'';
ID  :	('a'..'z'|'A'..'Z') ('a'..'z'|'A'..'Z'|'0'..'9'|'_')*
    ;
WS  :   ( ' '
        | '\t'
        | '\r'
        | '\n'
        ) -> skip
    ;


COMMENT
    : '#' ~[\r\n\f]*
    ; 
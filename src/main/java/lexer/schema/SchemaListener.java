// Generated from Schema.g4 by ANTLR 4.4
package lexer.schema;
import org.antlr.v4.runtime.misc.NotNull;
import org.antlr.v4.runtime.tree.ParseTreeListener;

/**
 * This interface defines a complete listener for a parse tree produced by
 * {@link SchemaParser}.
 */
public interface SchemaListener extends ParseTreeListener {
	/**
	 * Enter a parse tree produced by {@link SchemaParser#schema}.
	 * @param ctx the parse tree
	 */
	void enterSchema(@NotNull SchemaParser.SchemaContext ctx);
	/**
	 * Exit a parse tree produced by {@link SchemaParser#schema}.
	 * @param ctx the parse tree
	 */
	void exitSchema(@NotNull SchemaParser.SchemaContext ctx);
	/**
	 * Enter a parse tree produced by {@link SchemaParser#head}.
	 * @param ctx the parse tree
	 */
	void enterHead(@NotNull SchemaParser.HeadContext ctx);
	/**
	 * Exit a parse tree produced by {@link SchemaParser#head}.
	 * @param ctx the parse tree
	 */
	void exitHead(@NotNull SchemaParser.HeadContext ctx);
	/**
	 * Enter a parse tree produced by {@link SchemaParser#col}.
	 * @param ctx the parse tree
	 */
	void enterCol(@NotNull SchemaParser.ColContext ctx);
	/**
	 * Exit a parse tree produced by {@link SchemaParser#col}.
	 * @param ctx the parse tree
	 */
	void exitCol(@NotNull SchemaParser.ColContext ctx);
	/**
	 * Enter a parse tree produced by {@link SchemaParser#colList}.
	 * @param ctx the parse tree
	 */
	void enterColList(@NotNull SchemaParser.ColListContext ctx);
	/**
	 * Exit a parse tree produced by {@link SchemaParser#colList}.
	 * @param ctx the parse tree
	 */
	void exitColList(@NotNull SchemaParser.ColListContext ctx);
	/**
	 * Enter a parse tree produced by {@link SchemaParser#match}.
	 * @param ctx the parse tree
	 */
	void enterMatch(@NotNull SchemaParser.MatchContext ctx);
	/**
	 * Exit a parse tree produced by {@link SchemaParser#match}.
	 * @param ctx the parse tree
	 */
	void exitMatch(@NotNull SchemaParser.MatchContext ctx);
	/**
	 * Enter a parse tree produced by {@link SchemaParser#body}.
	 * @param ctx the parse tree
	 */
	void enterBody(@NotNull SchemaParser.BodyContext ctx);
	/**
	 * Exit a parse tree produced by {@link SchemaParser#body}.
	 * @param ctx the parse tree
	 */
	void exitBody(@NotNull SchemaParser.BodyContext ctx);
	/**
	 * Enter a parse tree produced by {@link SchemaParser#matches}.
	 * @param ctx the parse tree
	 */
	void enterMatches(@NotNull SchemaParser.MatchesContext ctx);
	/**
	 * Exit a parse tree produced by {@link SchemaParser#matches}.
	 * @param ctx the parse tree
	 */
	void exitMatches(@NotNull SchemaParser.MatchesContext ctx);
	/**
	 * Enter a parse tree produced by {@link SchemaParser#table}.
	 * @param ctx the parse tree
	 */
	void enterTable(@NotNull SchemaParser.TableContext ctx);
	/**
	 * Exit a parse tree produced by {@link SchemaParser#table}.
	 * @param ctx the parse tree
	 */
	void exitTable(@NotNull SchemaParser.TableContext ctx);
}
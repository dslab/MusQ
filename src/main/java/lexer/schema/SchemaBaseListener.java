// Generated from Schema.g4 by ANTLR 4.7
package lexer.schema;

import java.util.ArrayList;
import java.util.List;

import org.antlr.v4.runtime.ParserRuleContext;
import org.antlr.v4.runtime.tree.ErrorNode;
import org.antlr.v4.runtime.tree.TerminalNode;

import data.schema.GlobalSchema;
import data.schema.Matches;
import data.schema.Schema;

/**
 * This class provides an empty implementation of {@link SchemaListener},
 * which can be extended to create a listener which only needs to handle a subset
 * of the available methods.
 */
public class SchemaBaseListener implements SchemaListener {
	public GlobalSchema schemas;
	
	private Schema view;
	private List<String> cols;
	private Schema relation;
	private List<Schema> relations;
	private Matches match;
	private List<Matches> matches;
	
	@Override public void enterSchema(SchemaParser.SchemaContext ctx) { 
		schemas = new GlobalSchema();
		relations = new ArrayList<>();
		matches = new ArrayList<>();
	}

	@Override public void exitSchema(SchemaParser.SchemaContext ctx) { 
		schemas.setRelations(relations);
		schemas.setMatches(matches);
	}

	@Override public void enterHead(SchemaParser.HeadContext ctx) { 
		view = new Schema();
		cols = new ArrayList<>();
		view.setTable(ctx.ID().getText());
	}

	@Override public void exitHead(SchemaParser.HeadContext ctx) { 
		view.setColumns(cols);
		schemas.setView(view);
	}

	@Override public void enterBody(SchemaParser.BodyContext ctx) { }

	@Override public void exitBody(SchemaParser.BodyContext ctx) { }

	@Override public void enterColList(SchemaParser.ColListContext ctx) { 
		cols.add(ctx.col().getText());
	}

	@Override public void exitColList(SchemaParser.ColListContext ctx) { }

	@Override public void enterCol(SchemaParser.ColContext ctx) { }

	@Override public void exitCol(SchemaParser.ColContext ctx) { }

	@Override public void enterTable(SchemaParser.TableContext ctx) {
		relation = new Schema();
		cols = new ArrayList<>();
		relation.setTable(ctx.ID().get(0).toString());
		relation.setDatastore(ctx.ID().get(1).toString());
	}

	@Override public void exitTable(SchemaParser.TableContext ctx) { 
		relation.setColumns(cols);
		relations.add(relation);
	}
	
	@Override public void enterMatch(SchemaParser.MatchContext ctx) {
		match = new Matches();
		match.setTable1(ctx.ID().get(0).toString());
		match.setCol1(ctx.ID().get(1).toString());
		match.setTable2(ctx.ID().get(2).toString());
		match.setCol2(ctx.ID().get(3).toString());
		matches.add(match);
	}

	@Override public void exitMatch(SchemaParser.MatchContext ctx) { }
	
	@Override public void enterMatches(SchemaParser.MatchesContext ctx) {		
		
	}

	@Override public void exitMatches(SchemaParser.MatchesContext ctx) { 
		schemas.setMatch(match);
	}

	@Override public void enterEveryRule(ParserRuleContext ctx) { }

	@Override public void exitEveryRule(ParserRuleContext ctx) { }

	@Override public void visitTerminal(TerminalNode node) { }

	@Override public void visitErrorNode(ErrorNode node) { }
}
// Generated from Schema.g4 by ANTLR 4.4
package lexer.schema;
import org.antlr.v4.runtime.atn.*;
import org.antlr.v4.runtime.dfa.DFA;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.misc.*;
import org.antlr.v4.runtime.tree.*;
import java.util.List;
import java.util.Iterator;
import java.util.ArrayList;

@SuppressWarnings({"all", "warnings", "unchecked", "unused", "cast"})
public class SchemaParser extends Parser {
	static { RuntimeMetaData.checkVersion("4.7", RuntimeMetaData.VERSION); }

	protected static final DFA[] _decisionToDFA;
	protected static final PredictionContextCache _sharedContextCache =
		new PredictionContextCache();
	public static final int
		T__2=1, T__1=2, T__0=3, COMMA=4, PERIOD=5, LEFT_PAREN=6, RIGHT_PAREN=7, 
		COLON_DASH=8, NUMBER=9, STRING=10, ID=11, WS=12, COMMENT=13;
	public static final String[] tokenNames = {
		"<INVALID>", "'@'", "';'", "'='", "','", "'.'", "'('", "')'", "':-'", 
		"NUMBER", "STRING", "ID", "WS", "COMMENT"
	};
	public static final int
		RULE_schema = 0, RULE_head = 1, RULE_body = 2, RULE_colList = 3, RULE_col = 4, 
		RULE_table = 5, RULE_matches = 6, RULE_match = 7;
	public static final String[] ruleNames = {
		"schema", "head", "body", "colList", "col", "table", "matches", "match"
	};

	@Override
	public String getGrammarFileName() { return "Schema.g4"; }

	@Override
	public String[] getTokenNames() { return tokenNames; }

	@Override
	public String[] getRuleNames() { return ruleNames; }

	@Override
	public String getSerializedATN() { return _serializedATN; }

	@Override
	public ATN getATN() { return _ATN; }

	public SchemaParser(TokenStream input) {
		super(input);
		_interp = new ParserATNSimulator(this,_ATN,_decisionToDFA,_sharedContextCache);
	}
	public static class SchemaContext extends ParserRuleContext {
		public HeadContext head() {
			return getRuleContext(HeadContext.class,0);
		}
		public TerminalNode COLON_DASH() { return getToken(SchemaParser.COLON_DASH, 0); }
		public TerminalNode COMMA() { return getToken(SchemaParser.COMMA, 0); }
		public BodyContext body() {
			return getRuleContext(BodyContext.class,0);
		}
		public MatchesContext matches() {
			return getRuleContext(MatchesContext.class,0);
		}
		public SchemaContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_schema; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).enterSchema(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).exitSchema(this);
		}
	}

	public final SchemaContext schema() throws RecognitionException {
		SchemaContext _localctx = new SchemaContext(_ctx, getState());
		enterRule(_localctx, 0, RULE_schema);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(16); head();
			setState(17); match(COLON_DASH);
			setState(18); body();
			setState(19); match(COMMA);
			setState(20); matches();
			setState(21); match(T__1);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class HeadContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SchemaParser.ID, 0); }
		public TerminalNode RIGHT_PAREN() { return getToken(SchemaParser.RIGHT_PAREN, 0); }
		public ColListContext colList() {
			return getRuleContext(ColListContext.class,0);
		}
		public TerminalNode LEFT_PAREN() { return getToken(SchemaParser.LEFT_PAREN, 0); }
		public HeadContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_head; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).enterHead(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).exitHead(this);
		}
	}

	public final HeadContext head() throws RecognitionException {
		HeadContext _localctx = new HeadContext(_ctx, getState());
		enterRule(_localctx, 2, RULE_head);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(23); match(ID);
			setState(24); match(LEFT_PAREN);
			setState(25); colList();
			setState(26); match(RIGHT_PAREN);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class BodyContext extends ParserRuleContext {
		public TableContext table() {
			return getRuleContext(TableContext.class,0);
		}
		public BodyContext body(int i) {
			return getRuleContext(BodyContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SchemaParser.COMMA); }
		public List<BodyContext> body() {
			return getRuleContexts(BodyContext.class);
		}
		public TerminalNode COMMA(int i) {
			return getToken(SchemaParser.COMMA, i);
		}
		public BodyContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_body; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).enterBody(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).exitBody(this);
		}
	}

	public final BodyContext body() throws RecognitionException {
		BodyContext _localctx = new BodyContext(_ctx, getState());
		enterRule(_localctx, 4, RULE_body);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(28); table();
			setState(33);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(29); match(COMMA);
					setState(30); body();
					}
					} 
				}
				setState(35);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,0,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColListContext extends ParserRuleContext {
		public ColListContext colList(int i) {
			return getRuleContext(ColListContext.class,i);
		}
		public List<TerminalNode> COMMA() { return getTokens(SchemaParser.COMMA); }
		public List<ColListContext> colList() {
			return getRuleContexts(ColListContext.class);
		}
		public ColContext col() {
			return getRuleContext(ColContext.class,0);
		}
		public TerminalNode COMMA(int i) {
			return getToken(SchemaParser.COMMA, i);
		}
		public ColListContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_colList; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).enterColList(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).exitColList(this);
		}
	}

	public final ColListContext colList() throws RecognitionException {
		ColListContext _localctx = new ColListContext(_ctx, getState());
		enterRule(_localctx, 6, RULE_colList);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(36); col();
			setState(41);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(37); match(COMMA);
					setState(38); colList();
					}
					} 
				}
				setState(43);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,1,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class ColContext extends ParserRuleContext {
		public TerminalNode ID() { return getToken(SchemaParser.ID, 0); }
		public ColContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_col; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).enterCol(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).exitCol(this);
		}
	}

	public final ColContext col() throws RecognitionException {
		ColContext _localctx = new ColContext(_ctx, getState());
		enterRule(_localctx, 8, RULE_col);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(44); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class TableContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(SchemaParser.ID); }
		public TerminalNode RIGHT_PAREN() { return getToken(SchemaParser.RIGHT_PAREN, 0); }
		public ColListContext colList() {
			return getRuleContext(ColListContext.class,0);
		}
		public TerminalNode ID(int i) {
			return getToken(SchemaParser.ID, i);
		}
		public TerminalNode LEFT_PAREN() { return getToken(SchemaParser.LEFT_PAREN, 0); }
		public TableContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_table; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).enterTable(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).exitTable(this);
		}
	}

	public final TableContext table() throws RecognitionException {
		TableContext _localctx = new TableContext(_ctx, getState());
		enterRule(_localctx, 10, RULE_table);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(46); match(ID);
			setState(47); match(LEFT_PAREN);
			setState(48); colList();
			setState(49); match(RIGHT_PAREN);
			setState(50); match(T__2);
			setState(51); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatchesContext extends ParserRuleContext {
		public List<TerminalNode> COMMA() { return getTokens(SchemaParser.COMMA); }
		public MatchContext match() {
			return getRuleContext(MatchContext.class,0);
		}
		public MatchesContext matches(int i) {
			return getRuleContext(MatchesContext.class,i);
		}
		public TerminalNode COMMA(int i) {
			return getToken(SchemaParser.COMMA, i);
		}
		public List<MatchesContext> matches() {
			return getRuleContexts(MatchesContext.class);
		}
		public MatchesContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_matches; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).enterMatches(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).exitMatches(this);
		}
	}

	public final MatchesContext matches() throws RecognitionException {
		MatchesContext _localctx = new MatchesContext(_ctx, getState());
		enterRule(_localctx, 12, RULE_matches);
		try {
			int _alt;
			enterOuterAlt(_localctx, 1);
			{
			setState(53); match();
			setState(58);
			_errHandler.sync(this);
			_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			while ( _alt!=2 && _alt!=org.antlr.v4.runtime.atn.ATN.INVALID_ALT_NUMBER ) {
				if ( _alt==1 ) {
					{
					{
					setState(54); match(COMMA);
					setState(55); matches();
					}
					} 
				}
				setState(60);
				_errHandler.sync(this);
				_alt = getInterpreter().adaptivePredict(_input,2,_ctx);
			}
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static class MatchContext extends ParserRuleContext {
		public List<TerminalNode> ID() { return getTokens(SchemaParser.ID); }
		public TerminalNode ID(int i) {
			return getToken(SchemaParser.ID, i);
		}
		public List<TerminalNode> PERIOD() { return getTokens(SchemaParser.PERIOD); }
		public TerminalNode PERIOD(int i) {
			return getToken(SchemaParser.PERIOD, i);
		}
		public MatchContext(ParserRuleContext parent, int invokingState) {
			super(parent, invokingState);
		}
		@Override public int getRuleIndex() { return RULE_match; }
		@Override
		public void enterRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).enterMatch(this);
		}
		@Override
		public void exitRule(ParseTreeListener listener) {
			if ( listener instanceof SchemaListener ) ((SchemaListener)listener).exitMatch(this);
		}
	}

	public final MatchContext match() throws RecognitionException {
		MatchContext _localctx = new MatchContext(_ctx, getState());
		enterRule(_localctx, 14, RULE_match);
		try {
			enterOuterAlt(_localctx, 1);
			{
			setState(61); match(ID);
			setState(62); match(PERIOD);
			setState(63); match(ID);
			setState(64); match(T__0);
			setState(65); match(ID);
			setState(66); match(PERIOD);
			setState(67); match(ID);
			}
		}
		catch (RecognitionException re) {
			_localctx.exception = re;
			_errHandler.reportError(this, re);
			_errHandler.recover(this, re);
		}
		finally {
			exitRule();
		}
		return _localctx;
	}

	public static final String _serializedATN =
		"\3\u0430\ud6d1\u8206\uad2d\u4417\uaef1\u8d80\uaadd\3\17H\4\2\t\2\4\3\t"+
		"\3\4\4\t\4\4\5\t\5\4\6\t\6\4\7\t\7\4\b\t\b\4\t\t\t\3\2\3\2\3\2\3\2\3\2"+
		"\3\2\3\2\3\3\3\3\3\3\3\3\3\3\3\4\3\4\3\4\7\4\"\n\4\f\4\16\4%\13\4\3\5"+
		"\3\5\3\5\7\5*\n\5\f\5\16\5-\13\5\3\6\3\6\3\7\3\7\3\7\3\7\3\7\3\7\3\7\3"+
		"\b\3\b\3\b\7\b;\n\b\f\b\16\b>\13\b\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t\3\t"+
		"\2\2\n\2\4\6\b\n\f\16\20\2\2B\2\22\3\2\2\2\4\31\3\2\2\2\6\36\3\2\2\2\b"+
		"&\3\2\2\2\n.\3\2\2\2\f\60\3\2\2\2\16\67\3\2\2\2\20?\3\2\2\2\22\23\5\4"+
		"\3\2\23\24\7\n\2\2\24\25\5\6\4\2\25\26\7\6\2\2\26\27\5\16\b\2\27\30\7"+
		"\4\2\2\30\3\3\2\2\2\31\32\7\r\2\2\32\33\7\b\2\2\33\34\5\b\5\2\34\35\7"+
		"\t\2\2\35\5\3\2\2\2\36#\5\f\7\2\37 \7\6\2\2 \"\5\6\4\2!\37\3\2\2\2\"%"+
		"\3\2\2\2#!\3\2\2\2#$\3\2\2\2$\7\3\2\2\2%#\3\2\2\2&+\5\n\6\2\'(\7\6\2\2"+
		"(*\5\b\5\2)\'\3\2\2\2*-\3\2\2\2+)\3\2\2\2+,\3\2\2\2,\t\3\2\2\2-+\3\2\2"+
		"\2./\7\r\2\2/\13\3\2\2\2\60\61\7\r\2\2\61\62\7\b\2\2\62\63\5\b\5\2\63"+
		"\64\7\t\2\2\64\65\7\3\2\2\65\66\7\r\2\2\66\r\3\2\2\2\67<\5\20\t\289\7"+
		"\6\2\29;\5\16\b\2:8\3\2\2\2;>\3\2\2\2<:\3\2\2\2<=\3\2\2\2=\17\3\2\2\2"+
		"><\3\2\2\2?@\7\r\2\2@A\7\7\2\2AB\7\r\2\2BC\7\5\2\2CD\7\r\2\2DE\7\7\2\2"+
		"EF\7\r\2\2F\21\3\2\2\2\5#+<";
	public static final ATN _ATN =
		new ATNDeserializer().deserialize(_serializedATN.toCharArray());
	static {
		_decisionToDFA = new DFA[_ATN.getNumberOfDecisions()];
		for (int i = 0; i < _ATN.getNumberOfDecisions(); i++) {
			_decisionToDFA[i] = new DFA(_ATN.getDecisionState(i), i);
		}
	}
}
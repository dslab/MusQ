package mediator;

import org.json.JSONObject;

import data.schema.Datastore;
import data.schema.Matches;
import data.schema.SubQuery;

public interface Executor {
	public void execute();
	public JSONObject executeMySQL(SubQuery sq, Datastore ds, Matches match);
	public JSONObject executeCassandra(SubQuery sq, Datastore ds, Matches match);	
	public JSONObject executeDFS(SubQuery sq, Datastore ds, Matches match);	
	public JSONObject executeMongo(SubQuery sq, Datastore ds, Matches match);
}

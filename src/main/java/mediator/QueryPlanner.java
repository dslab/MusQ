package mediator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import data.datalog.Datalog;
import data.datalog.Predicate;
import data.schema.GlobalSchema;
import data.schema.Matches;
import data.schema.Schema;
import data.schema.SubQuery;

public class QueryPlanner {
	public List<QueryPlan> queryplans;
	Map<String, SubQuery> subQuery;
	Datalog datalog;
	List<GlobalSchema> querySchema;
	
	private static QueryPlanner instance = new QueryPlanner();
	
	public QueryPlanner(){
		QuerySplitter splitter = QuerySplitter.getInstance();
		QueryParser parser = QueryParser.getInstance();
		
		this.subQuery = splitter.subQuery;
		this.datalog = parser.datalog;
		this.querySchema = splitter.querySchema;
	}
	
	
	public static QueryPlanner getInstance( ) {
	      return instance;
	}
	
	public void plan(){		
		queryplans = new ArrayList<>();
		
		for(Predicate pred : datalog.getBody().getPredicate()){			
			GlobalSchema schema = getSchema(pred.getTable());
			List<Matches> match = schema.getMatches();
			for(Schema s : schema.getRelations()){				
				QueryPlan qplan = new QueryPlan();
				qplan.setParent(pred.getTable());
				qplan.setSubquery(subQuery.get(s.getTable()));
				qplan.setMatch(match);
				qplan.setMerged(false);
				queryplans.add(qplan);
			}
		}
//		for(QueryPlan p : queryplans){
//			System.out.println(p.toString());
//		}
		
//		return queryplans;
	}
	
	private GlobalSchema getSchema(String table){
		for(GlobalSchema schema: querySchema){
			if(schema.getView().getTable().equals(table)){
				return schema;
			}
		}
		return null;
	}
	
	public List<String> getAllParent(){
		List<String> parents = new ArrayList<>();
		for(QueryPlan plan: queryplans){
			if(!parents.contains(plan.getParent()))
				parents.add(plan.getParent());
		}
		return parents;
	}
	
	public Map<String, List<String>> getMatch(){
		Map<String, List<String>> match = new HashMap<>();
		for(QueryPlan plan: queryplans){
			List<String> tables = new ArrayList<>();
			for (Matches m : plan.getMatch()){
				tables.add(m.getTable1());
				tables.add(m.getTable2());
			}
			match.put(plan.getParent(), tables);
		}
		return match;
	}
	
	public List<String> getTables(Map<String, List<String>> match, Integer idx){
		Integer i = 0;
//		System.out.println(match.toString() + " " + idx);
		for(String key :match.keySet()){
			if(idx == i)
				return match.get(key);
			i++;
		}
		return null;		
	}
}
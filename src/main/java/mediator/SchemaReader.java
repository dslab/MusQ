package mediator;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;
import org.apache.commons.collections4.CollectionUtils;

import data.schema.GlobalSchema;
import data.schema.Schema;
import lexer.schema.SchemaBaseListener;
import lexer.schema.SchemaLexer;
import lexer.schema.SchemaParser;


public class SchemaReader {	
	private static BufferedReader br;
//	private static String inputFile = ("conf/mapping.schema");
	private SchemaBaseListener listener;
	public List<GlobalSchema> schemaTree;
	
	private static SchemaReader instance = new SchemaReader();
	
	public SchemaReader(){
		schemaTree = new ArrayList<>();
	}
	
	public static SchemaReader getInstance(){
		return instance;
	}
	
	public void read(String inputFile) throws IOException{
		InputStream is = System.in;
		if(inputFile != null){
			is = new FileInputStream(inputFile);
		}
		br = new BufferedReader(new InputStreamReader(is));
		
		String s = "";
		String str = "";
		List<String> strList = new ArrayList<>();
		while((s = br.readLine()) != null){
			str = str.concat(s);
			if(s.endsWith(";")){
				strList.add(str);
				str = "";
			}
		}

		for(String inputStr : strList){
			InputStream strIs = new ByteArrayInputStream(inputStr.getBytes());
			CharStream inputs = new UnbufferedCharStream(new BufferedReader(new InputStreamReader(strIs)));
			SchemaLexer mql = new SchemaLexer(inputs);
			mql.setTokenFactory(new CommonTokenFactory(true));
			CommonTokenStream tokens = new CommonTokenStream(mql);
			
			SchemaParser parser = new SchemaParser(tokens);
			//begin parsing only when "schema" grammar is found
			ParseTree tree = parser.schema();
			
			listener = new SchemaBaseListener();
			ParseTreeWalker.DEFAULT.walk(listener, tree);
			schemaTree.add(listener.schemas);
//			System.out.println(listener.schemas.toString());
		}
	}
	
	public GlobalSchema getSchema(String table){
		for(GlobalSchema s : schemaTree){
			if(s.getView().getTable().equals(table))
				return s;
		}
		return null;
	}
	
	public Map<String, List<String>> getAllSchemaTables(){
		Map<String, List<String>> tables = new HashMap<>();
		for(GlobalSchema schema : schemaTree){
			List<String> table = new ArrayList<>();
			for(Schema s : schema.getRelations()){
				table.add(s.getTable());
			}
			tables.put(schema.getView().getTable(),table);
		}
		return tables;
	}
	
	public boolean isSchemaExists(String t1, String t2){
		boolean bool = false;
		Map<String, List<String>> tables = getAllSchemaTables();
		for(String key: tables.keySet()){
			bool = CollectionUtils.containsAll(tables.get(key), new ArrayList<>(Arrays.asList(t1, t2)));
			if(bool)
				break;
		}
		System.out.println(bool);
		return bool;
	}
	
	public GlobalSchema getSchema(String t1, String t2){
		GlobalSchema schema = null;
		
		Map<String, List<String>> tables = getAllSchemaTables();
		for(String key: tables.keySet()){
//			System.out.println(t1 + " " + t2 + " " + tables.get(key));
			if(CollectionUtils.containsAll(tables.get(key), new ArrayList<>(Arrays.asList(t1, t2)))
					&& !t1.equals(t2)){
				schema = getSchema(key);
				break;
			}
		}
		return schema;
	}
}

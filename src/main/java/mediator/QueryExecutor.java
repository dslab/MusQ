package mediator;

public class QueryExecutor {
	Executor exec;
	
	public QueryExecutor(Executor exec){
		this.exec = exec;
	}
	
	public void execute(){
		exec.execute();
	}
}

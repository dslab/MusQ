package mediator;

import java.util.ArrayList;
import java.util.List;

import data.datalog.Datalog;
import data.datalog.Predicate;
import data.schema.GlobalSchema;

public class SchemaValidator {
	private Datalog mqlTree;
	private List<GlobalSchema> schemaTree;
	
	public SchemaValidator(Datalog mqlListener, List<GlobalSchema> schemaListener){
		this.mqlTree = mqlListener;
		this.schemaTree = schemaListener;
	}
	
	public boolean isValid(){
		boolean valid = false;
		List<String> qTables =  new ArrayList<>();
		List<String> sTables =  new ArrayList<>();
		for(Predicate pred: mqlTree.getBody().getPredicate()){
			qTables.add(pred.getTable());
		}
		
		for(GlobalSchema schema: schemaTree){
			sTables.add(schema.getView().getTable());
		}
		
		for(String table: qTables){
			if(sTables.contains(table)){
				valid = true;
			}
			else{
				valid = false;
				break;
			}
		}
		
		return valid;
	}
}

package mediator;

import org.json.JSONArray;
import org.json.JSONObject;

import utils.DatastoreUtil;
import utils.JsonUtil;
import wrapper.DFS.DFSWrapper;
import wrapper.MySQL.MySQLWrapper;
import wrapper.cassandra.CassandraWrapper;
import wrapper.mongo.MongoWrapper;
import data.schema.Datastore;
import data.schema.Matches;
import data.schema.SubQuery;

public class QueryExecutorSort implements Executor{
	@Override
	public void execute(){
		DatastoreUtil.init();
		
		QueryPlanner planner = QueryPlanner.getInstance();
		
		for(QueryPlan plan: planner.queryplans){
			SubQuery sq = plan.getSubquery();
			String dbID = sq.getSource().getDatastore();
			Datastore datastore = DatastoreUtil.getDatastore(dbID);
			String dsName = datastore.getDatabase();
			
			System.out.println(dsName);
//			System.out.println(sq.toString());
			Matches match = plan.getMatch().get(0);
//			System.out.println(plan.getSubquery().getView().getTable() + " " +  match.toString());
			switch(dsName){
				case Datastore.CASSANDRA:
					plan.setResult(executeCassandra(sq, datastore, match));
					break;
				case Datastore.MySQL:
					plan.setResult(executeMySQL(sq, datastore, match));
					break;
				case Datastore.DFS:
					plan.setResult(executeDFS(sq, datastore, match));
					break;
				case Datastore.Mongo:
					plan.setResult(executeMongo(sq, datastore, match));
					break;
			}
		}
	}
	
	@Override
	public JSONObject executeMySQL(SubQuery sq, Datastore ds, Matches match){
		JSONObject obj = MySQLWrapper.getData(sq, ds);
		JSONArray arr = obj.getJSONArray("result");
		String sortCol = getSortCol(match, sq.getView().getTable());
		arr = JsonUtil.sortJSON(arr, sortCol);
		obj.put("result", arr);
		return obj;
	}
	
	@Override
	public JSONObject executeCassandra(SubQuery sq, Datastore ds, Matches match){
		JSONObject obj = CassandraWrapper.getData(sq, ds);
		JSONArray arr = obj.getJSONArray("result");
		String sortCol = getSortCol(match, sq.getView().getTable());
		arr = JsonUtil.sortJSON(arr, sortCol);
		obj.put("result", arr);
		return obj;
	}
	
	@Override
	public JSONObject executeDFS(SubQuery sq, Datastore ds, Matches match){
		JSONObject obj = DFSWrapper.getData(sq, ds);
		JSONArray arr = obj.getJSONArray("result");
		String sortCol = getSortCol(match, sq.getView().getTable());
		arr = JsonUtil.sortJSON(arr, sortCol);
		obj.put("result", arr);
		return obj;
	}
	
	@Override
	public JSONObject executeMongo(SubQuery sq, Datastore ds, Matches match){
		JSONObject obj = MongoWrapper.getData(sq, ds);
		JSONArray arr = obj.getJSONArray("result");
		String sortCol = getSortCol(match, sq.getView().getTable());
		arr = JsonUtil.sortJSON(arr, sortCol);
		obj.put("result", arr);
		return obj;
	}
	
	private String getSortCol(Matches match, String table){
		String col = "";
		if(match.getTable1().equals(table))
			col = match.getCol1();
		else
			col = match.getCol2();
		return col;
	}
}

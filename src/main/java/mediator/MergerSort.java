package mediator;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import utils.JsonUtil;
import data.schema.Matches;

class Relation{
	private JSONArray arr;
    public int ENDPOS = -1;
    private int position = 0;
    
    public Relation(){
        this.arr = new JSONArray();
    }
    
    public Relation(JSONArray arr){
        this.arr = arr;
    }
    
    public int getPosition() {
		return position;
	}

	public void setPosition(int position) {
		this.position = position;
	}
	
	public String Key(String key){
        return arr.getJSONObject(position).get(key).toString();
    }
    
    public boolean increment(){
        if (getPosition() == arr.length() - 1 || getPosition() == ENDPOS){
            setPosition(ENDPOS);
            return false;
        }        
        setPosition(getPosition() + 1);
        return true;
    }

    public void add(Object key){
        arr.put(key);
    }

    public boolean isPastEnd(){
        return getPosition() == ENDPOS;
    }
    
    public String toString(){
    	return position + " " + arr.toString();
    }
}

public class MergerSort implements Merger{
	@Override
	public JSONArray join(JSONArray arr1, JSONArray arr2, List<Matches> matches) {
		System.out.println(">>> SORT MERGE JOIN");
		Matches match = matches.get(0);	
		String col1 = match.getCol1();
		String col2 = match.getCol2();
		//sort first
		arr1 = JsonUtil.sortJSON(arr1, col1);
		arr2 = JsonUtil.sortJSON(arr2, col2);
		
		JSONArray result = new JSONArray();
		Relation left = new Relation(arr1);
		Relation right = new Relation(arr2);

		while (!left.isPastEnd() && !right.isPastEnd()){
            if (left.Key(col1).compareTo(right.Key(col2)) == 0){         	
            	while (!left.isPastEnd() && left.Key(col1).compareTo(right.Key(col2)) == 0){
            		int rightIdx = right.getPosition();

            		while (rightIdx < arr2.length() && (left.Key(col1).compareTo(arr2.getJSONObject(rightIdx).get(col2).toString()) == 0)){
            			JSONObject resultObj = new JSONObject();
                    	for(String key: arr1.getJSONObject(left.getPosition()).keySet()){
        					resultObj.put(key, arr1.getJSONObject(left.getPosition()).get(key));
        				}
                    	for(String key: arr2.getJSONObject(rightIdx).keySet()){
        					resultObj.put(key, arr2.getJSONObject(rightIdx).get(key));
        				}            	
                    	result.put(resultObj);
//                    	System.out.println(resultObj.toString());
            			rightIdx++;
            		}
            		left.increment();

            		
            	}
//            	while (!right.isPastEnd() && (left.Key(col1).compareTo(right.Key(col2)) == 0)){
//        			JSONObject resultObj = new JSONObject();
//                	for(String key: arr1.getJSONObject(left.getPosition()).keySet()){
//    					resultObj.put(key, arr1.getJSONObject(left.getPosition()).get(key));
//    				}
//                	for(String key: arr2.getJSONObject(right.getPosition()).keySet()){
//    					resultObj.put(key, arr2.getJSONObject(right.getPosition()).get(key));
//    				}            	
//                	result.put(resultObj);
////                	System.out.println(resultObj.toString());
//        			
//        		}
//        		right.increment();
//        		left.increment();
            		
            }
            else if (left.Key(col1).compareTo(right.Key(col2)) < 0){
                left.increment();
            }
            else{
                right.increment();
            }
        }
//		System.out.println("FINAL RESULT: \n" + result.toString(4));
//		System.out.println(result.length());
		return result;
    }
	
	public static void main(String[] args){
		List<Matches> matches = new ArrayList<>();
		Matches match = new Matches();
		match.setCol1("SID");
		match.setCol2("sid");
		matches.add(match);
		
		String str1 = "[{\"serialNumber\": \"410413159-8\",\"model\": \"M0004\",\"SID\": \"S0001\"},{\"serialNumber\": \"455973706-1\",\"model\": \"M0001\",\"SID\": \"S0002\"},{\"serialNumber\": \"709506041-X\",\"model\": \"M0001\",\"SID\": \"S0003\"}]";
		String str2 = "[{\"dt\": \"2017-04-14\",\"heartrate\": 103,\"tm\": \"09:00:30.000000000\",\"sid\": \"S0001\"},{\"dt\": \"2017-04-14\",\"heartrate\": 90,\"tm\": \"09:00:20.000000000\",\"sid\": \"S0001\"},{\"dt\": \"2017-04-14\",\"heartrate\": 98,\"tm\": \"09:00:00.000000000\",\"sid\": \"S0002\"}]";
		JSONArray arr1 = new JSONArray(str1);
		JSONArray arr2 = new JSONArray(str2);
		System.out.println(arr1.toString(4));
		System.out.println(arr2.toString(4));
		MergerSort merger = new MergerSort();
		merger.join(arr1, arr2, matches);
	}
}

package mediator;


import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import data.schema.Matches;

public class MergerLoopNaive implements Merger{
	@Override
	public JSONArray join(JSONArray arr1, JSONArray arr2, List<Matches> matches) {
		System.out.println(">>> NAIVE NESTED LOOP JOIN");
		JSONArray result = new JSONArray();
		JSONObject obj;
		Matches match = matches.get(0);
		for(int i = 0; i < arr1.length(); i++){
			JSONObject obj1 = arr1.getJSONObject(i);
			
			for(int j = 0; j < arr2.length(); j++){
				JSONObject obj2 = arr2.getJSONObject(j);		
				obj = new JSONObject();
				
				for(String key1 : obj1.keySet()){
					if(match != null && !MergerUtil.isMatches(key1, match))
						obj.put(key1, obj1.get(key1));
					
					for(String key2 : obj2.keySet()){						
						if(match != null && !MergerUtil.isMatches(key2, match))
							obj.put(key2, obj2.get(key2));
						
						if(obj1.get(key1).equals(obj2.get(key2))){
							result.put(obj);
						}
					}
				}
			}
		}
		
		return result;
	}
}

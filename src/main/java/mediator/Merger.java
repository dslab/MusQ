package mediator;

import java.util.List;

import org.json.JSONArray;

import data.schema.Matches;

public interface Merger {
	public JSONArray join(JSONArray arr1, JSONArray arr2, List<Matches> match);
}

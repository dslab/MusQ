package mediator;

import java.util.List;

import org.json.JSONArray;

import data.schema.Matches;
import utils.JsonUtil;

public class MergerCartesian implements Merger{
	@Override
	public JSONArray join(JSONArray arr1, JSONArray arr2, List<Matches> match) {
		System.out.println(">>> CARTESIAN PRODUCT JOIN");
		JSONArray result = new JSONArray();
		result = JsonUtil.mergeJSON(arr1, arr2);
		return result;
	}
}

package mediator;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.InputStream;
import java.io.InputStreamReader;

import lexer.mql.MQLBaseListener;
import lexer.mql.MQLLexer;
import lexer.mql.MQLParser;

import org.antlr.v4.runtime.CharStream;
import org.antlr.v4.runtime.CommonTokenFactory;
import org.antlr.v4.runtime.CommonTokenStream;
import org.antlr.v4.runtime.UnbufferedCharStream;
import org.antlr.v4.runtime.tree.ParseTree;
import org.antlr.v4.runtime.tree.ParseTreeWalker;

import data.datalog.Datalog;

public class QueryParser {
	public Datalog datalog;
	
	private static QueryParser instance = new QueryParser();
	
	public QueryParser(){}
	
	public static QueryParser getInstance(){
		return instance;
	}
	
	public void parse(File qFile){
		InputStream is;
		try {
			is = new FileInputStream(qFile);
		
			BufferedReader br = new BufferedReader(new InputStreamReader(is));
			CharStream input = new UnbufferedCharStream(br);
			MQLLexer mql = new MQLLexer(input);
			mql.setTokenFactory(new CommonTokenFactory(true));
			CommonTokenStream tokens = new CommonTokenStream(mql);
		
			MQLParser parser = new MQLParser(tokens);
			ParseTree tree = parser.query();			
			
			MQLBaseListener listener = new MQLBaseListener();			
			ParseTreeWalker.DEFAULT.walk(listener, tree);
			
			datalog = listener.datalog;
//			System.out.println(listener.datalog.toString());
		} catch (FileNotFoundException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	
	}
}

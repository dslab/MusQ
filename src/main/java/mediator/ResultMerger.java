package mediator;

import java.util.ArrayList;
import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import data.schema.GlobalSchema;
import data.schema.Matches;

public class ResultMerger {
	Merger rm;
	
	public ResultMerger(Merger rm){
		this.rm = rm;
	}
	
	public JSONArray merge(){
		QueryPlanner planner = QueryPlanner.getInstance();
		
		List<JSONArray> resultList = new ArrayList<>();		
		List<QueryPlan> plans = new ArrayList<>();
		
		for(QueryPlan plan: planner.queryplans){			
			if(!plan.isMerged()){
				plans = MergerUtil.getQueryPlans(plan.getParent());
				resultList.add(mergeSubQuery(plans));
			}
		}
		
		return finalJoin(resultList);
	}
	
	private JSONArray mergeSubQuery(List<QueryPlan> plans){
		
		JSONObject result1 = plans.get(0).getResult();
		JSONObject result2 = plans.get(1).getResult();
		List<Matches> matches = plans.get(0).getMatch();
		JSONArray arr1 = result1.getJSONArray("result");		
		JSONArray arr2 = result2.getJSONArray("result");
		
		//force GC
		Runtime runtime = Runtime.getRuntime();
		runtime.gc();
		
		return rm.join(arr1, arr2, matches);
	}
	
	private JSONArray finalJoin(List<JSONArray> arrList){
		QueryPlanner planner = QueryPlanner.getInstance();		
		SchemaReader reader = SchemaReader.getInstance();
		JSONArray result = new JSONArray();

		if(arrList.size() == 1){
//			System.out.println(arrList.get(0).toString(4) + "\n");
			System.out.println("final result size : " + arrList.get(0).length());
			return arrList.get(0);
		}
		
		int index = 0;
		while(index < arrList.size() - 1){
			JSONArray arr1 = arrList.get(index);
			JSONArray arr2 = arrList.get(index+1);
			
			GlobalSchema schema = null;
			searchLoop: {
				for(String t1 : planner.getTables(planner.getMatch(), index)){
					for(String t2 :planner.getTables(planner.getMatch(), index+1)){
						if(reader.getSchema(t1,t2) != null){
							schema = reader.getSchema(t1,t2);
//							break searchLoop;
						}
					}
				}
			}

			if(schema != null){
//				System.out.println("USE SCHEMA");
				result = rm.join(arr1, arr2, schema.getMatches());
			}
			//if no global schema definition (no match exists) do nested loop join instead
			else{
//				System.out.println("NESTED LOOP JOIN");
				MergerLoopNaive loop = new MergerLoopNaive();				
				result = loop.join(arr1, arr2, null);
			}			
			
			index++;
		}
//		System.out.println(result.toString(4) + "\n");
		System.out.println("final result size : " + result.length());
		return result;
	}
}

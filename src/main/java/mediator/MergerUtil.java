package mediator;

import java.util.ArrayList;
import java.util.List;

import data.schema.Matches;

public class MergerUtil {
	public static List<QueryPlan> getQueryPlans(String parent){
		QueryPlanner planner = QueryPlanner.getInstance();
		List<QueryPlan> plans = new ArrayList<>();
		
		for(QueryPlan plan: planner.queryplans){
			if(plan.getParent().equals(parent)){
				plans.add(plan);
				plan.setMerged(true);
			}
		}
		
		return plans;
	}
	
	public static boolean isMatches(String col, Matches match){
		String col1 = match.getCol1();
		String col2 = match.getCol2();
		
		if(col.equals(col1) || col.equals(col2)){
			if(remove(col))
				return true;
		}
		return false;
	}
	
	public static boolean remove(String col){
		QueryParser parser = QueryParser.getInstance();
		if(!parser.datalog.getHead().getColumns().contains(col)){
			return true;
		}
		return false;
	}
}

package mediator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONObject;

import data.schema.Matches;

public class MergerHash implements Merger{
	@Override
	public JSONArray join(JSONArray arr1, JSONArray arr2, List<Matches> matches) {
		System.out.println(">>> HASH JOIN");
		JSONArray result = new JSONArray();
		JSONObject resultObj = new JSONObject();				
		Map<List<String>, List<JSONObject>> map = new HashMap<>();
		
		List<String> cols1 = new ArrayList<>();
		List<String> cols2 = new ArrayList<>();
		for(Matches m : matches){
//			System.out.println(m.toString());
			cols1.add(m.getCol1());
			cols2.add(m.getCol2());
		}
//		Matches match = matches.get(0);	
//		String col1 = match.getCol1();
//		String col2 = match.getCol2();
		
//		System.out.println(cols1 + " " + cols2);
		
		//check tuple size, hash smaller tuple
		if(arr1.length() <= arr2.length()){
			for(int i = 0; i < arr1.length(); i++){
				List<String> str = new ArrayList<>();
				for(String col1 : cols1)
					str.add(arr1.getJSONObject(i).get(col1).toString());
				
				List<JSONObject> v = map.getOrDefault(str, new ArrayList<>());
				v.add(arr1.getJSONObject(i));
//	            map.put(arr1.getJSONObject(i).getString(col1), v);
				map.put(str, v);
			}
//			System.out.println(map.toString());
			for(int i = 0; i < arr2.length(); i++){
				List<String> str = new ArrayList<>();
				for(String col2 : cols2)
					str.add(arr2.getJSONObject(i).get(col2).toString());
				
				List<JSONObject> v = map.get(str);
				if(v != null){
					for(JSONObject obj : v){
						for(int j = 0 ; j < matches.size(); j++){
							String col1 = cols1.get(j);
							String col2 = cols2.get(j);
//							System.out.println(obj.get(col1) + " " + arr2.getJSONObject(i).get(col2));
							if(obj.get(col1).equals(arr2.getJSONObject(i).get(col2))){
								resultObj = new JSONObject();
								
								for(String key: obj.keySet()){
	//								if(!MergerUtil.isMatches(key, match))
										resultObj.put(key, obj.get(key));
								}
								
								for(String key: arr2.getJSONObject(i).keySet()){
	//								if(!MergerUtil.isMatches(key, match))
										resultObj.put(key, arr2.getJSONObject(i).get(key));
								}
								
							}
						}
						result.put(resultObj);
					}
				}
			}
		}
		else{
			for(int i = 0; i < arr2.length(); i++){
				List<String> str = new ArrayList<>();
				for(String col2 : cols2)
					str.add(arr2.getJSONObject(i).get(col2).toString());
				List<JSONObject> v = map.getOrDefault(str, new ArrayList<>());
				v.add(arr2.getJSONObject(i));
	            map.put(str, v);            
			}
			
			for(int i = 0; i < arr1.length(); i++){
				List<String> str = new ArrayList<>();
				for(String col1 : cols1)
					str.add(arr1.getJSONObject(i).get(col1).toString());
				List<JSONObject> v = map.get(str);
				if(v != null){
					for(JSONObject obj : v){
						for(int j = 0 ; j < matches.size(); j++){
							String col1 = cols1.get(j);
							String col2 = cols2.get(j);
							if(obj.get(col2).equals(arr1.getJSONObject(i).get(col1))){
								resultObj = new JSONObject();
								
								for(String key: obj.keySet()){
	//								if(!MergerUtil.isMatches(key, match))
										resultObj.put(key, obj.get(key));
								}
								
								for(String key: arr1.getJSONObject(i).keySet()){
	//								if(!MergerUtil.isMatches(key, match))
										resultObj.put(key, arr1.getJSONObject(i).get(key));
								}
								
							}
						}
						result.put(resultObj);
					}
				}
			}
		}
		return result;
	}
}

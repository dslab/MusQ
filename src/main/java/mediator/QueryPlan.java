package mediator;

import java.util.List;

import org.json.JSONObject;

import data.schema.Matches;
import data.schema.SubQuery;

public class QueryPlan {
		private String parent;
		private SubQuery subquery;
		private List<Matches> match;
		private boolean merged;
		private JSONObject result;
		
		public String getParent() {
			return parent;
		}
		
		public void setParent(String parent) {
			this.parent = parent;
		}
		
		public SubQuery getSubquery() {
			return subquery;
		}

		public List<Matches> getMatch() {
			return match;
		}

		public void setMatch(List<Matches> match) {
			this.match = match;
		}
		
		public void setSubquery(SubQuery subquery) {
			this.subquery = subquery;
		}

		public boolean isMerged() {
			return merged;
		}

		public void setMerged(boolean merged) {
			this.merged = merged;
		}

		public JSONObject getResult() {
			return result;
		}

		public void setResult(JSONObject result) {
//			System.out.println(result.toString() + "\n\n");
			this.result = result;
		}
		
		public String toString(){
			String s = "";
			s += parent + " : ";
			if(!match.isEmpty()){
				for(Matches m: match){
					s += m.toString() + " ";
				}
			}
			s += "\n" + subquery.toString();
			
			if(result != null){
				s += result.toString() + "\n ";
			}
			
			s += merged + "\n ";
			return s;
		}
}

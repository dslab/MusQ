package mediator;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import data.datalog.Datalog;
import data.datalog.FilterPredicate;
import data.datalog.Predicate;
import data.schema.GlobalSchema;
import data.schema.Matches;
import data.schema.Schema;
import data.schema.SubQuery;

public class QuerySplitter {
	private Datalog datalog;
	private List<GlobalSchema> globalschema = new ArrayList<>();
	public Map<String, SubQuery> subQuery;
	public List<GlobalSchema> querySchema;
	
	private static QuerySplitter instance = new QuerySplitter();
	
	public QuerySplitter(){
		QueryParser parser = QueryParser.getInstance();
		SchemaReader reader = SchemaReader.getInstance();
		
		this.datalog = parser.datalog;
		this.globalschema = reader.schemaTree;
		subQuery = new LinkedHashMap<>();
	}
	
	public static QuerySplitter getInstance(){
		return instance;
	}
	
	public void split(){
//		Map<String, SubQuery> subquery = new LinkedHashMap<>();
		
		querySchema = new ArrayList<>();
		
//		System.out.print(datalog.toString());
		System.out.println("=========================================================");
		
		Map<String, List<String>> sqTable = new LinkedHashMap<>();
		
		//get query schema from global schema mapping
		for(Predicate p :datalog.getBody().getPredicate()){
			GlobalSchema schema = getGlobalSchemabyTable(p.getTable());
			if(schema != null){
				querySchema.add(schema);
				
				for(Schema source : schema.getRelations())
					sqTable.put(source.getTable(), new ArrayList<>());
			}
		}
		
		List<String> qCols = datalog.getHead().getColumns();
		List<String> sqCols = new ArrayList<>();
		
		//column projection, only select column mentioned in query
		for(String col : qCols){
//			System.out.println(col);
			List<GlobalSchema> schemas  = getGlobalSchemaByCol(col);
			for(GlobalSchema schema: schemas){
				Schema colSchema = getColTable(col, schema);				
				String table = colSchema.getTable();
				
				if(sqTable.get(table).isEmpty()){
					sqCols = new ArrayList<>();
					sqCols.add(col);
//					System.out.println(table + " " + sqCols);
					sqTable.put(table, sqCols);
//					System.out.println(sqTable.toString());
				}
				else{
					if(!sqTable.get(table).contains(col)){
//						if(sqTable.get(table).isEmpty()){
//							sqCols.add(col);
//						}
//						else{
							sqCols = sqTable.get(table);
							sqCols.add(col);
//						}
					}
//					System.out.println(table + " " + sqCols);
					sqTable.put(table, sqCols);
//					System.out.println(sqTable.toString());
				}
				
//				SubQuery sq = new SubQuery();
//				sq.setSource(colSchema);
			}
		}
		
//		System.out.println("\n" + sqTable.toString() + "\n");
		
		//if there is no column projection in source table query, get all columns instead	
		for(String key: sqTable.keySet()){
			if(sqTable.get(key).isEmpty()){
				Schema s = getSchemaByTable(key);
				sqTable.put(key, s.getColumns());
			}
		}
		
		//add match column into query projection
		for(GlobalSchema schema : globalschema){			
			for(int i = 0; i < schema.getMatches().size(); i++){
				Matches match = schema.getMatches().get(i);
				if(sqTable.get(match.getTable1()) != null && !sqTable.get(match.getTable1()).contains(match.getCol1())){
					List<String> cols1 = sqTable.get(match.getTable1());
					cols1.add(match.getCol1());
					sqTable.put(match.getTable1(), cols1);
				}				
				if(sqTable.get(match.getTable2()) != null && !sqTable.get(match.getTable2()).contains(match.getCol2())){
					List<String> cols2 = sqTable.get(match.getTable2());
					cols2.add(match.getCol2());
					sqTable.put(match.getTable2(), cols2);
				}
//				System.out.println(sqTable.toString());
			}
		}
		
		
		for(String key : sqTable.keySet()){
			Schema view = new Schema();
			view.setTable(key);
			view.setColumns(sqTable.get(key));
			
			SubQuery sq = new SubQuery();
			sq.setView(view);			
			sq.setSource(getSchemaByTable(key));
			
			subQuery.put(key, sq);			
		}
		
		//place filter into sub query				
		for(FilterPredicate filter: datalog.getBody().getFilters()){
			addFilter(filter);
			//place filter to another table if the column is match key
			if(isMatches(filter.getColumn())){
				List<String> cols = getMatches(filter.getColumn());
//				System.out.println(filter.getColumn() + " " + col);
				for(String col : cols){
					FilterPredicate f = new FilterPredicate(col, filter.getOperator(), filter.getCondition());
					addFilter(f);
				}
			}
		}
		
//		for(String key : subQuery.keySet()){
//			System.out.println(subQuery.get(key).toString());
//		}
		
//		return subquery;
	}
	
	private void addFilter(FilterPredicate filter){
		List<FilterPredicate> filters = new ArrayList<>();
		for(Schema s : getSchemaByCol(filter.getColumn())){
			String source = s.getTable();
			SubQuery sq = subQuery.get(source);
			if(sq.getFilter() == null){
				filters = new ArrayList<>();
				filters.add(filter);
			}
			else{
				filters = sq.getFilter();
				if(!isFilterExists(filters,filter))
					filters.add(filter);
			}
			sq.setFilter(filters);
		}
	}
	
	private GlobalSchema getGlobalSchemabyTable(String table){
		for(GlobalSchema schema: globalschema){
			if(table.equalsIgnoreCase(schema.getView().getTable())){
				return schema;
			}
		}
		return null;
	}
	
	private List<GlobalSchema> getGlobalSchemaByCol(String col){
		List<GlobalSchema> schemas = new ArrayList<>();
		for(GlobalSchema schema: querySchema){
			if(schema.getView().getColumns().contains(col)){
//				System.out.println(col + " " + schema.toString());
				schemas.add(schema);
			}
		}
		return schemas;
	}
	
	private Schema getSchemaByTable(String table){
		for(GlobalSchema schema: querySchema){
			for(Schema source: schema.getRelations()){
				if(source.getTable().equals(table)){
					return source;
				}
			}
		}
		return null;
	}
	
	private List<Schema> getSchemaByCol(String col){
		List<Schema> schemas = new ArrayList<>();
		for(GlobalSchema schema: querySchema){
			for(Schema source: schema.getRelations()){
				if(source.getColumns().contains(col)){
					schemas.add(source);
				}
			}
		}
		return schemas;
	}
	
//	private Schema getSchemaByCol(String col){
//		for(GlobalSchema schema: querySchema){
//			for(Schema source: schema.getRelations()){
//				if(source.getColumns().contains(col)){
//					return source;
//				}
//			}
//		}
//		return null;
//	}
	
	private Schema getColTable(String col, GlobalSchema schema){
		List<Schema> tables = schema.getRelations();
		
		for(Schema table: tables){
			if(table.getColumns().contains(col))
				return table;
		}
		
		return null;
	}
	
	private boolean isMatches(String col){
		for(GlobalSchema schema : querySchema){
			for(int i = 0; i < schema.getMatches().size(); i++){
				Matches match = schema.getMatches().get(i);
				
				if(match.getCol1().equals(col) || match.getCol2().equals(col))
					return true;
			}
		}
		return false;
	}
	
	private List<String> getMatches(String col){
		List<String> cols = new ArrayList<>();
		for(GlobalSchema schema : globalschema){
//			System.out.println(schema.toString());
			for(int i = 0; i < schema.getMatches().size(); i++){
				Matches match = schema.getMatches().get(i);
//				System.out.println(match.getCol1() + " " + match.getCol2() + " " + col);
				if(match.getCol1().equals(col))
					cols.add(match.getCol2());
//					return  match.getCol2();
				else if(match.getCol2().equals(col))
					cols.add(match.getCol1());
//					return match.getCol1();
			}
		}
		return cols;
	}
	
	private boolean isFilterExists(List<FilterPredicate> filters, FilterPredicate filter){
		for(FilterPredicate f : filters){
			if(f.getColumn().equals(filter.getColumn()) && f.getCondition().equals(filter.getCondition()) && f.getOperator().equals(filter.getOperator()))
				return true;
		}
		return false;
	}
}

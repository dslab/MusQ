package mediator;

import java.util.List;

import org.json.JSONArray;
import org.json.JSONObject;

import data.schema.Matches;

public class MergerLoopMapping implements Merger{
	@Override
	public JSONArray join(JSONArray arr1, JSONArray arr2, List<Matches> matches){
		System.out.println(">>> NESTED LOOP JOIN WITH SCHEMA MAPPING");
		JSONArray result = new JSONArray();
		
		Matches match = matches.get(0);		
		String col1 = match.getCol1();
		String col2 = match.getCol2();
		JSONObject resultObj;
		
		for(int i = 0; i < arr1.length(); i++){
			JSONObject r1 = arr1.getJSONObject(i);		
		
			for(int j = 0; j < arr2.length(); j++){				
				JSONObject r2 =  arr2.getJSONObject(j);
				if(r2.get(col2).equals(r1.get(col1))){
					
					resultObj = new JSONObject();
					for(String key: r1.keySet()){
//						if(!MergerUtil.isMatches(key, match))
							resultObj.put(key, r1.get(key));
					}
					for(String key: r2.keySet()){
//						if(!MergerUtil.isMatches(key, match))
							resultObj.put(key, r2.get(key));
					}
					result.put(resultObj);
					
				}
			}
		}
		return result;
	}
}

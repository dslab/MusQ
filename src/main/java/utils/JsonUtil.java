package utils;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class JsonUtil {
	public static Map<String, String> toMap(JSONObject object) throws JSONException {
	    Map<String, String> map = new LinkedHashMap<String, String>();

	    Iterator<String> keysItr = object.keys();
	    
	    while(keysItr.hasNext()) {
	        String key = keysItr.next();
	        String value = object.get(key).toString();
	        map.put(key, value);
	    }
	    return map;
	}
	
	@SuppressWarnings("rawtypes")
	public static JSONArray mergeJSON(JSONArray arr1, JSONArray arr2){
		JSONArray result = new JSONArray();
		int[] lengths = new int[] {arr1.length(), arr2.length()};
		for (int[] indices : new CartesianProduct(lengths)){
			JSONObject obj1 = arr1.getJSONObject(indices[0]);
			JSONObject obj2 = arr2.getJSONObject(indices[1]);
			JSONObject merged = new JSONObject();
			JSONObject[] objs = new JSONObject[] { obj1, obj2 };
			for (JSONObject obj : objs) {
			    Iterator it = obj.keys();
			    while (it.hasNext()) {
			        String key = (String)it.next();
			        merged.put(key, obj.get(key));
			    }
			}
//		    System.out.println(result1.getJSONObject(indices[0])  + ", " + result2.getJSONObject(indices[1]));
//			System.out.println(merged.toString());
			result.put(merged);
		}
		
//		System.out.println(result.toString());
		
		return result;
	}
	
	public static JSONObject deepMerge(JSONObject source, JSONObject target) throws JSONException {
	    for (String key: JSONObject.getNames(source)) {
	            Object value = source.get(key);
	            if (!target.has(key)) {
	                // new value for "key":
	                target.put(key, value);
	            } else {
	                // existing value for "key" - recursively deep merge:
	                if (value instanceof JSONObject) {
	                    JSONObject valueJson = (JSONObject)value;
	                    deepMerge(valueJson, target.getJSONObject(key));
	                } else {
	                    target.put(key, value);
	                }
	            }
	    }
	    return target;
	}
	
	public static JSONArray sortJSON(JSONArray jsonArr, String sortCol){
	    JSONArray sortedJsonArray = new JSONArray();

	    List<JSONObject> jsonValues = new ArrayList<JSONObject>();
	    for (int i = 0; i < jsonArr.length(); i++) {
	        jsonValues.add(jsonArr.getJSONObject(i));
	    }
	    Collections.sort( jsonValues, new Comparator<JSONObject>() {
	        @Override
	        public int compare(JSONObject a, JSONObject b) {
	            String valA = new String();
	            String valB = new String();

	            try {
	                valA = a.get(sortCol).toString();
	                valB = b.get(sortCol).toString();
	            } 
	            catch (JSONException e) {
	                //do something
	            }

	            return valA.compareTo(valB);
	            //if you want to change the sort order, simply use the following:
	            //return -valA.compareTo(valB);
	        }
	    });

	    for (int i = 0; i < jsonArr.length(); i++) {
	        sortedJsonArray.put(jsonValues.get(i));
	    }
//	    System.out.print(sortedJsonArray.toString(4));
	    return sortedJsonArray;
	}
	
	public static void main(String[] args){
		 String jsonArrStr = "[ { \"ID\": \"135\", \"Name\": \"Fargo Chan\" },{ \"ID\": \"432\", \"Name\": \"Aaron Luke\" },{ \"ID\": \"252\", \"Name\": \"Dilip Singh\" }]";
		 JSONArray arr = new JSONArray(jsonArrStr);
		 JsonUtil.sortJSON(arr, "Name");
	}
}

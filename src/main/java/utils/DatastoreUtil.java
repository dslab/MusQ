package utils;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.yaml.snakeyaml.DumperOptions;
import org.yaml.snakeyaml.DumperOptions.FlowStyle;
import org.yaml.snakeyaml.Yaml;
import org.yaml.snakeyaml.nodes.Tag;
import org.yaml.snakeyaml.representer.Representer;

import data.schema.Datastore;
import data.schema.DatastoreConfig;

/*
 * Update log by Hani Ramadhan 2018.02.06
 * 
 * Added:
 * - method writeConfig: to save configuration file
 * - method getAllDatastoreID: to display datastore's info on GUI
 * - method getUsername: to display datastore's user info on GUI
 * - method getPassword: to display datastore's password (hidden) info on GUI
 * - variable ArrayList<String> datastoreTypes: to display supported datastore types
 */

public class DatastoreUtil {
	private static final String configPath = "conf/datastore.yml";
	public static DatastoreConfig confData;
	public static List<Datastore> datastoreList = new ArrayList<>();
	public static String[] datastoreTypes = {"MySQL","Cassandra","HDFS","MongoDB"};
	
	public static void init(){
		readConfig();		
	}
	
	private static void readConfig(){
        InputStream input;
        try {
        	System.setProperty("hadoop.home.dir", "/home/hduser/hadoop/");
			input = new FileInputStream(new File(configPath));
			Yaml yaml = new Yaml();
            confData = yaml.loadAs(input, DatastoreConfig.class);
            datastoreList = confData.getDataStore();
            input.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}                     
    }
	
	public static Datastore getDatastore(String DBID){
		for(Datastore ds : datastoreList){
			if(ds.getID().equalsIgnoreCase(DBID))
				return ds;
		}
		return null;
	}
	
	public static String getDatastoreName(String DBID){		
		for(Datastore ds : datastoreList){
			if(ds.getID().equalsIgnoreCase(DBID))
				return ds.getDatabase();
		}
		return null;
	}
	
	public static String getDbName(String DBID){		
		for(Datastore ds : datastoreList){
			if(ds.getID().equalsIgnoreCase(DBID))
				return ds.getDbName();
		}
		return null;
	}
	
	public static String getAddress(String DBID){		
		for(Datastore ds : datastoreList){
			if(ds.getID().equalsIgnoreCase(DBID))
				return ds.getAddress();
		}
		return null;
	}
	
	public static String getUsername(String DBID) {
		for(Datastore ds : datastoreList){
			if(ds.getID().equalsIgnoreCase(DBID))
				return ds.getUser();
		}
		return null;
	}
	public static String getPassword(String DBID) {
		for(Datastore ds : datastoreList){
			if(ds.getID().equalsIgnoreCase(DBID))
				return ds.getPass();
		}
		return null;
	}
	
	public static void SaveConfig(){
		BufferedWriter output;
        try {
			output  = new BufferedWriter(new FileWriter(configPath));
//			DumperOptions option= new DumperOptions();
//			option.setDefaultFlowStyle(DumperOptions.FlowStyle.BLOCK);
//			option.setPrettyFlow(true);
//			Representer representer = new Representer();
//			representer.represent(datastoreList);
			Yaml yaml = new Yaml();
			String s = yaml.dumpAs(confData,new Tag("dataStore"),FlowStyle.BLOCK);
			output.write(s);
            output.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}      
	}
	
	public static void renewDatastore(List<Map<String,String>> datastoreConfigList) {
		datastoreList = new ArrayList<Datastore>();
		
		for(int i=0;i<datastoreConfigList.size();i++) {
			Map<String,String> datastoreConfig = datastoreConfigList.get(i);
			Datastore ds = new Datastore();
			ds.setID(datastoreConfig.get("ID"));
			ds.setDatabase(datastoreConfig.get("Database"));
			ds.setDbName(datastoreConfig.get("Name"));
			ds.setAddress(datastoreConfig.get("Address"));
			ds.setUser(datastoreConfig.get("Username"));
			ds.setPass(datastoreConfig.get("Password"));
			datastoreList.add(ds);
		}
		confData.setDataStore(datastoreList);
	}
	
	public static List<String> getAllDatastoreID(){
		ArrayList<String> listDatastoreName = new ArrayList<String>();
		for(Datastore ds : datastoreList){
			listDatastoreName.add(ds.getID());
		}
		
		return listDatastoreName;		
	}
	
}

package utils;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;

import org.json.JSONArray;
import org.json.JSONObject;

public class JSONAggregator {
	private static List<String> groupCols = new ArrayList<>();
	private static String aggCol;
	
	public static JSONArray aggregate(JSONArray arr, Map<String, String> agg){
		JSONArray result = null;
		String aggFunction = agg.keySet().toArray()[0].toString();
		
		Map<List<String>, List<JSONObject>> map = buildHashMap(arr, agg.get(aggFunction));
		aggCol = agg.get(aggFunction);
		
		if(aggFunction.equalsIgnoreCase("AVG")){
			result = avg(arr, map);
		}
		else if(aggFunction.equalsIgnoreCase("COUNT")){
			result = count(arr, map);
		}
		else if(aggFunction.equalsIgnoreCase("MIN")){
			result = min(arr, map);
		}
		else if(aggFunction.equalsIgnoreCase("MAX")){
			result = max(arr, map);
		}
		System.out.println("aggregated final result size : " + result.length());
//		System.out.println(result.toString(4));
		return result;
	}
	
	private static Map<List<String>, List<JSONObject>> buildHashMap(JSONArray arr, String aggCol){
		Map<List<String>, List<JSONObject>> map = new HashMap<>();
		
		//get grouping columns
		JSONObject obj = arr.getJSONObject(0);
		for(String col : obj.keySet()){
			if(!col.equalsIgnoreCase(aggCol))
				groupCols.add(col);
		}
		
		//build hash
		for(int i = 0; i < arr.length(); i++){
			List<String> key = new ArrayList<>();
			for(String col : groupCols)
				key.add(arr.getJSONObject(i).get(col).toString());
			
			List<JSONObject> v = map.getOrDefault(key, new ArrayList<>());
			v.add(arr.getJSONObject(i));
			map.put(key, v);
		}
		return map;
	}
	
	public static JSONArray avg(JSONArray arr, Map<List<String>, List<JSONObject>> map){
		JSONArray arrResult = new JSONArray();
		
		//calculate aggregate value
		for (final Entry<List<String>, List<JSONObject>> entry : map.entrySet()) {
			
			double sum = 0;
			for(JSONObject val : entry.getValue()){
				sum += (double) val.get(aggCol);
			}
			
			final int count = (int) entry.getValue().stream().count();
			JSONObject o = new JSONObject();
			int i = 0;
			for(String col : groupCols){
				o.put(col, entry.getKey().get(i++));
			}
			o.put(aggCol, sum/count);
			arrResult.put(o);
		}
		return arrResult;	
	}
	
	public static JSONArray count(JSONArray arr, Map<List<String>, List<JSONObject>> map){
		JSONArray arrResult = new JSONArray();
		
		//calculate aggregate value
		for (final Entry<List<String>, List<JSONObject>> entry : map.entrySet()) {		    
			final int count = (int) entry.getValue().stream().count();
			JSONObject o = new JSONObject();
			int i = 0;
			for(String col : groupCols){
				o.put(col, entry.getKey().get(i++));
			}
			o.put(aggCol, count);
			arrResult.put(o);
		}
		return arrResult;	
	}
	
	public static JSONArray min(JSONArray arr, Map<List<String>, List<JSONObject>> map){
		JSONArray arrResult = new JSONArray();
		Object minVal = null;
		
		//calculate aggregate value
		for (final Entry<List<String>, List<JSONObject>> entry : map.entrySet()) {
			for(JSONObject val : entry.getValue()){
				if(minVal == null || (double)minVal > (double) val.get(aggCol))
					minVal = val.get(aggCol);
			}
			JSONObject o = new JSONObject();
			int i = 0;
			for(String col : groupCols){
				o.put(col, entry.getKey().get(i++));
			}
			o.put(aggCol, minVal);
			arrResult.put(o);
		}
		return arrResult;	
	}
	
	public static JSONArray max(JSONArray arr, Map<List<String>, List<JSONObject>> map){
		JSONArray arrResult = new JSONArray();
		Object maxVal = null;
		
		//calculate aggregate value
		for (final Entry<List<String>, List<JSONObject>> entry : map.entrySet()) {
			for(JSONObject val : entry.getValue()){
				if(maxVal == null || (double)maxVal < (double) val.get(aggCol))
					maxVal = val.get(aggCol);
			}
			JSONObject o = new JSONObject();
			int i = 0;
			for(String col : groupCols){
				o.put(col, entry.getKey().get(i++));
			}
			o.put(aggCol, maxVal);
			arrResult.put(o);
		}
		return arrResult;	
	}
	
//	public static JSONArray avg(JSONArray arr, String aggCol){
//		List<Object[]> objList = new ArrayList<>();
////		System.out.println(arr.toString());
//		
//		int groupIdx = Integer.MIN_VALUE;
//		int aggIdx = Integer.MIN_VALUE;
//		
//		for(int i = 0; i < arr.length(); i++){
//			JSONObject obj = arr.getJSONObject(i);
//			Object[] objdata = new Object[obj.keySet().size()];
//			int idx = 0;
//			for(String key : obj.keySet()){
//				objdata[idx] = obj.get(key);
//				if(groupIdx == Integer.MIN_VALUE && !key.equalsIgnoreCase(aggCol)){
//					groupIdx = idx;
//				}
//				if(aggIdx == Integer.MIN_VALUE && key.equalsIgnoreCase(aggCol)){
//					aggIdx = idx;
//				}
//				idx++;
//			}
//			objList.add(objdata);
//		}
//		
//		if(aggIdx == Integer.MIN_VALUE)
//			aggIdx = 0;
//		
//		final int gIdx = groupIdx;
//		final int aIdx = aggIdx;
//		
//		
//		
//		final Map<String, List<Object[]>> map = objList.stream().collect(
//				Collectors.groupingBy(row -> row[gIdx].toString()));
//		
//		JSONArray arrResult = new JSONArray();
//		
//		for (final Map.Entry<String, List<Object[]>> entry : map.entrySet()) {
//		    final double average = entry.getValue().stream()
//		                                .mapToDouble(row -> (double) row[aIdx]).average().getAsDouble();
//		    JSONObject obj = getObj(arr, entry.getKey());
//		    obj.put(obj.names().getString(aggIdx), average);
//		    arrResult.put(obj);
//		}
//		
//		return arrResult;		
//	}
	
//	public static JSONArray count(JSONArray arr, String aggCol){
//		List<Object[]> objList = new ArrayList<>();
//		
//		int groupIdx = Integer.MIN_VALUE;
//		int aggIdx = Integer.MIN_VALUE;
//		
//		for(int i = 0; i < arr.length(); i++){
//			JSONObject obj = arr.getJSONObject(i);
//			Object[] objdata = new Object[obj.keySet().size()];
//			int idx = 0;
//			for(String key : obj.keySet()){
//				objdata[idx] = obj.get(key);
//				if(groupIdx == Integer.MIN_VALUE && !key.equalsIgnoreCase(aggCol)){
//					groupIdx = idx;
//				}
//				if(aggIdx == Integer.MIN_VALUE && key.equalsIgnoreCase(aggCol)){
//					aggIdx = idx;
//				}
//				idx++;
//			}
//			objList.add(objdata);
//		}
//		
//		final int gIdx = groupIdx;
//		final int aIdx = aggIdx;
//		
//		final Map<String, List<Object[]>> map = objList.stream().collect(
//				Collectors.groupingBy(row -> row[gIdx].toString()));
//		
//		JSONArray arrResult = new JSONArray();
//		
//		for (final Map.Entry<String, List<Object[]>> entry : map.entrySet()) {
//		    final double average = entry.getValue().stream()
//		                                .mapToInt(row -> (int) row[aIdx]).count();
//		    JSONObject obj = getObj(arr, entry.getKey());
//		    obj.put(obj.names().getString(aggIdx), average);
//		    arrResult.put(obj);
//		}
//		
//		return arrResult;		
//	}
//	
//	private static JSONObject getObj(JSONArray arr, String key){
//		for(int i = 0; i < arr.length(); i++){
//			JSONObject obj = arr.getJSONObject(i);
//			JSONArray keys = obj.names();
//			for(int j = 0; j < keys.length(); j++){
//				if(obj.get(keys.getString(j)).equals(key)){
//					return obj;
//				}
//			}
//	    }
//		return null;
//	}
}

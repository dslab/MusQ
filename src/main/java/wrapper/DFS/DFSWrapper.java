package wrapper.DFS;

import java.io.BufferedReader;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import data.datalog.FilterPredicate;
import data.schema.Datastore;
import data.schema.Schema;
import data.schema.SubQuery;

public class DFSWrapper {

	public static JSONObject getData(SubQuery sq, Datastore ds) {
		JSONObject res = new JSONObject();
		DFSDriver driver = DFSDriver.getInstance();
		List<BufferedReader> brList = driver.getDataFile(ds);
		
		JSONObject metaObj = new JSONObject();
		metaObj.put("dbName", ds.getDbName());
		metaObj.put("table", sq.getSource().getTable());
		metaObj.put("datastore", Datastore.DFS);
		res.put("meta", metaObj);
        
		JSONArray arr = new JSONArray();
        String line;
        try {
        	for(BufferedReader br : brList){
        		String[] cols =  br.readLine().split(","); 
				while (( line = br.readLine()) != null){
					JSONObject obj = new JSONObject();
					for(int i = 0; i < cols.length; i++){
						Object val = line.split(",")[i];
						if(isNumber(val.toString())){
							val = Double.parseDouble(val.toString());
						}
						if(sq.getSource().getColumns().contains(cols[i]))
							obj.put(cols[i], val);
					}
					arr.put(obj);
				}
//        		System.out.println(arr.toString(4));
				
		        if(sq != null && sq.getFilter().size() > 0){
		        	String col = "";
		        	String op = "";
		        	String fil = "";
					for(FilterPredicate filter : sq.getFilter()){
						col = filter.getColumn();
						fil = filter.getCondition();
						op = filter.getOperator();
						if(fil.startsWith("'") && fil.endsWith("'")){
							fil = fil.replaceAll("'", "");
						}
//						System.out.println(col + " " + op + " " + fil);
						
						JSONArray arr2 = new JSONArray();
						for(int i = 0; i < arr.length(); i++){
							switch(op){
								case "=":
									if(arr.getJSONObject(i).get(col).toString().equals(fil)){
										arr2.put(arr.getJSONObject(i));
									}
									 break;
								case "<=":
									if(arr.getJSONObject(i).get(col).toString().compareTo(fil) < 0 ||
											arr.getJSONObject(i).get(col).toString().equals(fil)){
										arr2.put(arr.getJSONObject(i));
									}
									break;
								case ">=":
									if(arr.getJSONObject(i).get(col).toString().compareTo(fil) > 0 ||
											arr.getJSONObject(i).get(col).toString().equals(fil)){
										arr2.put(arr.getJSONObject(i));
									}
									break;
								case "<":
									if(arr.getJSONObject(i).get(col).toString().compareTo(fil) < 0){
										arr2.put(arr.getJSONObject(i));
									}
									break;
								 case ">":
									 if(arr.getJSONObject(i).get(col).toString().compareTo(fil) > 0){
										 arr2.put(arr.getJSONObject(i));
									 }
									 break;
							 }
						 }
						 res.put("result", arr2);
					}
				}
		        else{
		        	res.put("result", arr);
		        }
				br.close();
        	}
			driver.fs.close();
        } catch (NumberFormatException | JSONException | IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//        System.out.println(res.toString(4));
		return res;
	}
	
	private static boolean isNumber(String s){
		return NumberUtils.isNumber(s);
	}
	
	public static void main(String args[]){
		SubQuery sq = new SubQuery();
		Schema source = new Schema();
		source.setTable("batchwearable");
		List<String> columns = Arrays.asList("SID", "dt", "avgRate");		
		source.setColumns(columns);
		sq.setSource(source);
		
		List<FilterPredicate> filters = new ArrayList<>();
		FilterPredicate filter = new FilterPredicate("SID", ">", "'S0003'");
		filters.add(filter);
		filter = new FilterPredicate("avgRate", "=", "95.711");
		filters.add(filter);
		sq.setFilter(filters);
		
		Datastore ds = new Datastore();
		ds.setDbName("sensor");
		ds.setAddress("hdfs://164.125.37.224:9000");
		getData(sq,ds);
	}
}

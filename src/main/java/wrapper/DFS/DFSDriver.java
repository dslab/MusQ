package wrapper.DFS;

import integrator.config.InstanceLevel;
import integrator.config.IntegratorUtil;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.apache.hadoop.conf.*;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.FileStatus;
import org.json.JSONArray;
import org.json.JSONObject;

import wrapper.DatastoreSchema;
import data.schema.Datastore;
import data.schema.SubQuery;

public class DFSDriver {
//	static String dfs = "hdfs://192.168.56.103:8020";
	static String dfs;// = "hdfs://164.125.37.224:9000";
	public FileSystem fs;
	public DFSDriver(){}
	
	public static DFSDriver instance = new DFSDriver();
	
	public static DFSDriver getInstance(){
		return instance;
	}

	public List<BufferedReader> getDataFile(Datastore ds){
		String folder = ds.getDbName();
		dfs = ds.getAddress();
		
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", dfs);

        
        List<BufferedReader> brList = new ArrayList<>();
		try {
			fs = FileSystem.get(conf);
			FileStatus[] status = fs.listStatus(new Path("/" + folder));
	        
			for(int i = 0 ; i < status.length; i++){            
	            String filepath = status[i].getPath().toString();
//	            System.out.println(filepath);
	            Path pt = new Path(filepath);
	            BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));
	            brList.add(br);
	        }
	        
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
//		System.out.println(res.toString(4));
        return brList;
	}

	public List<DatastoreSchema> getSchema(Datastore ds) {
		List<DatastoreSchema> schemas = new ArrayList<>();
		String folder = ds.getDbName();
		dfs = ds.getAddress();
		
        Configuration conf = new Configuration();
        conf.set("fs.defaultFS", dfs);

		try {
			fs = FileSystem.get(conf);
			FileStatus[] status = fs.listStatus(new Path("/" + folder));
			for(int i = 0 ; i < status.length; i++){       
	            String filepath = status[i].getPath().toString();

	            Path pt = new Path(filepath);
	            BufferedReader br = new BufferedReader(new InputStreamReader(fs.open(pt)));
	            //get column name and first data for sample
	            String[] cols =  br.readLine().split(",");
	            String[] data = br.readLine().split(",");
	            
	            Map<String, String> columns = new LinkedHashMap<String, String>();
	            for(int j = 0; j < cols.length; j++){
	            	if(NumberUtils.isNumber(data[j])){
	            		columns.put(cols[j], "NUMBER");
	            	}
	            	else{
	            		columns.put(cols[j], "TEXT");
	            	}            	
	            }
	            DatastoreSchema schema = new DatastoreSchema(ds.getDbName(), columns, ds);
	            schemas.add(schema);
	            br.close();
			}
	        fs.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return schemas;
	}

	public JSONObject getDataInstance(Datastore ds, String table) {
		JSONObject result = new JSONObject();
		
		DFSDriver driver = DFSDriver.getInstance();
		List<BufferedReader> brList = driver.getDataFile(ds);
		
		InstanceLevel conf = IntegratorUtil.confData.getInstanceLevel();
		int limit = conf.getNumberOfData();
		
		JSONObject metaObj = new JSONObject();
		metaObj.put("dbName", ds.getDbName());
		metaObj.put("table", table);
		metaObj.put("datastore", Datastore.DFS);
		result.put("meta", metaObj);
        
		JSONArray arr = new JSONArray();
		String line;
		int counter = 0;
		try {
			for(BufferedReader br : brList){				
				String[] cols =  br.readLine().split(",");
				while (( line = br.readLine()) != null && counter < limit){
					JSONObject obj = new JSONObject();
					for(int i = 0; i < cols.length; i++){
						Object val = line.split(",")[i];
						if(NumberUtils.isNumber(val.toString())){
							val = Double.parseDouble(val.toString());
						}
						obj.put(cols[i], val);
					}
					arr.put(obj);
					counter++;
				}
				result.put("result", arr);
				br.close();
			}
			driver.fs.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		return result;
	}
}

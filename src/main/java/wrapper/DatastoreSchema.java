package wrapper;

import java.util.Map;

import data.schema.Datastore;

public class DatastoreSchema {
	private String table;
	private Map<String, String> columns;
	private Datastore datastore;
	
	public DatastoreSchema(String table, Map<String, String> columns, Datastore datastore){
		this.table = table;
		this.columns = columns;
		this.datastore = datastore;
	}
	
	public String getTable() {
		return table;
	}
	
	public void setTable(String table) {
		this.table = table;
	}
	
	public Map<String, String> getColumns() {
		return columns;
	}
	
	public void setColumns(Map<String, String> columns) {
		this.columns = columns;
	}

	public Datastore getDatastore() {
		return datastore;
	}

	public void setDatastore(Datastore datastore) {
		this.datastore = datastore;
	}
	
	public String toString(){
		return table.concat("\n").concat(columns.toString()).concat("\n").concat(datastore.toString());
	}
}

package wrapper.MySQL;

import java.util.Map;

import data.schema.Datastore;
import wrapper.DatastoreSchema;

public class MySQLSchema extends DatastoreSchema{
	private String dbName;
	private Map<String, String> primaryKey;
	private Map<String, String> foreignKey;
	
	public MySQLSchema(String dbName, String table, Map<String, String> columns, Datastore datastore, Map<String, String> primaryKey, Map<String, String> foreignKey){
		super(table, columns, datastore);
		this.setDbName(dbName);
		this.primaryKey = primaryKey;
		this.setForeignKey(foreignKey);
	}

	public Map<String, String> getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(Map<String, String> primaryKey) {
		this.primaryKey = primaryKey;
	}

	public Map<String, String> getForeignKey() {
		return foreignKey;
	}

	public void setForeignKey(Map<String, String> foreignKey) {
		this.foreignKey = foreignKey;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	
}

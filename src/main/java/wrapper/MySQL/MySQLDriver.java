package wrapper.MySQL;

import org.json.*;

import data.datalog.FilterPredicate;
import data.schema.Datastore;
import data.schema.Query;
import integrator.config.InstanceLevel;
import integrator.config.IntegratorUtil;

import java.io.BufferedReader;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.sql.Connection;
import java.sql.DatabaseMetaData;
import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import utils.DatastoreUtil;
import utils.JsonUtil;

public class MySQLDriver {
	public Connection conn;
	public List<MySQLSchema> schemas;
	static String inputFile = ("conf/mySQL.schema");
	static String datastoreFile = ("conf/datastore");
	
	static List<String> dbList = new ArrayList<>();
	
	private static MySQLDriver instance = new MySQLDriver();
	
	public MySQLDriver() {}
	
	public static MySQLDriver getInstance(){
		return instance;
	}
	
	public void connect(String address, String user, String pass){
		try {
			conn =  DriverManager.getConnection(address, user, pass);
			getDB();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}	

	public void getDB(){
		DatabaseMetaData dbmd;

		try {
			dbmd = conn.getMetaData();
			ResultSet ctlgs = dbmd.getCatalogs();
			 
			while (ctlgs.next())
			{
			    dbList.add(ctlgs.getString(1));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
	}
	
	public void changeDB(String dbName){
		try {
			conn.setCatalog(dbName);
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public Map<String, String> getColumns(DatabaseMetaData md, String table){
		Map<String, String> cols = new LinkedHashMap<String, String>();
		try {
			ResultSet rsCol = md.getColumns(null, null, table, null);
			while (rsCol.next()) {
				//System.out.println(rsCol.getString("COLUMN_NAME"));
				cols.put(rsCol.getString("COLUMN_NAME"), rsCol.getString("TYPE_NAME"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return cols;
	}
	
	public Map<String, String> getPrimaryKeys(DatabaseMetaData md, String table){
		Map<String, String> primary = new LinkedHashMap<String, String>();
		try {
			ResultSet rs = md.getPrimaryKeys(null, null, table);
			while (rs.next()) {
				primary.put(rs.getString("COLUMN_NAME"), rs.getString("KEY_SEQ"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}		
		return primary;
	}
	
	public Map<String, String> getForeignKey(DatabaseMetaData md, String table){
		Map<String, String> foreign = new LinkedHashMap<>();
		try {
			ResultSet rs = md.getImportedKeys(null, null, table);
			while (rs.next()) {
				foreign.put(rs.getString("PKTABLE_NAME"), rs.getString("FKCOLUMN_NAME"));
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		return foreign;
	}
	
	public List<MySQLSchema> getSchema(Datastore ds){
//		System.out.println(">> getting schema from database " + ds.getDbName());
		List<MySQLSchema> schemas = new ArrayList<MySQLSchema>();
		try {
			String db = ds.getDbName();
			changeDB(db);
			DatabaseMetaData md = conn.getMetaData();
			ResultSet rs = md.getTables(null, null, "%", null);		
			
			while (rs.next()) {
				//make sure TABLE_TYPE is TABLE
				if(rs.getString("TABLE_TYPE").equals("TABLE")){
					String table = rs.getString("TABLE_NAME");
					MySQLSchema schema = new MySQLSchema(db, table, getColumns(md, table), ds, getPrimaryKeys(md, table), getForeignKey(md, table));
					schemas.add(schema);					
				}
			}
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}	
		return schemas;
	}
	
	public JSONObject getDataInstance(Datastore ds, String table){
		JSONObject result = new JSONObject();	

		InstanceLevel conf = IntegratorUtil.confData.getInstanceLevel();
		int limit = conf.getNumberOfData();
		String query = "SELECT * FROM " + table + " LIMIT " + limit;
		
		//execute query;
		Statement stmt = null;
		try {
			stmt = conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			result = MySQLWrapper.toJSON(rs, ds.getDbName(), table);
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
//	@SuppressWarnings("resource")
//	public static List<MySQLSchema> getSchemaFromFile(){
//		List<MySQLSchema> schemas = new ArrayList<MySQLSchema>();
//		BufferedReader br;
//		try {
//			br = new BufferedReader(new FileReader(inputFile));
//			StringBuilder sb = new StringBuilder();
//		    String line = br.readLine();
//
//		    while (line != null) {
//		        sb.append(line);
//		        sb.append(System.lineSeparator());
//		        line = br.readLine();
//		    }
//		    String str = sb.toString();
//		    
//		    JSONArray arr = new JSONArray(str);
////			System.out.println(str);
//			for(int i = 0; i < arr.length(); i++){
//				JSONObject obj = arr.getJSONObject(i);
//				JSONObject dsObj = obj.getJSONObject("datastore");
//				String db = dsObj.getString("dbName");
//				Datastore datastore = getDatastore(db);
//				MySQLSchema schema = new MySQLSchema(db, obj.get("table").toString(), JsonUtil.toMap(obj.getJSONObject("columns")), datastore, JsonUtil.toMap(obj.getJSONObject("primary")), JsonUtil.toMap(obj.getJSONObject("foreign")));
//				schemas.add(schema);
//			}
//					
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return schemas;
//	}
	
//	public void createSchemaFile(){
//		schemas = getSchema();
//		System.out.println(">> getting schema done");
//		
//		JSONArray arr = new JSONArray();
//		System.out.println(">> writin json obj");
//		for(MySQLSchema s : schemas){			
//			JSONObject tabObj = new JSONObject();
//			
//			JSONObject dbObj = new JSONObject();
//			Datastore ds = s.getDatastore();
//			dbObj.put("ID", ds.getID());
//			dbObj.put("address", ds.getAddress());
//			dbObj.put("dbName", ds.getDbName());
//			dbObj.put("database", ds.getDatabase());
//			tabObj.put("datastore", dbObj);	
//			
//			tabObj.put("table", s.getTable());	
//
//			JSONObject colObj = new JSONObject();			
//			Map<String, String> map = s.getColumns();			
//			for (Map.Entry<String, String> entry : map.entrySet()) {
//				colObj.put(entry.getKey(), entry.getValue());
//			}
//			tabObj.put("columns", colObj);
//			
//			tabObj.put("primary", s.getPrimaryKey());
//			tabObj.put("foreign", s.getForeignKey());
//			
//			arr.put(tabObj);
//		}
//		
//		try (FileWriter file = new FileWriter(inputFile)) {
//            file.write(arr.toString(4));
//            file.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//		
//		System.out.println("MySQL schema created");
//	}
	
//	@SuppressWarnings("resource")
//	private static Datastore getDatastore(String db){
//		Datastore datastore = null;
//		BufferedReader br;
//		
//		try {
//			br = new BufferedReader(new FileReader(datastoreFile));
//		    String line = br.readLine();
//		    
//		    while(line != null){
//		    	String[] str = line.split(" ");
//		    	if(str.length == 4){
//		    		if(str[3].equals(db) && str[2].equals(Datastore.MySQL)){
//		    			datastore = new Datastore();
//		    			datastore.setID(str[0]);
//				    	datastore.setAddress(str[1]);
//				    	datastore.setDatabase(str[2]);
//				    	datastore.setDbName(str[3]);
//				    	return datastore;
//		    		}
//		    	}
//		    	line = br.readLine();
//		    }
//		   
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		return datastore;
//	}
	
//	public JSONObject getDataInstance(String tableName){
//		JSONObject res = new JSONObject();
//		JSONArray arr = new JSONArray();
//		
//		JSONObject metaObj = new JSONObject();
//		metaObj.put("dbName", tableName.split("\\.")[0]);
//		metaObj.put("table", tableName.split("\\.")[1]);
//		metaObj.put("datastore", Datastore.MySQL);
//		res.put("meta", metaObj);
//		
//		try {
//			Statement stmt = conn.createStatement() ;
//			String query = "SELECT * FROM " + tableName + " LIMIT 100";
////			System.out.println(query);
//			ResultSet rs = stmt.executeQuery(query) ;
//			while (rs.next()) {
//				int numColumns = rs.getMetaData().getColumnCount();
//				JSONObject obj = new JSONObject();			
//				for ( int i = 1 ; i <= numColumns ; i++ ) {
//					obj.put(rs.getMetaData().getColumnName(i), rs.getObject(i));
////					System.out.println( "COLUMN " + rs.getMetaData().getColumnTypeName(i) + " = " + rs.getObject(i) );
//				}
//				arr.put(obj);
//				
//			}
//		} catch (SQLException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		res.put("result", arr);
//		
////		System.out.println(res.toString(4));
//		return res;
//	}
}

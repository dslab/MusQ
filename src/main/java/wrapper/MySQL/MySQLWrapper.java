package wrapper.MySQL;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import data.datalog.FilterPredicate;
import data.schema.Datastore;
import data.schema.SubQuery;

public class MySQLWrapper {
	public static JSONObject getData(SubQuery sq, Datastore ds){
		//connect to MySQL db
		MySQLDriver driver = MySQLDriver.getInstance();
		String address = "jdbc:mysql://"+ ds.getAddress() +"/";
		String user = ds.getUser();
		String pass = ds.getPass();
		driver.connect(address, user, pass);
		
		JSONObject result = new JSONObject();
		
		//parse query
		String query = parseQuery(sq, ds.getDbName());
		
		//execute query;
		Statement stmt = null;
		try {
			stmt = driver.conn.createStatement();
			ResultSet rs = stmt.executeQuery(query);
			
			result = toJSON(rs, ds.getDbName(), sq.getSource().getTable());
			driver.conn.close();
		} catch (SQLException e) {
			e.printStackTrace();
		}
		
		return result;
	}
	
	private static String parseQuery(SubQuery sq, String dbName){
		String query = "SELECT " + String.join(",", sq.getView().getColumns()) + " FROM " + dbName + "." + sq.getSource().getTable();
		
		if(sq.getFilter() != null){
			query += " WHERE ";
			int i = 1;
			for(FilterPredicate filter : sq.getFilter()){
				query += filter.toString();
				if(i <= sq.getFilter().size() - 1){
					query += " AND ";
				}
				i++;
			}
		}
		
//		System.out.println(query);
		return query;
	}
	
	public static JSONObject toJSON(ResultSet rs, String dbName, String table){
		JSONObject resultObj = new JSONObject();
		JSONArray arr = new JSONArray();
		
		JSONObject metaObj = new JSONObject();
		metaObj.put("dbName", dbName);
		metaObj.put("table", table);
		metaObj.put("datastore", Datastore.MySQL);
		resultObj.put("meta", metaObj);
		
		try {
			while (rs.next()) {
				int numColumns = rs.getMetaData().getColumnCount();
				JSONObject obj = new JSONObject();			
				for ( int i = 1 ; i <= numColumns ; i++ ) {
					obj.put(rs.getMetaData().getColumnName(i), rs.getObject(i));
				}
				arr.put(obj);				
			}
			
			resultObj.put("result", arr);
		} catch (JSONException | SQLException e) {
			e.printStackTrace();
		}
		
//		System.out.println(resultObj.toString(4));
		return resultObj;
	}
}

package wrapper.mongo;

import integrator.config.InstanceLevel;
import integrator.config.IntegratorUtil;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import org.apache.commons.lang.math.NumberUtils;
import org.bson.Document;
import org.json.JSONObject;

import wrapper.DatastoreSchema;

import com.mongodb.MongoClient;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.MongoDatabase;
import com.mongodb.client.model.Projections;

import data.datalog.FilterPredicate;
import data.schema.Datastore;
import data.schema.Query;
import data.schema.Schema;

public class MongoDriver {
	public MongoClient mongoClient;
	public static MongoDatabase db;
		
	private static MongoDriver instance = new MongoDriver();	
	
	public MongoDriver(){}
	
	public static MongoDriver getInstance(){
		return instance;
	}
	
	public void connect(String IP, int port){
		mongoClient = new MongoClient(IP, port);		
	}	

	public MongoDatabase getDB(String dbName){
		db = mongoClient.getDatabase(dbName);
		return db;
	}
	
	public static MongoCollection<Document> getCollection(String coll){
		return db.getCollection(coll);
	}
	
	private void getstats(){
		Document stats = db.runCommand(new Document("dbstats", 1));

        for (Map.Entry<String, Object> set : stats.entrySet()) {
                       
            System.out.format("%s: %s%n", set.getKey(), set.getValue());
        }
	}
	
	
	
	public List<DatastoreSchema> getSchema(Datastore ds){
		List<DatastoreSchema> schemas = new ArrayList<>();
		MongoDriver driver = MongoDriver.getInstance();
		driver.getDB(ds.getDbName());
		for(String table : db.listCollectionNames()){
			MongoCollection<Document> coll = MongoDriver.getCollection(table);
			Document doc = coll.find().projection(Projections.excludeId()).first();
			
			Map<String, String> columns = new LinkedHashMap<String, String>();
			for(String key: doc.keySet()){
				if(NumberUtils.isNumber(doc.get(key).toString())){
					columns.put(key, "NUMBER");
				}
				else{
					columns.put(key, "TEXT");
				}
//				System.out.println(key + " " + doc.get(key) + " " + NumberUtils.isNumber(doc.get(key).toString()));
			}
			DatastoreSchema schema = new DatastoreSchema(table, columns, ds);
            schemas.add(schema);
		}
		return schemas;
	}
	
	public JSONObject getDataInstance(Datastore ds, String table) {
		InstanceLevel conf = IntegratorUtil.confData.getInstanceLevel();
		int limit = conf.getNumberOfData();
		MongoCollection<Document> coll = MongoDriver.getCollection(table);
		MongoCursor<Document> cursor = coll.find().limit(limit).projection(Projections.excludeId()).iterator();
		JSONObject result = MongoWrapper.toJSON(cursor, ds.getDbName(), table);
		return result;
	}
	
	public static void main(String[] args){
		MongoDriver mongodriver = new MongoDriver();
		mongodriver.connect("164.125.121.115", 27017);
		mongodriver.getDB("sensor");
		
		Schema view = new Schema();
		view.setTable("SQ");
		List<String> cols = new ArrayList<>();
		cols.add("SID");
		cols.add("avgRate");
		view.setColumns(cols);
		
		List<Schema> relations = new ArrayList<>();
		Schema relation = new Schema();
		relation.setTable("batchwearable");
		cols = new ArrayList<>();
		cols.add("SID");
		cols.add("dt");
		cols.add("avgRate");
		cols.add("minRate");
		cols.add("maxRate");
		relation.setColumns(cols);
		relation.setDatastore("DB5");
		relations.add(relation);
		
		
		List<FilterPredicate> filters = new ArrayList<>();
		FilterPredicate filter = new FilterPredicate("SID", "=", "S0001");
//		filters.add(filter);
//		filter = new FilterPredicate("dt", "=", "2015-02-01");
//		filter = new FilterPredicate("avgRate", ">", "85");
//		filters.add(filter);
		
		Query sq = new Query();
		sq.setView(view);
		sq.setSource(relations);
		sq.setFilter(filters);
		
//		mongodriver.getData(sq);
		
//		mongodriver.getstats();
		
//		coll = mongodriver.getCollection("batchwearable");
//		MongoCursor<Document> cursor = coll.find().iterator();
//		while (cursor.hasNext()) {
//	        System.out.println(cursor.next().toJson());
//	    }
	}	
}

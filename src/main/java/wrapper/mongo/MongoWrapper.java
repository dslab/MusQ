package wrapper.mongo;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.math.NumberUtils;
import org.bson.Document;
import org.bson.conversions.Bson;
import org.json.JSONArray;
import org.json.JSONObject;

import com.mongodb.BasicDBObject;
import com.mongodb.client.MongoCollection;
import com.mongodb.client.MongoCursor;
import com.mongodb.client.model.Projections;

import data.datalog.FilterPredicate;
import data.schema.Datastore;
import data.schema.SubQuery;

public class MongoWrapper {

	public static JSONObject getData(SubQuery sq, Datastore ds) {
		MongoDriver driver = MongoDriver.getInstance();
		driver.connect(ds.getAddress(), 27017);
		driver.getDB(ds.getDbName());

		String table = sq.getSource().getTable();
		
		MongoCollection<Document> coll = MongoDriver.getCollection(table);
		MongoCursor<Document> cursor = null;
		
		if(sq.getFilter() != null && sq.getFilter().size() > 0){
			BasicDBObject filter = parseFilterQuery(sq);			
			cursor = coll.find((Bson) filter).projection(Projections.fields(Projections.include(sq.getView().getColumns()), Projections.excludeId())).iterator();
		}
		else{
			cursor = coll.find().projection(Projections.fields(Projections.include(sq.getView().getColumns()), Projections.excludeId())).iterator();
		}
		
		JSONObject result = toJSON(cursor, ds.getDbName(), sq.getSource().getTable());
//		System.out.println(result.toString());
		return result;
	}
	private static BasicDBObject parseFilterQuery(SubQuery sq){
		BasicDBObject qFilter = new BasicDBObject();	
		
		if(sq.getFilter().size() > 0){
			
			List<BasicDBObject> andQuery = new ArrayList<BasicDBObject>();
			
			for(FilterPredicate filter : sq.getFilter()){				
				BasicDBObject qObj = getDBObject(filter);
				andQuery.add(qObj);
				
				if(sq.getFilter().size() == 1){
					qFilter = getDBObject(filter);
				}				
			}
			
			if(sq.getFilter().size() > 1){
				qFilter.put("$and", andQuery);
			}
		}
//		System.out.println(qFilter);
		
		return qFilter;
	}
	
	public static JSONObject toJSON(MongoCursor<Document> cursor, String dbName, String table){
		JSONObject result = new JSONObject();
		JSONArray arr = new JSONArray();
		
		JSONObject metaObj = new JSONObject();
		metaObj.put("dbName", dbName);
		metaObj.put("table", table);
		metaObj.put("datastore", Datastore.Mongo);
		result.put("meta", metaObj);
		
		while (cursor.hasNext()) {
			Document doc = cursor.next();
//			System.out.println(doc.keySet().toString());
			
			String s = doc.toJson();
//			System.out.println(s);
			JSONObject obj = new JSONObject(s);
			arr.put(obj);
		}
		
		result.put("result", arr);
		
		return result;
	}
	
	private static BasicDBObject getDBObject(FilterPredicate filter){
		String col = filter.getColumn();
		String op = filter.getOperator();
		String conds = filter.getCondition();
		
		BasicDBObject query = new BasicDBObject();
		BasicDBObject obj;
		Object cond = 0.0;
		if(isNumber(conds)){
			cond = Double.parseDouble(conds);
		}
//		else if(conds.startsWith("'") && conds.endsWith("'")){
//			cond = conds.replaceAll("'", "");
//		}
//		System.out.println(cond);
		
		switch(op){
			case "=":
				if(conds.startsWith("'") && conds.endsWith("'")){
					cond = conds.replaceAll("'", "");
				}
				query.put(col, cond);
				break;
			case "<":
				if(conds.startsWith("'") && conds.endsWith("'")){
					cond = conds.replaceAll("'", "\"");
				}
				obj = BasicDBObject.parse("{$lt: " + cond + "}");
				query.put(col, obj);
				break;
			case ">":
				if(conds.startsWith("'") && conds.endsWith("'")){
					cond = conds.replaceAll("'", "\"");
				}
				obj = BasicDBObject.parse("{$gt: " + cond + "}");
				query.put(col, obj);
				break;
			case "<=":
				if(conds.startsWith("'") && conds.endsWith("'")){
					cond = conds.replaceAll("'", "\"");
				}
//				System.out.println("{$lte: " + cond + "}");
				obj = BasicDBObject.parse("{$lte: " + cond + "}");
				query.put(col, obj);
				break;
			case ">=":
				if(conds.startsWith("'") && conds.endsWith("'")){
					cond = conds.replaceAll("'", "\"");
				}
//				System.out.println("{$lte: " + cond + "}");
				obj = BasicDBObject.parse("{$gte: " + cond + "}");
				query.put(col, obj);
				break;
		}
		return query;
	}
	
	private static boolean isNumber(String s){
//		return StringUtils.isNumeric(s);
		return NumberUtils.isNumber(s);
	}
}

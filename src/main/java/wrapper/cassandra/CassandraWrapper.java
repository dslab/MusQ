package wrapper.cassandra;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.List;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;

import data.datalog.FilterPredicate;
import data.schema.Datastore;
import data.schema.SubQuery;

public class CassandraWrapper {
	static List<CassandraSchema> schemas;
	public static JSONObject getData(SubQuery sq, Datastore ds){
		CassandraDriver driver = CassandraDriver.getInstance();
		
		if(driver.cluster == null || driver.cluster.isClosed())
			driver.connect(ds.getAddress(), ds.getDbName());
		
		schemas = driver.getSchema(driver.cluster, ds);
		
		String query = parseQuery(sq);
		
		ResultSet rs = driver.session.execute(query);
		
		JSONObject result = toJSON(rs, ds.getDbName(), sq.getSource().getTable());

//		driver.cluster.close();
		
		return result;
	}
	
	private static String parseQuery(SubQuery sq){
		String query = "";
		
		String table = sq.getSource().getTable();		
		query = "SELECT " + String.join(",", sq.getView().getColumns()) + " FROM " + table;
		
		if(sq.getFilter() != null){
			if(sq.getFilter().size() > 0){
				query += " WHERE ";
				int i = 1;
				
				List<String> filterCols = new ArrayList<>();
				
				for(FilterPredicate filter : sq.getFilter()){
					query += filter.toString();
					if(i <= sq.getFilter().size() - 1){
						query += " AND ";
					}
	
					filterCols.add(filter.getColumn());
					i++;
				}
				if(isFilterRestricted(table, filterCols, sq.getFilter())){
					query += " ALLOW FILTERING;";
				}
			}
		}
//		System.out.println(query);
		return query;
	}
	
	public static JSONObject toJSON(ResultSet rs, String dbName, String table){
		JSONObject result = new JSONObject();
		JSONArray arr = new JSONArray();
		
		JSONObject metaObj = new JSONObject();
		metaObj.put("dbName", dbName);
		metaObj.put("table", table);
		metaObj.put("datastore", Datastore.CASSANDRA);
		result.put("meta", metaObj);
		
		for (Row row : rs){
//			JSONObject obj = new JSONObject(row.getString(0));
//			arr.put(obj);
			JSONObject obj = new JSONObject();
			obj = getData(row);
			arr.put(obj);
		}
		
		result.put("result", arr);
		return result;
	}
	
	private static JSONObject getData(Row row){
		JSONObject obj = new JSONObject();
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
		for(int i = 0; i < row.getColumnDefinitions().size(); i++){
			switch(row.getColumnDefinitions().getType(i).toString()){
				case "ascii":				
				case "inet":
				case "text":				
				case "varchar":
					obj.put(row.getColumnDefinitions().getName(i), row.getString(i));
					break;
				case "date":
					obj.put(row.getColumnDefinitions().getName(i), sdf.format(new Date(row.getDate(i).getMillisSinceEpoch())));
					break;
				case "time":					
					obj.put(row.getColumnDefinitions().getName(i), sdf.format(new Date(row.getTime(i))));
					break;
				case "timestamp":
					obj.put(row.getColumnDefinitions().getName(i), sdf.format(new Date(row.getTimestamp(i).getTime())));
					break;
				case "bigint":
				case "counter":
				case "smallint":
				case "varint":
				case "tinyint":
				case "int":
					obj.put(row.getColumnDefinitions().getName(i), row.getInt(i));
					break;				
				case "boolean":
					obj.put(row.getColumnDefinitions().getName(i), row.getBool(i));
					break;			
				case "decimal":
					obj.put(row.getColumnDefinitions().getName(i), row.getDecimal(i));
					break;
				case "double":
					obj.put(row.getColumnDefinitions().getName(i), row.getDouble(i));
					break;
				case "float":
					obj.put(row.getColumnDefinitions().getName(i), row.getFloat(i));
					break;				
				case "list":
				case "map":
				case "set":
				case "tuple":
				case "frozen":
				case "blob":
					obj.put(row.getColumnDefinitions().getName(i), row.getObject(i));
					break;
				case "timeuuid":
				case "uuid":
					obj.put(row.getColumnDefinitions().getName(i), row.getUUID(i));
					break;
			}
		}
		return obj;
	}
	
	public static boolean isFilterRestricted(String table, List<String> column, List<FilterPredicate> filters){
		boolean isKey = false;

		Set<String> keys = null;
		for(CassandraSchema schema : schemas){
			if(schema.getTable().equalsIgnoreCase(table)){
				keys = schema.getPartitionKey().keySet();
				break;
			}
		}
	//	System.out.println(column + " " + keys);
		if(column.containsAll(keys)){
			
			for(FilterPredicate filter : filters){
				if(keys.contains(filter.getColumn()) && filter.getCondition().equals("=")){
					isKey = true;
				}
				else{
					isKey = false;
				}
			}
		}
		else{
			isKey = false;
		}
		
	//	System.out.println(keys.toString() + " " + column + " " + keys.contains(column));
		for(String col : column){
	//		System.out.println(isKey + " "+ col + " " + keys.toString() + " " + keys.contains(col));
			if(!keys.contains(col)){
				return true;
			}
		}
		
		return !isKey;
	}
}

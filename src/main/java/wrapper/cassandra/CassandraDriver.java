package wrapper.cassandra;

import integrator.config.InstanceLevel;
import integrator.config.IntegratorUtil;

import java.util.ArrayList;
import java.util.Collection;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;
import java.util.Set;

import org.json.JSONArray;
import org.json.JSONObject;

import com.datastax.driver.core.Cluster;
import com.datastax.driver.core.ColumnMetadata;
import com.datastax.driver.core.KeyspaceMetadata;
import com.datastax.driver.core.ResultSet;
import com.datastax.driver.core.Row;
import com.datastax.driver.core.Session;
import com.datastax.driver.core.TableMetadata;

import data.schema.Datastore;

public class CassandraDriver {
	public Cluster cluster;
	public Session session;	
	
	private static CassandraDriver instance = new CassandraDriver();
	
	public CassandraDriver() {}
	
	public static CassandraDriver getInstance(){
		return instance;
	}

	public void connect(String IP, String keyspace){
		cluster = Cluster.builder()
		        .withClusterName("Cluster")
		        .addContactPoint(IP)
		        .build();
		session = cluster.connect(keyspace);
	}
	
	public void close(){
		cluster.close();
	}
	
	public List<CassandraSchema> getSchema(Cluster cluster, Datastore ds){
		String keyspace = ds.getDbName();
		KeyspaceMetadata meta = cluster.getMetadata().getKeyspace(keyspace);
		Collection<TableMetadata> tabMeta = meta.getTables();
		
		List<CassandraSchema> schemas = new ArrayList<CassandraSchema>();		
		for(TableMetadata m : tabMeta){	
			CassandraSchema schema = new CassandraSchema(ds.getID(), m.getName(), getColumns(m), ds, getPrimaryKey(m), getPartitionKey(m));
			schemas.add(schema);
//			System.out.println(schema.toString() + "\n");
		}
		return schemas;
	}
	
	public Map<String, String> getColumns(TableMetadata m){
		Map<String, String> cols = new LinkedHashMap<String, String>();
		for(ColumnMetadata col : m.getColumns()){
			cols.put(col.getName(), col.getType().toString());				
		}
		return cols;
	}
	
	public Map<String, String> getPrimaryKey(TableMetadata m){
		Map<String, String> primary = new LinkedHashMap<String, String>();
		for(ColumnMetadata col : m.getPrimaryKey()){
			primary.put(col.getName(), col.getType().toString());
		}
		return primary;
	}
	
	public Map<String, String> getPartitionKey(TableMetadata m){
		Map<String, String> partition = new LinkedHashMap<String, String>();
//		System.out.println("partition " + m.getPartitionKey());
		for(ColumnMetadata col : m.getPartitionKey()){
			partition.put(col.getName(), col.getType().toString());
		}
		return partition;
	}
	
	public JSONObject getDataInstance(Datastore ds, String table){
		JSONObject result = new JSONObject();
		
		InstanceLevel conf = IntegratorUtil.confData.getInstanceLevel();
		int limit = conf.getNumberOfData();
		String query = "SELECT * FROM " + table + " LIMIT " + limit;
		
		ResultSet rs = session.execute(query);
		
		result = CassandraWrapper.toJSON(rs, ds.getDbName(), table);
		return result;
	}
	
//	public List<CassandraSchema> schemas;
//	static String inputFile = ("conf/cassandra.schema");
//	static String datastoreFile = ("conf/datastore");
//	static String dbName = "traffic";
//	@SuppressWarnings("resource")
//	public static List<CassandraSchema> getSchemaFromFile(){
//		List<CassandraSchema> schemas = new ArrayList<CassandraSchema>();
//		BufferedReader br;
//		try {
//			br = new BufferedReader(new FileReader(inputFile));
//			StringBuilder sb = new StringBuilder();
//		    String line = br.readLine();
//
//		    while (line != null) {
//		        sb.append(line);
//		        sb.append(System.lineSeparator());
//		        line = br.readLine();
//		    }
//		    String str = sb.toString();
//		    
//		    JSONArray arr = new JSONArray(str);
////			System.out.println(str);
//			for(int i = 0; i < arr.length(); i++){
//				JSONObject obj = arr.getJSONObject(i);
////				Map<String,String> columns = new LinkedHashMap<>();
//				Datastore datastore = getDatastore(dbName);
//				CassandraSchema schema = new CassandraSchema(getDBID(), obj.get("table").toString(), JsonUtil.toMap(obj.getJSONObject("columns")), datastore, JsonUtil.toMap(obj.getJSONObject("primary")), JsonUtil.toMap(obj.getJSONObject("partition")));
//				schemas.add(schema);
//			}
//					
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		return schemas;
//	}
	
//	public void createSchemaFile(){
//		schemas = getSchema(cluster, "traffic");
//		JSONArray arr = new JSONArray();
//		for(CassandraSchema s : schemas){
//			JSONObject tabObj = new JSONObject();
//			
//			JSONObject dbObj = new JSONObject();
//			Datastore ds = s.getDatastore();
//			dbObj.put("ID", ds.getID());
//			dbObj.put("address", ds.getAddress());
//			dbObj.put("dbName", ds.getDbName());
//			dbObj.put("database", ds.getDatabase());
//			tabObj.put("datastore", dbObj);	
//			
//			tabObj.put("table", s.getTable());					
//			
//			JSONObject colObj = new JSONObject();
//			
//			Map<String, String> map = s.getColumns();			
//			for (Map.Entry<String, String> entry : map.entrySet()) {
//				colObj.put(entry.getKey(), entry.getValue());
//			}
//			tabObj.put("columns", colObj);
//			
//			tabObj.put("primary", s.getPrimaryKey());
//			tabObj.put("partition", s.getPartitionKey());
//			
//			arr.put(tabObj);
//		}
//
//		try (FileWriter file = new FileWriter(inputFile)) {
//            file.write(arr.toString(4));
//            file.flush();
//        } catch (IOException e) {
//            e.printStackTrace();
//        }
//		
//		System.out.println("Cassandra schema created");
//	}
	
//	@SuppressWarnings("resource")
//	private static String getDBID(){
//		String db = "";
//		String file = ("conf/datastore");
//		BufferedReader br;
//		try {
//			br = new BufferedReader(new FileReader(file));
//			String line = br.readLine();
//		    
//		    while(line != null){
//		    	String[] str = line.split(" ");
//		    	if(str.length == 4){		    		
//		    		if(str[3].equals(dbName)){
//		    			db = str[0].concat(".").concat(dbName);
//		    			return db;
//		    		}
//		    	}
//		    	line = br.readLine();
//		    }
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		    
//		return db;
//	}
//	
//	@SuppressWarnings("resource")
//	private static Datastore getDatastore(String db){
//		Datastore datastore = null;
//		BufferedReader br;
//		
//		try {
//			br = new BufferedReader(new FileReader(datastoreFile));
//		    String line = br.readLine();
//		    
//		    while(line != null){
//		    	String[] str = line.split(" ");
//		    	if(str.length == 4){
////		    		System.out.println(db + str[0] + " " + str[1] + " " + str[2] + " " + str[3]);
//		    		if(str[3].equals(db) && str[2].equals(Datastore.CASSANDRA)){
//		    			datastore = new Datastore();
//		    			datastore.setID(str[0]);
//				    	datastore.setAddress(str[1]);
//				    	datastore.setDatabase(str[2]);
//				    	datastore.setDbName(str[3]);
//				    	return datastore;
//		    		}
//		    	}
//		    	line = br.readLine();
//		    }
//		   
//		} catch (IOException e) {
//			// TODO Auto-generated catch block
//			e.printStackTrace();
//		}
//		
//		return datastore;
//	}
	
//	public JSONObject getDataInstance(String tableName){
//		String query = "select JSON * from " + tableName + " limit 100";
//		JSONObject res = getData(tableName, query);
//		return res;
//	}
//	
//	private JSONObject getData(String tableName, String query){
//		JSONObject res = new JSONObject();
//		JSONArray arr = new JSONArray();
//		
//		JSONObject metaObj = new JSONObject();
//		metaObj.put("dbName", dbName);
//		metaObj.put("table", tableName);
//		metaObj.put("datastore", Datastore.CASSANDRA);
//		res.put("meta", metaObj);
//
//		ResultSet rs = session.execute(query);
//		for (Row row : rs){
////			System.out.println();
//			JSONObject obj = new JSONObject(row.getString(0));			
//			arr.put(obj);
//		}
		
//		for (Row row : rs) {
////			System.out.println(row);
//			JSONObject obj = new JSONObject();
//		    for (ColumnDefinitions.Definition definition : row.getColumnDefinitions()) {
////		        System.out.printf("Column %s has type %s%n",
////		                definition.getName(),
////		                definition.getType().toString());
//		        String col = definition.getName();
//		        //String val = "";
//		        switch(definition.getType().toString()){
//			        case "varchar":
//			        case "text":
////			        	val = row.getString(col);
//			        	obj.put(col, row.getString(col));
//			        	break;
//			        case "int":
////			        	val = Integer.toString(row.getInt(col));
//			        	obj.put(col, row.getInt(col));
//			        	break;
//			        case "date":
////			        	val = row.getDate(col).toString();
//			        	obj.put(col, row.getDate(col));
//			        	break;
//			        case "time":
////			        	val = Long.toString(row.getTime(col));
//			        	obj.put(col, row.getTime(col));
//			        	break;
//			        case "double":
//			        	obj.put(col, row.getDouble(col));
//			        	break;
//			        default:
////			        	val = row.getMap(col, String.class, String.class);
//			        	obj.put(col, row.getMap(col, String.class, String.class));
//			        	break;
//		        }
////		        obj.put(col, val);
////		        System.out.println(col + " " + val);
////		        Class c = definition.getType().asJavaClass();
////		        System.out.println(row.get()));
//		    }
//		    arr.put(obj);
//		}
//		res.put("result", arr);
//		System.out.println(res.toString(4));
//		return res;
//	}
	
//	public static boolean isFilterRestricted(String table, List<String> column){
//		boolean isKey = false;
//		List<CassandraSchema> cassandraSchema = getSchemaFromFile();
//		
//		Set<String> keys = null;
//		for(CassandraSchema schema : cassandraSchema){
////			System.out.println(schema.getTable() + " " + table);
//			if(schema.getTable().equalsIgnoreCase(table)){
//				keys = schema.getPartitionKey().keySet();
//				break;
//			}
//		}
////		System.out.println(column + " " + keys);
//		if(column.containsAll(keys)){
//			isKey = true;
//		}
//		else{
//			isKey = false;
//		}
//		
////		System.out.println(keys.toString() + " " + column + " " + keys.contains(column));
//		for(String col : column){
////			System.out.println(isKey + " "+ col + " " + keys.toString() + " " + keys.contains(col));
//			if(!keys.contains(col)){
//				return true;
//			}
//		}
//		
//		return !isKey;
//	}
	
//	public static void main(String[] args){
//		CassandraDriver driver = CassandraDriver.getInstance();
//		
//		driver.connect("164.125.37.223", "traffic");
		
//		driver.getDataInstance("readings");
//		driver.createSchemaFile();
//		driver.close();
//		List<CassandraSchema> schemas = driver.getSchema(cluster, "sensor");		
//		driver.createSchemaFile(schemas);
//		return;
//	}
}

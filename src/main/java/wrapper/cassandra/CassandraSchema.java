package wrapper.cassandra;

import java.util.Map;

import data.schema.Datastore;
import wrapper.DatastoreSchema;

public class CassandraSchema extends DatastoreSchema{
	private String dbName;
	private Map<String, String> primaryKey;
	private Map<String, String> partitionKey;
	
	public CassandraSchema(String dbName, String table, Map<String, String> columns, Datastore datastore, Map<String, String> primaryKey, Map<String, String> partitionKey){
		super(table, columns, datastore);
		this.dbName = dbName;
		this.primaryKey = primaryKey;
		this.partitionKey = partitionKey;
	};

	public Map<String, String> getPrimaryKey() {
		return primaryKey;
	}

	public void setPrimaryKey(Map<String, String> primaryKey) {
		this.primaryKey = primaryKey;
	}

	public Map<String, String> getPartitionKey() {
		return partitionKey;
	}

	public void setPartitionKey(Map<String, String> partitionKey) {
		this.partitionKey = partitionKey;
	}

	public String getDbName() {
		return dbName;
	}

	public void setDbName(String dbName) {
		this.dbName = dbName;
	}
	
	
}

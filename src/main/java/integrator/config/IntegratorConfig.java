package integrator.config;

public class IntegratorConfig {
	private double threshold;
	private SchemaLevel schemaLevel;
	private InstanceLevel instanceLevel;
	
	public double getThreshold() {
		return threshold;
	}
	
	public void setThreshold(double threshold) {
		this.threshold = threshold;
	}
	
	public SchemaLevel getSchemaLevel() {
		return schemaLevel;
	}	
	public void setSchemaLevel(SchemaLevel schemaLevel) {
		this.schemaLevel = schemaLevel;
	}
	
	public InstanceLevel getInstanceLevel() {
		return instanceLevel;
	}
	
	public void setInstanceLevel(InstanceLevel instanceLevel) {
		this.instanceLevel = instanceLevel;
	}
	
	@Override
	public String toString(){
		return schemaLevel.toString().concat("\n").concat(instanceLevel.toString());
	}	
}

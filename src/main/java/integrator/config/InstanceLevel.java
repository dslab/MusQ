package integrator.config;

public final class InstanceLevel {
	private boolean enabled;
	private double weight;
	private int numberOfData;
	
	public boolean getEnabled() {
		return enabled;
	}
	
	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}

	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}

	public int getNumberOfData() {
		return numberOfData;
	}

	public void setNumberOfData(int numberOfData) {
		this.numberOfData = numberOfData;
	}
	
	@Override
	public String toString(){
		String s = "Instance Level :";
		s += "\n\t enabled : " + enabled;
		s += "\n\t weight : " + weight;
		s += "\n\t number of data : " + numberOfData;
		return s;
	}
}

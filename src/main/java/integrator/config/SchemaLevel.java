package integrator.config;

import java.util.List;

public final class SchemaLevel {
	private boolean enabled;
	private double weight;
	private List<Similarity> similarity;
	
	public boolean getEnabled() {
		return enabled;
	}

	public void setEnabled(boolean enabled) {
		this.enabled = enabled;
	}
	
	public double getWeight() {
		return weight;
	}

	public void setWeight(double weight) {
		this.weight = weight;
	}
	
	public List<Similarity> getSimilarity() {
		return similarity;
	}

	public void setSimilarity(List<Similarity> similarity) {
		this.similarity = similarity;
	}
	
	@Override
	public String toString(){
		String s = "Schema Level :";
		s += "\n\t enabled : " + enabled;
		s += "\n\t weight : " + weight;
		s += "\n\t similarity : " + similarity.toString();
		return s;
	}	
}

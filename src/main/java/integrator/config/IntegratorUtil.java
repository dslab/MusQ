package integrator.config;

import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;

import org.yaml.snakeyaml.Yaml;

public class IntegratorUtil {
	private static final String configPath = "conf/integrator.yml";
	public static IntegratorConfig confData;
	public static void main(String[] args) {
		// TODO Auto-generated method stub
		readConfig();
	}
	public static void readConfig(){
        InputStream input;
        try {
			input = new FileInputStream(new File(configPath));
			Yaml yaml = new Yaml();
            confData = yaml.loadAs(input, IntegratorConfig.class);
            System.out.println(confData.toString());
            input.close();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}                     
    }
}

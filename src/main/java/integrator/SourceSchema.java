package integrator;

import java.util.Map;

import data.schema.Datastore;

public class SourceSchema {
	private String table;
	private Map<String, String> columns;
	private Datastore datastore;
	
	public String getTable() {
		return table;
	}
	
	public void setTable(String table) {
		this.table = table;
	}
	
	public Map<String, String> getColumns() {
		return columns;
	}
	
	public void setColumns(Map<String, String> columns) {
		this.columns = columns;
	}

	public Datastore getDatastore() {
		return datastore;
	}

	public void setDatastore(Datastore datastore) {
		this.datastore = datastore;
	}
	
	public String toString(){
		String s = datastore.getDatabase() + "\t" + table + "\n";
		for(String col: columns.keySet()){
			s += col + ":" + columns.get(col) + "\t";
		}
		s += "\n";
//		s += "\n" + datastore.toString();
		return s;
	}
	
}

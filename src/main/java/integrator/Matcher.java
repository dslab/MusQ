package integrator;

import integrator.config.InstanceLevel;
import integrator.config.IntegratorUtil;
import integrator.config.SchemaLevel;

import java.sql.SQLException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import javax.swing.JOptionPane;

import org.json.JSONObject;

import wrapper.DatastoreSchema;
import wrapper.DFS.DFSDriver;
import wrapper.MySQL.MySQLDriver;
import wrapper.MySQL.MySQLSchema;
import wrapper.cassandra.CassandraDriver;
import wrapper.cassandra.CassandraSchema;
import wrapper.mongo.MongoDriver;
import data.schema.Datastore;

public class Matcher {	
	static List<MySQLSchema> mySqlSchema = new ArrayList<>();
	static List<CassandraSchema> cassandraSchema = new ArrayList<>();
	static List<DatastoreSchema> mongoSchema = new ArrayList<>();
	static List<DatastoreSchema> HDFSSchema = new ArrayList<>();
	public static List<SourceSchema> schemaList = new ArrayList<>();
	
	List<JSONObject> jsonList = new ArrayList<>();
	
	private static Matcher instance = new Matcher();
	
	public Matcher(){}
	
	public static Matcher getInstance(){
		return instance;
	}
	
	public void getMySQLSchema(Datastore ds, boolean getInstance){
		MySQLDriver driver = MySQLDriver.getInstance();
		try {
			if(driver.conn == null || driver.conn.isClosed()){
				String address = "jdbc:mysql://"+ ds.getAddress() +"/";
				String user = ds.getUser();
				String pass = ds.getPass();
				driver.connect(address, user, pass);				
			}
			
			mySqlSchema.addAll(driver.getSchema(ds));
			
			if(getInstance){
				getMySQLInstance(ds, driver.getSchema(ds));
			}
			
			driver.conn.close();
		} catch (SQLException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
	}
	
	public void getCassandraSchema(Datastore ds, boolean getInstance){
		CassandraDriver driver = CassandraDriver.getInstance();
		if(driver.cluster == null || driver.cluster.isClosed())
			driver.connect(ds.getAddress(), ds.getDbName());
		
		cassandraSchema.addAll(driver.getSchema(driver.cluster, ds));
		
		if(getInstance){
			getCassandraInstance(ds, driver.getSchema(driver.cluster, ds));
		}
		driver.close();
	}
	
	public void getMongoSchema(Datastore ds, boolean getInstance){
		MongoDriver driver = MongoDriver.getInstance();
		if(driver.mongoClient == null){
			driver.connect(ds.getAddress(), 27017);
		}
		mongoSchema.addAll(driver.getSchema(ds));
		
		if(getInstance){
			getMongoInstance(ds, driver.getSchema(ds));
		}
		
		driver.mongoClient.close();
	}
	
	public void getHDFSSchema(Datastore ds, boolean getInstance){
		DFSDriver driver = DFSDriver.getInstance();
		HDFSSchema.addAll(driver.getSchema(ds));
		
		if(getInstance){
			getHDFSInstance(ds, driver.getSchema(ds));
		}
	}
	
	public List<SourceSchema> combineSchema(){
		//combine all schema
		schemaList = new ArrayList<>();
		for(MySQLSchema schema: mySqlSchema){
			SourceSchema s = new SourceSchema();
			s.setTable(schema.getTable());
			s.setColumns(schema.getColumns());
			s.setDatastore(schema.getDatastore());
			
			schemaList.add(s);
		}
		
		for(CassandraSchema schema : cassandraSchema){
			SourceSchema s = new SourceSchema();
			s.setTable(schema.getTable());
			s.setColumns(schema.getColumns());
			s.setDatastore(schema.getDatastore());
			schemaList.add(s);
		}
		
		for(DatastoreSchema schema : HDFSSchema){
			SourceSchema s = new SourceSchema();
			s.setTable(schema.getTable());
			s.setColumns(schema.getColumns());
			s.setDatastore(schema.getDatastore());
			schemaList.add(s);
		}
		
		for(DatastoreSchema schema : mongoSchema){
			SourceSchema s = new SourceSchema();
			s.setTable(schema.getTable());
			s.setColumns(schema.getColumns());
			s.setDatastore(schema.getDatastore());
			schemaList.add(s);
		}
		
		return schemaList;
	}
	
	private void getMySQLInstance(Datastore ds, List<MySQLSchema> schemas){
		for(MySQLSchema schema : schemas){
			MySQLDriver driver = MySQLDriver.getInstance();
			JSONObject data = driver.getDataInstance(ds, schema.getTable());
			jsonList.add(data);
		}
	}
	
	private void getCassandraInstance(Datastore ds, List<CassandraSchema> schemas){
		for(CassandraSchema schema : schemas){
			CassandraDriver driver = CassandraDriver.getInstance();
			JSONObject data = driver.getDataInstance(ds, schema.getTable());
			jsonList.add(data);
		}
	}
	
	private void getMongoInstance(Datastore ds, List<DatastoreSchema> schemas){
		for(DatastoreSchema schema : schemas){
			MongoDriver driver = MongoDriver.getInstance();
			JSONObject data = driver.getDataInstance(ds, schema.getTable());
			jsonList.add(data);
		}
	}
	
	private void getHDFSInstance(Datastore ds, List<DatastoreSchema> schemas){
		for(DatastoreSchema schema : schemas){
			DFSDriver driver = DFSDriver.getInstance();
			JSONObject data = driver.getDataInstance(ds, schema.getTable());
			jsonList.add(data);
		}
	}
	
	public List<SimilarityScore> calculate(List<SourceSchema> schemas){
//		IntegratorUtil.readConfig();
		SchemaLevel schemaConf = IntegratorUtil.confData.getSchemaLevel();
		InstanceLevel instanceConf = IntegratorUtil.confData.getInstanceLevel();
		
		SchemaMatcher schemaMatch = new SchemaMatcher();
		InstanceMatcher instanceMatch = new InstanceMatcher();
		
		List<SimilarityScore> schemaScore = new ArrayList<>();
		List<SimilarityScore> instanceScore = new ArrayList<>();
		
		for(int i = 0; i < schemas.size() - 1; i++){
			for(int j = i + 1; j < schemas.size(); j++){
				List<SimilarityScore> score1 = schemaMatch.match(schemas.get(i), schemas.get(j));
				schemaScore.addAll(score1);
				List<SimilarityScore> score2 = instanceMatch.match(schemas.get(i), schemas.get(j));
				instanceScore.addAll(score2);
			}
		}
		
		List<SimilarityScore> combinedScore = new ArrayList<>();
		for(SimilarityScore sc1 : schemaScore){
			SimilarityScore score = getScore(sc1, instanceScore);
			double total = sc1.getSimScore();
			if(score != null){
				total = (total * schemaConf.getWeight()) + (score.getSimScore() * instanceConf.getWeight());
				
			}
			else{
				total = (total * schemaConf.getWeight()) + 0.0;
			}

			
			if(total > IntegratorUtil.confData.getThreshold()){
				sc1.setSimScore(total);
				combinedScore.add(sc1);				
			}

		}
		
		
		//sort score in reversed order
		Collections.sort(combinedScore);
		return combinedScore;		
	}
	
//	public String getDataType(Schema schema, String col){
//		String type = "";
//		System.out.println(schema.getDatastore());
//		switch(schema.getDatastore().getDatabase()){
//			case Datastore.MySQL:
//				for(MySQLSchema mysql: mySqlSchema){
//					type = mysql.getColumns().get(col);
//					break;
//				}
//				break;
//			case Datastore.CASSANDRA:
//				for(CassandraSchema cassandra: cassandraSchema){
//					type = cassandra.getColumns().get(col);
//					break;
//				}
//				break;
//		}
//		return type;
//	}
	
//	private List<SimilarityScore> schemaLevelMatch(){
//		NameMatcher nameMatch = new NameMatcher();
//		List<SimilarityScore> nameScore = new ArrayList<>();
//		
//		for(int i = 0; i < schemaList.size() - 1; i++){
//			for(int j = i + 1; j < schemaList.size(); j++){
//				List<SimilarityScore> score = nameMatch.match(schemaList.get(i), schemaList.get(j));
//				nameScore.addAll(score);
//			}
//		}
//		
//		return nameScore;
//	}
	
//	private List<SimilarityScore> instanceLevelMatch() throws SQLException{		
//		List<SimilarityScore> instanceScore = new ArrayList<>();
//		DataMatcher dataMatch = new DataMatcher();
//		List<JSONObject> datalist = getAllInstances();
//		dataMatch.datalist = datalist;
//		
////		for(JSONObject data : datalist){
////			System.out.println(data.toString(4));
////		}
//		
//		for(int i = 0; i < schemaList.size() - 1; i++){
//			for(int j = i + 1; j < schemaList.size(); j++){
////
////				System.out.println(res.toString(4));
////				System.out.println(">> " + schemaList.get(i).getTable() 
////						+ " " + schemaList.get(i).getDatastore().getDatabase() 
////						+ " " + schemaList.get(j).getTable() 
////						+ " " + schemaList.get(j).getDatastore().getDatabase());
//				List<SimilarityScore> score = dataMatch.match(schemaList.get(i), schemaList.get(j));
////				System.out.println();
//				instanceScore.addAll(score);
//			}			
//		}
//		
//		return instanceScore;
//	}
	
//	private List<JSONObject> getAllInstances() throws SQLException{
//		cassandradriver = CassandraDriver.getInstance();
//		mysqldriver = MySQLDriver.getInstance();
//		
//		List<JSONObject> jsonList = new ArrayList<>();
//		for(int i = 0; i < schemaList.size(); i++){
//			String ds = schemaList.get(i).getDatastore().getDatabase();
//			String table = schemaList.get(i).getDatastore().getDbName().concat(".").concat(schemaList.get(i).getTable());
//			JSONObject res = getInstance(ds, table);
//			jsonList.add(res);
//		}
//		
//		mysqldriver.conn.close();
//		cassandradriver.close();
//		return jsonList;
//	}
	
//	private JSONObject getInstance(String datastore, String tableName){
//		JSONObject res = null;
//		System.out.println(tableName + " " + datastore);
//		switch(datastore){
//			case(Datastore.MySQL):				
//				res = mysqldriver.getDataInstance(tableName);
//				break;
//			case(Datastore.CASSANDRA):
//				res = cassandradriver.getDataInstance(tableName.split("\\.")[1]);
//				break;
//		}
//		
//		return res;
//	}
	
	private static  SimilarityScore getScore(SimilarityScore sc1, List<SimilarityScore> scList){
		SimilarityScore score = null;
		for(SimilarityScore sc2 : scList){
			if(sc1.getSchema1().equals(sc2.getSchema1()) 
					&& sc1.getSchema2().equals(sc2.getSchema2())
					&& sc1.getCol1().equals(sc2.getCol1())
					&& sc1.getCol2().equals(sc2.getCol2())){
//				System.out.println(sc1.toString() + " === " + sc2.toString());
				score = sc2;
			}
		}
		return score;
	}
	
	
	
//	public static void main(String[] args) throws SQLException{
//		double[] weight = {0.4,0.6};
//		SchemaMatcher match = new SchemaMatcher();
//		match.init();
//		
//		List<SimilarityScore> schemaScore = match.schemaLevelMatch();		
//		List<SimilarityScore> instanceScore = match.instanceLevelMatch();
//		
//		List<SimilarityScore> combinedScore = new ArrayList<>();
//		for(SimilarityScore sc1 : schemaScore){
//			SimilarityScore score = getScore(sc1, instanceScore);
//			double total = sc1.getSimScore();
//			if(score != null){
//				total = (total * weight[0]) + (score.getSimScore()*weight[1]);
//				
//			}
//			else{
//				total = (total * weight[0]) + 0.0;
//			}
//
//			if(total > 0.4){
//				sc1.setSimScore(total);
//				combinedScore.add(sc1);				
//			}
//
//		}
//		
//		//sort score in reversed order
//		Collections.sort(combinedScore);
		
//		SchemaMapper.createMapping(combinedScore);
//	}
}

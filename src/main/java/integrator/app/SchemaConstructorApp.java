package integrator.app;

import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import java.awt.BorderLayout;
import javax.swing.JMenuBar;
import javax.swing.JMenu;
import javax.swing.JMenuItem;
import javax.swing.SwingConstants;
import javax.swing.JTree;
import javax.swing.JSplitPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;
import javax.swing.border.BevelBorder;
import java.awt.Panel;
import javax.swing.BoxLayout;
import javax.swing.border.SoftBevelBorder;
import java.awt.Color;
import java.awt.FlowLayout;
import java.awt.GridLayout;
import javax.swing.SpringLayout;
import javax.swing.GroupLayout;
import javax.swing.GroupLayout.Alignment;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.DefaultMutableTreeNode;

public class SchemaConstructorApp {

	private JFrame frmSchemaConstructor;
	private JTable table;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					SchemaConstructorApp window = new SchemaConstructorApp();
					window.frmSchemaConstructor.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the application.
	 */
	public SchemaConstructorApp() {
		initialize();
	}

	/**
	 * Initialize the contents of the frame.
	 */
	private void initialize() {
		frmSchemaConstructor = new JFrame();
		frmSchemaConstructor.getContentPane().setBackground(Color.BLACK);
		frmSchemaConstructor.setTitle("Schema Constructor");
		frmSchemaConstructor.setBounds(100, 100, 1024, 768);
		frmSchemaConstructor.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		
		JMenuBar menuBar = new JMenuBar();
		frmSchemaConstructor.setJMenuBar(menuBar);
		
		JMenu mnDatabase = new JMenu("Database");
		menuBar.add(mnDatabase);
		
		JMenuItem mntmAddDatabase = new JMenuItem("Add Database");
		mnDatabase.add(mntmAddDatabase);
		
		JMenuItem mntmRemoveDatabase = new JMenuItem("Remove Database");
		mnDatabase.add(mntmRemoveDatabase);
		
		JPanel panel = new JPanel();
		panel.setBackground(Color.LIGHT_GRAY);
		SpringLayout sl_panel = new SpringLayout();
		panel.setLayout(sl_panel);
		GroupLayout groupLayout = new GroupLayout(frmSchemaConstructor.getContentPane());
		groupLayout.setHorizontalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 1008, Short.MAX_VALUE)
		);
		groupLayout.setVerticalGroup(
			groupLayout.createParallelGroup(Alignment.LEADING)
				.addComponent(panel, GroupLayout.DEFAULT_SIZE, 708, Short.MAX_VALUE)
		);
		
		JTree tree = new JTree();
		tree.setModel(new DefaultTreeModel(
			new DefaultMutableTreeNode("Database") {
				{
				}
			}
		));
		sl_panel.putConstraint(SpringLayout.NORTH, tree, 0, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, tree, 0, SpringLayout.WEST, panel);
		sl_panel.putConstraint(SpringLayout.SOUTH, tree, 0, SpringLayout.SOUTH, panel);
		sl_panel.putConstraint(SpringLayout.EAST, tree, 198, SpringLayout.WEST, panel);
		panel.add(tree);
		
		table = new JTable();
		sl_panel.putConstraint(SpringLayout.NORTH, table, 1, SpringLayout.NORTH, panel);
		sl_panel.putConstraint(SpringLayout.WEST, table, 0, SpringLayout.EAST, tree);
		sl_panel.putConstraint(SpringLayout.SOUTH, table, 0, SpringLayout.SOUTH, tree);
		sl_panel.putConstraint(SpringLayout.EAST, table, 810, SpringLayout.EAST, tree);
		table.setModel(new DefaultTableModel(
			new Object[][] {
				{null, null},
				{null, null},
			},
			new String[] {
				"New column", "New column"
			}
		));
		panel.add(table);
		frmSchemaConstructor.getContentPane().setLayout(groupLayout);
	}
}

package integrator;

import integrator.config.IntegratorUtil;
import integrator.config.SchemaLevel;
import integrator.config.Similarity;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import org.apache.commons.text.similarity.JaccardSimilarity;

import edu.cmu.lti.lexical_db.ILexicalDatabase;
import edu.cmu.lti.lexical_db.NictWordNet;
import edu.cmu.lti.ws4j.RelatednessCalculator;
import edu.cmu.lti.ws4j.impl.Lin;
import edu.cmu.lti.ws4j.impl.Path;
import edu.cmu.lti.ws4j.impl.WuPalmer;
import edu.cmu.lti.ws4j.util.WS4JConfiguration;

public class SchemaMatcher{
	private static final String TEXT = "TEXT";
	private static final String NUMBER = "NUMBER";
	private static final String DATETIME = "DATETIME";
	private static final String UNDEFINED = "UNDEFINED";
	
	//set true to enable column name split by uppercase and underscore (more detailed comparison)
	//set false to compare whole column name
	private static final boolean enableWordSplit = true;
	
	
	List<SimilarityScore> schemaScore = new ArrayList<>();
	
	public List<SimilarityScore> match(SourceSchema S1, SourceSchema S2) {			
		SchemaLevel schemaConf = IntegratorUtil.confData.getSchemaLevel();
		Map<String, String> map1 = S1.getColumns();
		Map<String, String> map2 = S2.getColumns();
		
		List<SimilarityScore> simScore = new ArrayList<>();		
		
		for (Map.Entry<String, String> entry1 : map1.entrySet()) {
			for (Map.Entry<String, String> entry2 : map2.entrySet()) {
				double score = 0.0;
				for(Similarity simConf : schemaConf.getSimilarity()){
					
					if(simConf.getName().equals("string") && simConf.getEnabled()){
						double schemaScore = nameMatch(entry1.getKey(), entry2.getKey());
						score += simConf.getWeight() * schemaScore;
//						System.out.println(entry1.getKey() + " " + entry2.getKey() + " " + schemaScore + " " + score);
					}
					else if(simConf.getName().equals("datatype") && simConf.getEnabled()){
						double typeScore = typeMatch(entry1.getValue(), entry2.getValue());
						score += simConf.getWeight() * typeScore;
//						System.out.println(entry1.getKey() + " " + entry2.getKey() + " " + typeScore + " " + score);
					}
					else if(simConf.getName().equals("semantic") && simConf.getEnabled()){
						double semnticScore = semanticMatch(entry1.getKey(), entry2.getKey());
						score += simConf.getWeight() * semnticScore;
//						System.out.println(entry1.getKey() + " " + entry2.getKey() + " " + semnticScore + " " + score);
					}
				}
				
				SimilarityScore sc = new SimilarityScore();
				sc.setSchema1(S1);
				sc.setSchema2(S2);
				sc.setCol1(entry1.getKey());
				sc.setCol2(entry2.getKey());
				sc.setSimScore(score);
				simScore.add(sc);
//				System.out.println(sc.toString());
			}

		}
		return simScore;
	}
	
	public double nameMatch(String col1, String col2) {
		JaccardSimilarity sim = new JaccardSimilarity();
		double score = 0.0;
		
		if(enableWordSplit){
			String[] str1 = splitString(col1);
			String[] str2 = splitString(col2);
			for(int i = 0; i < str1.length; i++){
				for(int j = 0; j < str2.length; j++){
					score += sim.apply(str1[i].toLowerCase(), str2[j].toLowerCase());
				}
			}
			score = score/(str1.length + str2.length);
		}
		else
			score = sim.apply(col1.toLowerCase(), col2.toLowerCase());
				
		return score;
	}


	public double typeMatch(String type1, String type2) {
		double score;
		if(getTypeCategory(type1).equals(getTypeCategory(type2))){
			score = 1;
		}
		else{
			score = 0;
		}
		return score;
	}
	
	public double semanticMatch(String col1, String col2){
		double score = 0.0;
		
		if(enableWordSplit){
			String[] str1 = splitString(col1);
			String[] str2 = splitString(col2);
			for(int i = 0; i < str1.length; i++){
				for(int j = 0; j < str2.length; j++){
					score += getSemanticScore(str1[i].toLowerCase(), str2[j].toLowerCase());
				}
			}
			
			score = score/(str1.length + str2.length);
		}
		else
			score = getSemanticScore(col1, col2);
		return score;
	}
	
	private ILexicalDatabase db = new NictWordNet();
	private RelatednessCalculator[] rcs = {
		new Path(db), new WuPalmer(db), new Lin(db)
	};
	
	private double getSemanticScore(String word1, String word2) {
		double score = 0.0;
		WS4JConfiguration.getInstance().setMFS(true);
//		System.out.print(word1 + " <> " + word2 + " = ");
		for ( RelatednessCalculator rc : rcs ) {
			double s = rc.calcRelatednessOfWords(word1, word2);
//			System.out.println( rc.getClass().getName()+"\t"+s );
			//sometime the result is infinity, set to 1 instead (this is strange, check the documentation again)
			if(s > 1){
				s = 1;
			}
			score += s;
		}		
		score = score/3;
		return score;
	}
	
	private String[] splitString(String s){
		String[] str;
		//split by underscore
		if(s.contains("_")){			
			str = s.split("_");
		}
		//split by uppercase letter
		else{			
			str = s.split("(?=\\p{Upper})");
		}
//		System.out.println(Arrays.asList(str).toString());		
		return str;
	}
	
	private String getTypeCategory(String col){
		String category = UNDEFINED;
		switch(col){
			//cassandra
			case "ascii":				
			case "inet":
			case "text":				
			case "varchar":
			//mysql
			case "CHAR":
			case "VARCHAR":
			case "TINYTEXT":			
			case "MEDIUMTEXT":
			case "LONGTEXT":
			case "BLOB":
			case "MEDIUMBLOB":
			case "LONGBLOB":
			//mysql, hdfs, mongo
			case "TEXT":
				category = TEXT;
				break;
			//cassandra
			case "date":
			case "time":					
			case "timestamp":
			//mysql
			case "DATE":
			case "DATETIME":
			case "TIMESTAMP":
			case "TIME":
			case "YEAR":
				category = DATETIME;
				break;
			//cassandra
			case "bigint":
			case "counter":
			case "smallint":
			case "varint":
			case "tinyint":
			case "int":				
			case "boolean":						
			case "decimal":
			case "double":
			case "float":
			//mysql
			case "TINYINT":
			case "SMALLINT":
			case "MEDIUMINT":
			case "INT":
			case "BIGINT":
			case "FLOAT":
			case "DOUBLE":
			case "DECIMAL":
			//hdfs & mongo
			case "NUMBER":
				category = NUMBER;
				break;				
			case "list":
			case "map":
			case "set":
			case "tuple":
			case "frozen":
			case "blob":
			case "timeuuid":
			case "uuid":
				category = UNDEFINED;
				break;
		}
		return category;
	}
}

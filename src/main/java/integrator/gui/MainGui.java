package integrator.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.ImageIcon;
import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;

import processor.gui.ProcessorGui;

import javax.swing.JLabel;
import javax.swing.JButton;
import javax.swing.BoxLayout;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class MainGui extends JFrame {

	private JPanel contentPane;

	/**
	 * Launch the application.
	 */
	public static void main(String[] args) {
		EventQueue.invokeLater(new Runnable() {
			public void run() {
				try {
					MainGui frame = new MainGui();
					frame.setVisible(true);
				} catch (Exception e) {
					e.printStackTrace();
				}
			}
		});
	}

	/**
	 * Create the frame.
	 */
	public MainGui() {
		setTitle("MusQ - Start");
		setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
		setBounds(400, 200, 429, 144);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		contentPane.setLayout(new BorderLayout(0, 0));
		setContentPane(contentPane);
		setIconImage(new ImageIcon("img/Logo2.jpg").getImage());
		
		JLabel label = new JLabel(" ");
		label.setIcon(new ImageIcon("img/Logo2.jpg"));
		contentPane.add(label, BorderLayout.WEST);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.EAST);
		panel.setLayout(new BoxLayout(panel, BoxLayout.Y_AXIS));
		
		JButton btnDsConfig = new JButton("Datastore Configuration");
		btnDsConfig.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new IntegratorDsConfigGui().setVisible(true);;
			}
		});
		btnDsConfig.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnDsConfig.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)btnDsConfig.getMinimumSize().getHeight()));
		panel.add(btnDsConfig);
		
		JButton btnSchemaBuilder = new JButton("GlobalSchema Construction");
		btnSchemaBuilder.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				new IntegratorDsMatcherGui().setVisible(true);
			}
		});
		btnSchemaBuilder.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnSchemaBuilder.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)btnSchemaBuilder.getMinimumSize().getHeight()));
		panel.add(btnSchemaBuilder);
		
		JButton btnQueryProcessor = new JButton("Query Processing");
		btnQueryProcessor.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				new ProcessorGui().setVisible(true);;
			}
		});
		btnQueryProcessor.setAlignmentX(Component.CENTER_ALIGNMENT);
		btnQueryProcessor.setMaximumSize(new Dimension(Integer.MAX_VALUE, (int)btnQueryProcessor.getMinimumSize().getHeight()));
		panel.add(btnQueryProcessor);
	}

}

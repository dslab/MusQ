package integrator.gui;

import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JOptionPane;

import data.schema.Datastore;
import integrator.Mapper;
import integrator.Matcher;
import integrator.SimilarityScore;
import integrator.SourceSchema;
import integrator.config.InstanceLevel;
import integrator.config.IntegratorUtil;
import utils.DatastoreUtil;

public class SchemaBuilderUtil {
/*
 * 		String dsId = parentDetails[DS_ID_INDEX].trim();
		String dsName = parentDetails[DS_NAME_INDEX];
		String tableName = (String)selectedNode.getUserObject();
 */
	public final static int SCHEMAID_DSID_POS = 0;
	public final static int SCHEMAID_DSNAME_POS = 1;
	public final static int SCHEMAID_TABLENAME_POS = 2;

	private static List<SourceSchema> schemas;
	private static List<SourceSchema> selectedSchemas;
	private static List<String> globalSchemaIdList;
	private static List<SimilarityScore> simScoreList; 
	private static Matcher matcher;
	private static boolean getInstance;
	
	public static void init() {
		InstanceLevel conf = IntegratorUtil.confData.getInstanceLevel();
		getInstance = conf.getEnabled();
		matcher = Matcher.getInstance();
	}
	public static void extractSchema() {
		List<String> datastoreIdList = DatastoreUtil.getAllDatastoreID();
		for(String s:datastoreIdList) {

			Datastore ds = DatastoreUtil.getDatastore(s);
			if(ds.getDatabase()=="Cassandra") matcher.getCassandraSchema(ds, getInstance);
			else if(ds.getDatabase()=="MySQL") matcher.getMySQLSchema(ds, getInstance);
			else if(ds.getDatabase()=="MongoDB") matcher.getMongoSchema(ds, getInstance);
			else if(ds.getDatabase()=="HDFS") matcher.getHDFSSchema(ds, getInstance);				
		}
		
		schemas =  matcher.combineSchema();
	}
	public static List<String> getTablesFromDatastore(String dsId) {
		List<String> tableList = new ArrayList<String>();
		for(SourceSchema sch:schemas)
			if(sch.getDatastore().getID()==dsId)
				tableList.add(sch.getTable());
		
		return tableList;
	}
	
	public static Map<String,String> getSchemaDetails(String datastoreId, String tableName){
		Map<String,String> schemaDetails = new LinkedHashMap<String,String>();
		schemaDetails.put("Datastore ID", datastoreId);
		
		for(SourceSchema sch:schemas) {

			if(sch.getDatastore().getID().equals(datastoreId))
			{
				if(sch.getTable().equals(tableName)) {
					schemaDetails.put("Datastore Type", sch.getDatastore().getDatabase());
					schemaDetails.put("Datastore Name", sch.getDatastore().getDbName());
					schemaDetails.put("Address", sch.getDatastore().getAddress());
					schemaDetails.put("Name",tableName);
					schemaDetails.put("Columns:", " ");
					schemaDetails.putAll(sch.getColumns());
					break;
				}
			}
				
		}
		return schemaDetails;
	}

	private static String[] breakSchemaId(String schemaId) {
		return schemaId.split("\\p{Punct}");
	}
	
	private static void doSelectSchema(List<String> schemaIdList) {
		selectedSchemas = new ArrayList<SourceSchema>();
		for(String schemaId:schemaIdList) {

			String[] schemaInfos = breakSchemaId(schemaId);
			String datastoreId = schemaInfos[SCHEMAID_DSID_POS];
			String tableName = schemaInfos[SCHEMAID_TABLENAME_POS];
			for(SourceSchema sch:schemas) {

				if(sch.getDatastore().getID().equals(datastoreId)&&sch.getTable().equals(tableName)) {
					selectedSchemas.add(sch);
					break;
				}
					
			}
		}
		

	}
	private static void calculateGlobalSchemas(){
		simScoreList = matcher.calculate(selectedSchemas);
		globalSchemaIdList = new ArrayList<String>();
		for(int i=0;i<simScoreList.size();i++){
			globalSchemaIdList.add("M"+(i+1));
		}
	}
	
	public static int buildGlobalSchemas(List<String> schemaIdList) {
		doSelectSchema(schemaIdList);
		calculateGlobalSchemas();
		return globalSchemaIdList.size();
	}
	
	public static List<String> getAllCandidateId(){
		if(globalSchemaIdList.size()==0)
			return null;
		return globalSchemaIdList;
	}
	
	public static List<String> getCandidateDetails(String candidateId){
		int index = globalSchemaIdList.indexOf(candidateId);
		List<String> details = new ArrayList<String>();
		SimilarityScore simScore = simScoreList.get(index);
		
		details.add("Name: "+candidateId);
		details.add("Similarity score: " + simScore.getSimScore());
		details.add("Tables: " + simScore.getSchema1().getDatastore().getDbName() + ":"  + simScore.getSchema1().getTable() +
				"," + simScore.getSchema2().getDatastore().getDbName() + ":"  + simScore.getSchema2().getTable() );
		
		details.add("Relationship: "+ simScore.getSchema1().getTable().concat(".").concat(simScore.getCol1()) + "="+
				simScore.getSchema2().getTable().concat(".").concat(simScore.getCol2()));
		
		details.add("Table "+simScore.getSchema1().getTable()+ " columns:");
		for(String col: simScore.getSchema1().getColumns().keySet()){
			if(col.equals(simScore.getCol1()))
				details.add("+ "+col+" [related columns, cannot be removed]");
			else
				details.add("+ "+col);
		}
		details.add("Table "+simScore.getSchema2().getTable()+ " columns:");
		for(String col: simScore.getSchema2().getColumns().keySet()){
			if(col.equals(simScore.getCol2()))
				details.add("- "+col+" [related columns, cannot be removed]");
			else
				details.add("- "+col);
		}
		
		return details;
	}
	
	public static void renameGlobalSchema(int globalSchemaIndex, String newGlobalSchemaId){
		globalSchemaIdList.set(globalSchemaIndex, newGlobalSchemaId);
	}
	
	public static void deleteGlobalSchema(int globalSchemaIndex){
		globalSchemaIdList.remove(globalSchemaIndex);
		simScoreList.remove(globalSchemaIndex);
		
	}
	
	public static void removeColumnFromSchema(String globalSchemaId, String rowContent) {
		int globalSchemaIndex = globalSchemaIdList.indexOf(globalSchemaId);
		SimilarityScore simScore = simScoreList.get(globalSchemaIndex);
		SourceSchema schema = simScore.getSchema2();
		
		if(rowContent.startsWith("+")) schema = simScore.getSchema1();	
		
		Map <String,String> columns = schema.getColumns();
		
		String columnName = rowContent.split(" ")[1];
		
		columns.remove(columnName);
		schema.setColumns(columns);
		
		if(rowContent.startsWith("+")) simScore.setSchema1(schema);	
		else simScore.setSchema2(schema);			
	}
	
	public static boolean saveAllGlobalSchema(String outFile) {
	    BufferedWriter writer;
		try {
			writer = new BufferedWriter(new FileWriter(outFile));
			
			for(String s:globalSchemaIdList) {
				String fileContent = "";
				fileContent = fileContent + getTextBasedSchema(s);
				writer.write(fileContent);
				writer.write("\n");
				writer.write("\n");
				}
				writer.close();
			} catch (IOException e) {
				e.printStackTrace();
				return false;
		}
		return true;
	}
	
	public static String getTextBasedSchema(String globalSchemaId) {
		int index = globalSchemaIdList.indexOf(globalSchemaId);
		return Mapper.createMapping(globalSchemaId, simScoreList.get(index));
	}

	public static boolean checkTextContentSame(String schemaText, String selectedGlobalSchemaId) {
		if(schemaText.equals(getTextBasedSchema(selectedGlobalSchemaId)))
			return true;
		return false;
	}
	
	public static boolean schemaValidator(String schemaText) {
		/*
		 * 		mapping += globalSchemaId + "(" + Mapper.getCombinedColumn(sc) + ") :-\n";
		mapping += table1 + "(" + getColumns(sc.getSchema1()) + ")@" + db1 + ",\n";
		mapping += table2 + "(" + getColumns(sc.getSchema2()) + ")@" + db2 + ",\n";
		mapping += sc.getSchema1().getTable().concat(".").concat(sc.getCol1()).concat(" = ") 
		.concat(sc.getSchema2().getTable()).concat(".").concat(sc.getCol2()).concat(";");

		 */
		
		String[] lines = schemaText.split("\n");
		for(int i=0;i<lines.length;i++)
			JOptionPane.showMessageDialog(null, lines[i]);
		return false;
		
	}
	
}

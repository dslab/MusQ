package integrator.gui;

import java.awt.Component;
import java.awt.Dimension;
import java.awt.GridBagConstraints;
import java.awt.GridBagLayout;
import java.awt.Insets;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.ComponentAdapter;
import java.awt.event.ComponentEvent;
import java.awt.event.ItemEvent;
import java.awt.event.ItemListener;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.DefaultCellEditor;
import javax.swing.DefaultComboBoxModel;
import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JCheckBox;
import javax.swing.JComboBox;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.JPanel;
import javax.swing.JScrollPane;
import javax.swing.JSeparator;
import javax.swing.JSplitPane;
import javax.swing.JTabbedPane;
import javax.swing.JTable;
import javax.swing.JTextArea;
import javax.swing.JTextField;
import javax.swing.JTree;
import javax.swing.JViewport;
import javax.swing.SwingConstants;
import javax.swing.UIManager;
import javax.swing.border.EmptyBorder;
import javax.swing.event.ChangeEvent;
import javax.swing.event.ChangeListener;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.event.TableModelEvent;
import javax.swing.event.TableModelListener;
import javax.swing.event.TreeSelectionEvent;
import javax.swing.event.TreeSelectionListener;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableColumn;
import javax.swing.table.TableColumnModel;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;

import integrator.config.IntegratorUtil;
import utils.DatastoreUtil;

@SuppressWarnings({ "serial" })
public class IntegratorDsMatcherGui extends JFrame {
	
	private static final int BUTTON_COLINDEX = 1;
	private static final int CONTENT_COLINDEX = 0;
	
	
	private static final int RESPONSE_YES = 0;
	
	private static final int TAB_TEXT_EDITOR = 1;
	
	private static final int DS_ID_INDEX = 0;
	private static final int DS_NAME_INDEX = 3;
	private static final int DS_TYPE_INDEX = 1;
	private static final int DS_ADDR1_INDEX = 4;
	private static final int DS_ADDR2_INDEX = 5;
	private static final int DS_ADDR3_INDEX = 6;
	private static final int DS_ADDR4_INDEX = 7;
	
	private static final int MIN_SCHEMA_COMBINED = 2;
	
	
	private JPanel contentPane;
	private JTable tableDatastoreDetails;
	private JScrollPane scrollPaneDetails;
	private JPanel panelSelectedSchemaBuilding;
	private JTree tree;
	private JSplitPane splitPaneChild;
	private JScrollPane scrollPaneTree;
	private JButton btnAddToSchema;
	private JPanel panel_1;
	private JLabel lblNewLabel;
	private JLabel lblInfoSelectedSchema;
	private JScrollPane scrollPane;
	private JPanel panelGlobalSchemaCandidates;
	private JLabel lblMatchingResult;
	private JLabel lblGlobalSchemasCandidate;
	private JScrollPane scrollPaneTableGlobalSchema;
	private JTable tableGlobalSchema;
	private JButton btnNewButton;
	private JTable tableSelectedSchema;
	private JButton btnBuildGlobalSchema;
	private JSeparator separator;
	
	private JLabel lblInfoBuildGlobalSchema;
	private JButton btnRemoveCandidate;
	private JPanel panel;
	private JLabel lblInfoCandidateName;
	private JTextField tfCandidateName;
	private JButton btnRenameCandidate;
	private JComboBox<String> comboCandidates;
	private JPanel panel_3;
	private JTabbedPane tabbedPane;
	private JPanel panel_2;
	private JPanel panel_4;
	
	private JScrollPane scrollPaneTextAreaGlobalSchema;
	private JTextArea textAreaSchemaEditor;

	/**
	 * Create the frame.
	 */
	public IntegratorDsMatcherGui() {
		/*
		 * JFrame Initialization
		 */

		DatastoreUtil.init();
		IntegratorUtil.readConfig();
		setTitle("MusQ - Global Schema Building");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(400, 200, 640, 540);
		addComponentListener(new ComponentAdapter() {
			@Override
			public void componentResized(ComponentEvent e) {
				resizeTableTwoColumnsWithButton(tableGlobalSchema);
				resizeTableTwoColumnsWithButton(tableSelectedSchema);
			}
		});
		
		

		setIconImage(new ImageIcon("img/Logo2.jpg").getImage());
		
		/*
		 * ContentPane Initialization
		 */
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		GridBagLayout gbl_contentPane = new GridBagLayout();
		gbl_contentPane.columnWidths = new int[]{0, 0};
		gbl_contentPane.rowHeights = new int[]{0, 0};
		gbl_contentPane.columnWeights = new double[]{1.0, Double.MIN_VALUE};
		gbl_contentPane.rowWeights = new double[]{1.0, Double.MIN_VALUE};
		contentPane.setLayout(gbl_contentPane);
		
		/*
		 * Main splitPane Initialization
		 */
		JSplitPane splitPane = new JSplitPane();
		splitPane.setEnabled(false);
		splitPane.setResizeWeight(0.3);
		splitPane.setDividerLocation(0.3);
		
		GridBagConstraints gbc_splitPane = new GridBagConstraints();
		gbc_splitPane.weightx = 1.0;
		gbc_splitPane.fill = GridBagConstraints.BOTH;
		gbc_splitPane.gridx = 0;
		gbc_splitPane.gridy = 0;
		contentPane.add(splitPane, gbc_splitPane);

		/*
		 * Left splitPane Initialization
		 * (DataStore/Schema Tree-Add to Schema Button-DataStore/Schema Properties table)
		 */
		JSplitPane splitPaneParent = new JSplitPane();
		
		splitPaneParent.setResizeWeight(0.5);
		splitPaneParent.setDividerLocation(0.5);

		splitPaneParent.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPane.setLeftComponent(splitPaneParent);
		
		
		/*
		 * DataStore/Schema Tree and DataStore/Schema Properties table initialization
		 */
		

		SchemaBuilderUtil.init();
		SchemaBuilderUtil.extractSchema();
		initTree();
		initTableDatastoreDetails();
		
		scrollPaneDetails = new JScrollPane(tableDatastoreDetails);
		splitPaneParent.setRightComponent(scrollPaneDetails);
		
		splitPaneChild = new JSplitPane();
		splitPaneChild.setResizeWeight(0.99);
		splitPaneChild.setOrientation(JSplitPane.VERTICAL_SPLIT);
		splitPaneChild.setEnabled(false);
		splitPaneParent.setLeftComponent(splitPaneChild);
		splitPaneParent.setEnabled(false);
		
		scrollPaneTree = new JScrollPane(tree);
		splitPaneChild.setLeftComponent(scrollPaneTree);
		
		lblNewLabel = new JLabel("Pick some of the preferred source schemas to build global schema");
		scrollPaneTree.setColumnHeaderView(lblNewLabel);
		
		panel_1 = new JPanel();
		splitPaneChild.setRightComponent(panel_1);
		
		btnAddToSchema = new JButton("Add Schema to Builder");
		btnAddToSchema.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				JButton btn = (JButton) e.getSource();
				if(btn.isEnabled()) {
					addSchemaToSelected();
					btn.setEnabled(false);
				}
			}
		});
		btnAddToSchema.setEnabled(false);
		panel_1.add(btnAddToSchema);
		
		panelSelectedSchemaBuilding = new JPanel();
		splitPane.setRightComponent(panelSelectedSchemaBuilding);
		panelSelectedSchemaBuilding.setLayout(new BoxLayout(panelSelectedSchemaBuilding, BoxLayout.Y_AXIS));
		
		lblInfoSelectedSchema = new JLabel("Selected Source Schemas: 0");
		lblInfoSelectedSchema.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelSelectedSchemaBuilding.add(lblInfoSelectedSchema);
		
		lblInfoBuildGlobalSchema = new JLabel("[Minimum selected schemas to create global schema: 2]");
		lblInfoBuildGlobalSchema.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelSelectedSchemaBuilding.add(lblInfoBuildGlobalSchema);
		
		scrollPane = new JScrollPane();
		panelSelectedSchemaBuilding.add(scrollPane);
		
		initTableSelectedSchema();
		scrollPane.setViewportView(tableSelectedSchema);
		tableSelectedSchema.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tableSelectedSchema.getTableHeader().setReorderingAllowed(false);
		tableSelectedSchema.getTableHeader().setResizingAllowed(false);
		renderButtonTable(tableSelectedSchema);
		
		separator = new JSeparator();
		
		btnBuildGlobalSchema = new JButton("Build Global Schema");
		btnBuildGlobalSchema.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int numCandidates = SchemaBuilderUtil.buildGlobalSchemas(retrieveSelectedSchemas());
				if(numCandidates==0)
					JOptionPane.showMessageDialog(null, "Global schema cannot be built from these schemas because their similarity score is too low");
				else
				{
					JOptionPane.showMessageDialog(null, "Global schema building finished. Total schema candidates created: "+numCandidates);
					
					fillListCandidate();
				}
						
				String infoNumGeneratedGlobalSchema = lblMatchingResult.getText().split("[0-9]+")[0];
				infoNumGeneratedGlobalSchema = infoNumGeneratedGlobalSchema + numCandidates;
				lblMatchingResult.setText(infoNumGeneratedGlobalSchema);
			}
		});
		btnBuildGlobalSchema.setEnabled(false);
		btnBuildGlobalSchema.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelSelectedSchemaBuilding.add(btnBuildGlobalSchema);
		panelSelectedSchemaBuilding.add(Box.createRigidArea(new Dimension(0, 5)));
		panelSelectedSchemaBuilding.add(separator);
		panelSelectedSchemaBuilding.add(Box.createRigidArea(new Dimension(0, 5)));
		
		panelGlobalSchemaCandidates = new JPanel();
		panelSelectedSchemaBuilding.add(panelGlobalSchemaCandidates);
		GridBagLayout gbl_panelGlobalSchemaCandidates = new GridBagLayout();
		gbl_panelGlobalSchemaCandidates.columnWidths = new int[] {0};
		gbl_panelGlobalSchemaCandidates.rowHeights = new int[] {0, 0, 0, 0};
		gbl_panelGlobalSchemaCandidates.columnWeights = new double[]{1.0};
		gbl_panelGlobalSchemaCandidates.rowWeights = new double[]{0.0, 0.0, 1.0, 1.0};
		panelGlobalSchemaCandidates.setEnabled(false);
		panelGlobalSchemaCandidates.setLayout(gbl_panelGlobalSchemaCandidates);
		
		lblMatchingResult = new JLabel("Matching Result: 0");
		lblMatchingResult.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblMatchingResult = new GridBagConstraints();
		gbc_lblMatchingResult.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblMatchingResult.anchor = GridBagConstraints.WEST;
		gbc_lblMatchingResult.insets = new Insets(0, 0, 5, 0);
		gbc_lblMatchingResult.gridx = 0;
		gbc_lblMatchingResult.gridy = 0;
		panelGlobalSchemaCandidates.add(lblMatchingResult, gbc_lblMatchingResult);
		
		lblGlobalSchemasCandidate = new JLabel("Global Schemas Candidate");
		lblGlobalSchemasCandidate.setHorizontalAlignment(SwingConstants.LEFT);
		GridBagConstraints gbc_lblGlobalSchemasCandidate = new GridBagConstraints();
		gbc_lblGlobalSchemasCandidate.fill = GridBagConstraints.HORIZONTAL;
		gbc_lblGlobalSchemasCandidate.anchor = GridBagConstraints.WEST;
		gbc_lblGlobalSchemasCandidate.insets = new Insets(0, 0, 5, 0);
		gbc_lblGlobalSchemasCandidate.gridx = 0;
		gbc_lblGlobalSchemasCandidate.gridy = 1;
		panelGlobalSchemaCandidates.add(lblGlobalSchemasCandidate, gbc_lblGlobalSchemasCandidate);
		
		panel_3 = new JPanel();
		GridBagConstraints gbc_panel_3 = new GridBagConstraints();
		gbc_panel_3.insets = new Insets(0, 0, 5, 0);
		gbc_panel_3.fill = GridBagConstraints.BOTH;
		gbc_panel_3.gridx = 0;
		gbc_panel_3.gridy = 2;
		panelGlobalSchemaCandidates.add(panel_3, gbc_panel_3);
		panel_3.setLayout(new BoxLayout(panel_3, BoxLayout.X_AXIS));

		panel_3.add(Box.createRigidArea(new Dimension(3, 0)));
		comboCandidates = new JComboBox<String>();
		panel_3.add(comboCandidates);
		
		panel_3.add(Box.createRigidArea(new Dimension(5, 0)));
		
		btnRemoveCandidate = new JButton("Remove Candidate");
		btnRemoveCandidate.setEnabled(false);
		btnRemoveCandidate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				int response = showMessageYesNo("Are you sure to delete schema candidate: "+comboCandidates.getSelectedItem().toString(), 
						"Removing Single Schema Candidate");
				if(response==RESPONSE_YES){
					SchemaBuilderUtil.deleteGlobalSchema(comboCandidates.getSelectedIndex());
					fillListCandidate();
					String infoNumGeneratedGlobalSchema = lblMatchingResult.getText().split("[0-9]+")[0];
					infoNumGeneratedGlobalSchema = infoNumGeneratedGlobalSchema + comboCandidates.getItemCount();
					lblMatchingResult.setText(infoNumGeneratedGlobalSchema);
					
				}
			}
		});
		panel_3.add(btnRemoveCandidate);
		
		panel_3.add(Box.createRigidArea(new Dimension(3, 0)));
		
		comboCandidates.addItemListener(new ItemListener() {
			
			@Override
			public void itemStateChanged(ItemEvent e) {
				// TODO Auto-generated method stub
				if (e.getStateChange() == ItemEvent.SELECTED) {			          
			          viewTableRefreshGlobalSchema();
			          renderButtonTable(tableGlobalSchema);
			          viewTextFieldRefreshRename();

			       }
			}
		});
		
		tabbedPane = new JTabbedPane(JTabbedPane.TOP);
		GridBagConstraints gbc_tabbedPane = new GridBagConstraints();
		gbc_tabbedPane.weighty = 100.0;
		gbc_tabbedPane.insets = new Insets(0, 0, 5, 0);
		gbc_tabbedPane.fill = GridBagConstraints.BOTH;
		gbc_tabbedPane.gridx = 0;
		gbc_tabbedPane.gridy = 3;
		panelGlobalSchemaCandidates.add(tabbedPane, gbc_tabbedPane);
		
		tabbedPane.addChangeListener(new ChangeListener() {
			
			@Override
			public void stateChanged(ChangeEvent e) {
				if(tabbedPane.getSelectedIndex()==TAB_TEXT_EDITOR)
					loadTextBasedEditor();
				else
					loadTableBasedEditor();
			}
		});
		
		panel_2 = new JPanel();
		tabbedPane.addTab("Table Editor View", null, panel_2, null);
		panel_2.setLayout(new BoxLayout(panel_2, BoxLayout.Y_AXIS));
		
		panel = new JPanel();
		panel_2.add(panel);
		panel.setLayout(new BoxLayout(panel, BoxLayout.X_AXIS));

		panel.add(Box.createRigidArea(new Dimension(3,0)));
		
		lblInfoCandidateName = new JLabel("Name:");
		panel.add(lblInfoCandidateName);
		
		panel.add(Box.createRigidArea(new Dimension(5,0)));
		
		tfCandidateName = new JTextField();
		panel.add(tfCandidateName);
		tfCandidateName.setColumns(10);
		tfCandidateName.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				checkAndActivateBtnRename();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				checkAndActivateBtnRename();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				checkAndActivateBtnRename();
			}
		});

		panel.add(Box.createRigidArea(new Dimension(5,0)));
		
		btnRenameCandidate = new JButton("Rename Candidate");
		btnRenameCandidate.setEnabled(false);
		btnRenameCandidate.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				int selectedIndex = comboCandidates.getSelectedIndex();
				if(!checkCandidateName()) {
					JOptionPane.showMessageDialog(null, "Schema name should only contains alphanumerical characters only (e.g. A-Z, a-z, 0-9)");
					return;
				}
				
				int response = showMessageYesNo("Are you sure you want to rename global schema candidate: '"+
						comboCandidates.getItemAt(selectedIndex).toString() +"' to '"+tfCandidateName.getText()+"'?", 
						"Renaming global schema candidate");
				if(response==RESPONSE_YES) {
					SchemaBuilderUtil.renameGlobalSchema(selectedIndex, tfCandidateName.getText());
					fillListCandidate();
					comboCandidates.setSelectedIndex(selectedIndex);
				}
			}
		});
		panel.add(btnRenameCandidate);
		
		panel.add(Box.createRigidArea(new Dimension(3,0)));
		
		
		scrollPaneTableGlobalSchema = new JScrollPane();
		panel_2.add(scrollPaneTableGlobalSchema);

		initTableGlobalSchema();
		scrollPaneTableGlobalSchema.setViewportView(tableGlobalSchema);
		tableGlobalSchema.setAutoResizeMode(JTable.AUTO_RESIZE_OFF);
		tableGlobalSchema.getTableHeader().setReorderingAllowed(false);
		tableGlobalSchema.getTableHeader().setResizingAllowed(false);
		renderButtonTable(tableGlobalSchema);
		
		panel_4 = new JPanel();
		tabbedPane.addTab("Text View", null, panel_4, null);
		panel_4.setLayout(new BoxLayout(panel_4, BoxLayout.Y_AXIS));
		
		scrollPaneTextAreaGlobalSchema = new JScrollPane();
		panel_4.add(scrollPaneTextAreaGlobalSchema);
		
		textAreaSchemaEditor = new JTextArea();
		textAreaSchemaEditor.setEditable(false);
		scrollPaneTextAreaGlobalSchema.setViewportView(textAreaSchemaEditor);
		textAreaSchemaEditor.getDocument().addDocumentListener(new DocumentListener() {
			
			@Override
			public void removeUpdate(DocumentEvent e) {
				checkAndActivateBtnSaveSchema();
			}
			
			@Override
			public void insertUpdate(DocumentEvent e) {
				checkAndActivateBtnSaveSchema();
			}
			
			@Override
			public void changedUpdate(DocumentEvent e) {
				checkAndActivateBtnSaveSchema();
			}
		});
		
		
/*		btnSaveCandidate = new JButton("Save Candidate");
		btnSaveCandidate.addMouseListener(new MouseAdapter() {
			@Override
			public void mouseClicked(MouseEvent e) {
				SchemaBuilderUtil.schemaValidator(textAreaSchemaEditor.getText());
			}
		});
		btnSaveCandidate.setEnabled(false);
		btnSaveCandidate.setAlignmentX(Component.RIGHT_ALIGNMENT);
		panel_4.add(btnSaveCandidate);*/
		
		
		btnNewButton = new JButton("Save Global Schemas");
		btnNewButton.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				String schemaFile = "";
				boolean fileValid = false;
				String[] schemaSplits = null;
				File f = null;
				while(!fileValid) {
					/*
					 * (null, "Please enter new quantity", "Please enter new quantity", JOptionPane.QUESTION_MESSAGE,null,null,"default text"
					 */
					schemaFile = (String) JOptionPane.showInputDialog(null,"Please enter your schema file name (ended with .schema, e.g. 'example.schema')",
							"Saving Global Schemas File",JOptionPane.QUESTION_MESSAGE,null,null,schemaFile);
					fileValid=true;
					if(schemaFile == null)
						fileValid = false;
					if(fileValid&&!schemaFile.contains(".")) {
						fileValid=false;
						JOptionPane.showMessageDialog(null, "Please input '.' as separator for your file's name and extension");
					}else
						schemaSplits = schemaFile.split("\\.");
					
					if(fileValid&&schemaSplits[0].equals("")){
						fileValid=false;
						JOptionPane.showMessageDialog(null, "Please input a file name String");
					}
					
					if(fileValid&&!schemaSplits[schemaSplits.length-1].equals("schema")){
						fileValid=false;
						JOptionPane.showMessageDialog(null, "Please input '.schema' for your file's extension");
					}else
						f = new File(schemaFile);
					
					if(fileValid)
					try {
					       f.getCanonicalPath();
					       if(f.exists()) {
								JOptionPane.showMessageDialog(null, "File already exist!");
					    	   fileValid = false;
					    			   
					       }
				    }
					    catch (IOException e1) {
					    	JOptionPane.showMessageDialog(null, e1.toString());
					    	fileValid = false;
				    }
				}
				if(SchemaBuilderUtil.saveAllGlobalSchema("conf/"+schemaFile))
			    	JOptionPane.showMessageDialog(null,"File Saved Successfully!");
				else

			    	JOptionPane.showMessageDialog(null,"Uh-oh! There's a problem, we will fix it as soon as possible");
			}
		});
		btnNewButton.setAlignmentX(Component.CENTER_ALIGNMENT);
		panelSelectedSchemaBuilding.add(btnNewButton);
		
	}
	
/* Initialization and UI Rendering Area*/
//---------------------Tree Area------------------------------//
	private void initTree() {
		DefaultMutableTreeNode root = new DefaultMutableTreeNode("Datastores");
		tree = new JTree();
		
		addListenerTreeNodeSelected();
		addListenerTreeDoubleClicked();
		populateTree(root);
		
		tree.setModel(new DefaultTreeModel(root));
		tree.setAutoscrolls(true);
	}
	
	private void addListenerTreeNodeSelected() {
		tree.addTreeSelectionListener(new TreeSelectionListener() {
			public void valueChanged(TreeSelectionEvent e) {
				DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                        tree.getLastSelectedPathComponent();
				if(node.isRoot()){
					tableDatastoreDetails.setModel(createEmptyDsSchemaTable());
					deactivateBtnAddToSchema();
				}
				else if(node.isLeaf()){
					viewTableRefreshSchema(node);
					if(isSelectedSchema(node))
						deactivateBtnAddToSchema();
					else
						activateBtnAddToSchema();
				}
				else{
					viewTableRefreshDatastore(node);
					deactivateBtnAddToSchema();
				}
					
			}
		});
	}
	private void addListenerTreeDoubleClicked() {
		tree.addMouseListener(new MouseAdapter() {
			@Override
			public void mousePressed(MouseEvent e) {
				if(e.getSource().equals(tree)) {
					if(e.getClickCount()==2) {
						DefaultMutableTreeNode node = (DefaultMutableTreeNode)
		                        tree.getLastSelectedPathComponent();
						if(node.isLeaf()&&btnAddToSchema.isEnabled())
						{
							addSchemaToSelected(node);
							btnAddToSchema.setEnabled(false);
						}
					}
				}
			}
		});
	}
	private void populateTree(DefaultMutableTreeNode root) {
		for(String datastoreId:DatastoreUtil.getAllDatastoreID()){
			DefaultMutableTreeNode dsNode = createDatastoreNodeToRoot(root,datastoreId);
			
			List<String> tableList = SchemaBuilderUtil.getTablesFromDatastore(datastoreId);
			for(String table:tableList)
				dsNode.add(new DefaultMutableTreeNode(table));
			
		}
		
	}
	private DefaultMutableTreeNode createDatastoreNodeToRoot(DefaultMutableTreeNode root, String dsId) {
		String nodeTitle = dsId + " [" + DatastoreUtil.getDatastoreName(dsId) +  "]:" + 
				DatastoreUtil.getDbName(dsId) + "@" + DatastoreUtil.getAddress(dsId);
		DefaultMutableTreeNode dsNode = new DefaultMutableTreeNode(nodeTitle);
		
		root.add(dsNode);
		return dsNode;
	}
		

//------------Data store details area-------------------------//
	private void initTableDatastoreDetails() {
		tableDatastoreDetails = new JTable();
		tableDatastoreDetails.setModel(createEmptyDsSchemaTable());
	}

	private DefaultTableModel createEmptyDsSchemaTable() {
		DefaultTableModel dModel = new DefaultTableModel(new Object[][] {},new String[] {"Properties", "Details"}) {


			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		return dModel;
	}
	
	
	
	private void initTableSelectedSchema() {
		tableSelectedSchema = new JTable();
		tableSelectedSchema.setRowSelectionAllowed(false);
		tableSelectedSchema.getTableHeader().setReorderingAllowed(false);
		tableSelectedSchema.getTableHeader().setResizingAllowed(false);
		DefaultTableModel selectedSchemaTModel = createEmptySelectedSchemaTable();
		tableSelectedSchema.setModel(selectedSchemaTModel);
	}
	
	private void initTableGlobalSchema() {
		tableGlobalSchema = new JTable();
		tableGlobalSchema.setRowSelectionAllowed(false);
		DefaultTableModel globalSchemaTModel = createEmptyGlobalSchemaTable();
		tableGlobalSchema.setModel(globalSchemaTModel);
	}

	
	private DefaultTableModel createEmptySelectedSchemaTable() {
		DefaultTableModel dModel = new DefaultTableModel(new Object[][] {},new String[] {"Schema", "Action"}) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return column==BUTTON_COLINDEX;
			}
		};
		dModel.addTableModelListener(new TableModelListener() {
			
			@Override
			public void tableChanged(TableModelEvent e) {
				DefaultTableModel dModelEvent = (DefaultTableModel) e.getSource();
				
				if(dModelEvent.getRowCount()<MIN_SCHEMA_COMBINED)
					btnBuildGlobalSchema.setEnabled(false);
				else
					btnBuildGlobalSchema.setEnabled(true);
				
				String info = lblInfoSelectedSchema.getText();
				String[] infoSplits = info.split("[0-9]+");
				lblInfoSelectedSchema.setText(infoSplits[0]+(dModelEvent.getRowCount()));
				
			}
		});
		return dModel;
	}
	

	private DefaultTableModel createEmptyGlobalSchemaTable() {
		DefaultTableModel dModel = new DefaultTableModel( new Object[][] {},
				new String[] {"Global Schema Properties","Action"}) {
			
			@Override
			public boolean isCellEditable(int row, int column) {
				return column==BUTTON_COLINDEX;
			}
		};
	return dModel;
	}
	

	private void deactivateBtnAddToSchema() {
		btnAddToSchema.setEnabled(false);
	}
	
	private void activateBtnAddToSchema() {
		btnAddToSchema.setEnabled(true);
	}
	
	
	private boolean isSelectedSchema(DefaultMutableTreeNode selectedNode) {
		DefaultTableModel selectedSchemaTableModel = (DefaultTableModel) tableSelectedSchema.getModel();
		int rowCount = selectedSchemaTableModel.getRowCount();
		
		String schemaId = createSchemaId(selectedNode);
		
		for(int i=0;i<rowCount;i++)
			if(selectedSchemaTableModel.getValueAt(i, CONTENT_COLINDEX).toString().equals(schemaId))
				return true;
		return false;
	}
	
	private List<String> retrieveSelectedSchemas(){
		DefaultTableModel dModel = (DefaultTableModel) tableSelectedSchema.getModel();
		ArrayList<String> selectedSchemaIds = new ArrayList<String>();
		for(int i=0;i<dModel.getRowCount();i++)
			selectedSchemaIds.add(dModel.getValueAt(i, CONTENT_COLINDEX ).toString());
		return selectedSchemaIds;
		
	}
	
	private void fillListCandidate() {
		List<String> candidateIdList = SchemaBuilderUtil.getAllCandidateId();
		
		DefaultComboBoxModel<String> cbModel  = new DefaultComboBoxModel<String>();
		if(candidateIdList!=null) {
			for(String s:candidateIdList)
				cbModel.addElement(s);
			btnRemoveCandidate.setEnabled(true);
		}

		comboCandidates.setModel(cbModel);
		
		viewTableRefreshGlobalSchema();
		renderButtonTable(tableGlobalSchema);
		viewTextFieldRefreshRename();

	}
	
	private void addSchemaToSelected() {
		DefaultMutableTreeNode node = (DefaultMutableTreeNode)
                tree.getLastSelectedPathComponent();
		addSchemaToSelected(node);

	}
	
	private void addSchemaToSelected(DefaultMutableTreeNode node) {
		DefaultTableModel dModel = (DefaultTableModel) tableSelectedSchema.getModel();
		dModel.addRow(new Object[] {createSchemaId(node),"Remove"});
		tableSelectedSchema.setModel(dModel);

		renderButtonTable(tableSelectedSchema);
	}
	
	private void renderButtonTable(JTable renderedTable) {

		
		renderedTable.getColumn("Action").setCellRenderer(new ButtonRenderer());
		renderedTable.getColumn("Action").setCellEditor(
	        new ButtonEditor(new JCheckBox()));

		resizeTableTwoColumnsWithButton(renderedTable);
	}
	

	private void viewTableRefreshDatastore(DefaultMutableTreeNode selectedNode) {
		String fullTitle = (String)selectedNode.getUserObject();
		String[] details = fullTitle.split("\\p{Punct}");
		
		
		DefaultTableModel tModel = createEmptyDsSchemaTable();
		tModel.addRow(new Object[]{"Datastore ID",details[DS_ID_INDEX]});
		tModel.addRow(new Object[]{"Datastore Name",details[DS_NAME_INDEX]});
		tModel.addRow(new Object[]{"Database Type",details[DS_TYPE_INDEX]});
		tModel.addRow(new Object[]{"Address",details[DS_ADDR1_INDEX]+"."+details[DS_ADDR2_INDEX]+"."+
		details[DS_ADDR3_INDEX]+"."+details[DS_ADDR4_INDEX]});
		tableDatastoreDetails.setModel(tModel);
	}
	
	private void viewTableRefreshSchema(DefaultMutableTreeNode selectedNode) {
		DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode)selectedNode.getParent();
		String parentFullTitle = (String) parentNode.getUserObject();
		String[] parentDetails = parentFullTitle.split("\\p{Punct}");
		String dsId = parentDetails[DS_ID_INDEX].trim();
		
		DefaultTableModel tModel = createEmptyDsSchemaTable();
		
		Map<String,String> schemaDetails = SchemaBuilderUtil.getSchemaDetails(dsId, (String)selectedNode.getUserObject());
		boolean isAfterColumn = false;
		for(String key:schemaDetails.keySet()) {
			if(!isAfterColumn)
				tModel.addRow(new Object[]{key,schemaDetails.get(key)});
			else
				tModel.addRow(new Object[]{"-"+key,schemaDetails.get(key)});
				
			if(key=="Columns:")
				isAfterColumn = true;
		}
		tableDatastoreDetails.setModel(tModel);
	}
	
	private void viewTableRefreshGlobalSchema() {		
		DefaultTableModel dModel = createEmptyGlobalSchemaTable();
		
		if(comboCandidates.getSelectedItem()!=null) {
			String selectedCandidateId = comboCandidates.getSelectedItem().toString();
			List<String> rowContents = SchemaBuilderUtil.getCandidateDetails(selectedCandidateId);
			for(String s:rowContents)
				if((s.startsWith("+")||s.startsWith("-"))&&!s.contains("related"))
					dModel.addRow(new Object[] {s,"Remove"});
				else
					dModel.addRow(new Object[] {s,null});
		}
		tableGlobalSchema.setModel(dModel);
		renderButtonTable(tableGlobalSchema);
	}
	
	private void viewTextFieldRefreshRename() {
		tfCandidateName.setText("");
		if(comboCandidates.getSelectedItem()!=null) {
			tfCandidateName.setText(comboCandidates.getSelectedItem().toString());
		}
	}

	private void loadTextBasedEditor() {
		if(comboCandidates.getSelectedItem()==null)
			return;
		String schemaText = SchemaBuilderUtil.getTextBasedSchema(comboCandidates.getSelectedItem().toString());
		textAreaSchemaEditor.setText(schemaText); 
		textAreaSchemaEditor.setEnabled(true);
		
	}
	
	private void checkAndActivateBtnSaveSchema() {
		//if(!SchemaBuilderUtil.checkTextContentSame(textAreaSchemaEditor.getText(), comboCandidates.getSelectedItem().toString()))
		//	btnSaveCandidate.setEnabled(true);
	}
	
	private void loadTableBasedEditor() {
		
	}
	
	
/*-------------------------- UI Functionalities area*/
	private String createSchemaId(DefaultMutableTreeNode selectedNode) {
		if(!selectedNode.isLeaf())
			return "";
		DefaultMutableTreeNode parentNode = (DefaultMutableTreeNode)selectedNode.getParent();
		String parentFullTitle = (String) parentNode.getUserObject();
		String[] parentDetails = parentFullTitle.split("\\p{Punct}");
		String dsId = parentDetails[DS_ID_INDEX].trim();
		String dsName = parentDetails[DS_NAME_INDEX];
		String tableName = (String)selectedNode.getUserObject();
		
		return dsId+"-"+dsName+":"+tableName;
	}
	private boolean isRemovedSchemaSelectedNodeTree(String schemaIdOnSelectedSchemaTable) {
		
		String schemaIdOnSelectedNodeTree = createSchemaId((DefaultMutableTreeNode) tree.getLastSelectedPathComponent());
		if(schemaIdOnSelectedNodeTree.equals(""))
			return false;
		return schemaIdOnSelectedNodeTree.equals(schemaIdOnSelectedSchemaTable);
	}
	private int showMessageYesNo(String question, String title) {
		int response = JOptionPane.showOptionDialog(null, question, title, 
				JOptionPane.PLAIN_MESSAGE, JOptionPane.QUESTION_MESSAGE, null, new Object[]{"Yes","No"}, 
				"Yes");
		return response;
	}
	private void checkAndActivateBtnRename() {
		if(tfCandidateName.getText().equals(comboCandidates.getSelectedItem().toString()))
			btnRenameCandidate.setEnabled(false);
		else
			btnRenameCandidate.setEnabled(true);
		
	}
	private boolean checkCandidateName() {
		if(tfCandidateName.getText().contains(" ")||tfCandidateName.getText().matches(".*\\p{Punct}.*"))
			return false;
		return true;
	}
	
	private void resizeTableTwoColumnsWithButton(JTable table) {
		JViewport viewport = (JViewport)table.getParent();
		int tableWidth = viewport.getWidth() - 16;
		
		TableColumnModel columnModel = table.getColumnModel();
		TableColumn columnContent = columnModel.getColumn(CONTENT_COLINDEX);
		TableColumn columnButton = columnModel.getColumn(BUTTON_COLINDEX);
		float columnContentPercentWidth = 0.75f;
		float columnButtonPercentWidth = 0.25f;
		columnContent.setPreferredWidth(Math.round(columnContentPercentWidth*tableWidth));
		columnButton.setPreferredWidth(Math.round(columnButtonPercentWidth*tableWidth));	
	}
	
	class ButtonRenderer extends JButton implements TableCellRenderer {

		  public ButtonRenderer() {
			  setOpaque(true);
			  }

		  public Component getTableCellRendererComponent(JTable table, Object value,
				  boolean isSelected, boolean hasFocus, int row, int column) {
			  if(value==null)
				  return null;
			  
			  if(isSelected){
				  setForeground(table.getSelectionForeground());
				  setBackground(table.getSelectionBackground());
				  } 
			  else{
				  setForeground(table.getForeground());
				  setBackground(UIManager.getColor("Button.background"));
				  }
			  
			  setText((value == null) ? "" : value.toString());
			  
			  return this;
		  }
	}
	
	class ButtonEditor extends DefaultCellEditor {
		  protected JButton button;

		  private JTable table;
		  private int row;
		  private String label;

		  public ButtonEditor(JCheckBox checkBox) {
			  super(checkBox);
			  button = new JButton();
			  button.setOpaque(true);
			  button.addActionListener(new ActionListener() {
				  public void actionPerformed(ActionEvent e) {
					  fireEditingStopped();
					  }
				  });
			  }

		  public Component getTableCellEditorComponent(JTable table, Object value,
				  boolean isSelected, int row, int column) {
			  if(value==null)
				  return null;
			  
			  if (isSelected) {
			      button.setForeground(table.getSelectionForeground());
			      button.setBackground(table.getSelectionBackground());
			  } else {
			      button.setForeground(table.getForeground());
			      button.setBackground(table.getBackground());
			  }
			  label = (value == null) ? "" : value.toString();
			  
			  
			 
			  button.setText(label);
			  this.row = row;
			  this.table = table;
			  
			  return button;
		  }

		  public Object getCellEditorValue() {
			  return new String(label);
		  }

		  public boolean stopCellEditing() {
		    return super.stopCellEditing();
		  }

		  protected void fireEditingStopped() {
			  super.fireEditingStopped();
			  if(label.equals("Remove")){
		    	  DefaultTableModel dModel = (DefaultTableModel) table.getModel();
		    	  String removedRowContent = dModel.getValueAt(row, CONTENT_COLINDEX).toString();
		    	  
		    	  if(table.equals(tableSelectedSchema))
		    		  if(isRemovedSchemaSelectedNodeTree(removedRowContent))
		    			  activateBtnAddToSchema();
		    	  if(table.equals(tableGlobalSchema)){
		    		  SchemaBuilderUtil.removeColumnFromSchema(comboCandidates.getSelectedItem().toString(), removedRowContent);
		    		  //viewTableRefreshGlobalSchema();
		    	  }
		    	  
		    	  dModel.removeRow(row);
		    	  table.setModel(dModel);
	    		  renderButtonTable(tableGlobalSchema);
			  }
		}
	}
}

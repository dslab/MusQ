package integrator.gui;

import java.awt.BorderLayout;
import java.awt.EventQueue;

import javax.swing.JFrame;
import javax.swing.JPanel;
import javax.swing.border.EmptyBorder;
import javax.swing.JScrollPane;
import java.awt.FlowLayout;
import javax.swing.BoxLayout;
import javax.swing.JButton;
import java.awt.Component;
import java.awt.Dimension;

import com.jgoodies.forms.layout.FormLayout;
import com.jgoodies.forms.layout.ColumnSpec;
import com.jgoodies.forms.layout.RowSpec;

import data.schema.Datastore;
import integrator.config.InstanceLevel;
import integrator.config.IntegratorUtil;
import utils.DatastoreUtil;

import javax.swing.JTextField;
import com.jgoodies.forms.layout.FormSpecs;
import java.awt.GridBagLayout;
import java.awt.GridBagConstraints;
import javax.swing.JLabel;
import javax.swing.JOptionPane;
import javax.swing.GroupLayout;
import javax.swing.ImageIcon;
import javax.swing.GroupLayout.Alignment;
import java.awt.Insets;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.swing.JSeparator;
import javax.swing.SwingConstants;
import javax.swing.SwingUtilities;
import javax.swing.JComboBox;
import javax.swing.JPasswordField;
import java.awt.Font;
import java.awt.event.ActionListener;
import java.awt.event.ActionEvent;

public class IntegratorDsConfigGui extends JFrame {

	/**
	 * 
	 */
	private static final long serialVersionUID = 1L;

	private JPanel contentPane;

	private JPanel configPane;
	private ArrayList<JPanel> configList;
	private ArrayList<String> configNameList;
	private List<Map<String,String>> datastoreConfigList;
	
	/**
	 * Launch the application.
	 */

	/**
	 * Create the frame.
	 */
	public IntegratorDsConfigGui() {
		

		DatastoreUtil.init();
		IntegratorUtil.readConfig();
		
		setTitle("MusQ - Datastore Configuration");
		setDefaultCloseOperation(JFrame.DISPOSE_ON_CLOSE);
		setBounds(400, 200, 450, 505);
		contentPane = new JPanel();
		contentPane.setBorder(new EmptyBorder(5, 5, 5, 5));
		setContentPane(contentPane);
		contentPane.setLayout(new BorderLayout(0, 0));

		setIconImage(new ImageIcon("img/Logo2.jpg").getImage());
		JLabel lblInfoTitle = new JLabel("Datastore Configuration");
		contentPane.add(lblInfoTitle, BorderLayout.NORTH);
		
		JPanel panel = new JPanel();
		contentPane.add(panel, BorderLayout.SOUTH);
		panel.setLayout(new FlowLayout(FlowLayout.RIGHT, 5, 5));
		
		JButton btnAddDs = new JButton("Add Datastore");
		btnAddDs.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				addNewDsConfig();
				
			}
		});
		
		panel.add(btnAddDs);
		
		JButton btnReset = new JButton("Reset to Default");
		btnReset.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				initLoad();
			}
		});
		panel.add(btnReset);
		
		JButton btnBuild = new JButton("Build Schema");
		btnBuild.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				saveConfig();

				new IntegratorDsMatcherGui().setVisible(true);;
				dispose();
			}
		});
		panel.add(btnBuild);
		
		JScrollPane scrollPane_1 = new JScrollPane();
		contentPane.add(scrollPane_1, BorderLayout.CENTER);
		
		JPanel panelConf = new JPanel();
		scrollPane_1.setViewportView(panelConf);
		panelConf.setLayout(new BoxLayout(panelConf, BoxLayout.Y_AXIS));
		
		configPane = panelConf;
		initLoad();
	}
	
	private void addDsConfig(String s) {
		
		JPanel panel = new JPanel();
		panel.setAlignmentY(Component.TOP_ALIGNMENT);
		configPane.add(panel);
		GridBagLayout gbl_panel = new GridBagLayout();
		gbl_panel.columnWidths = new int[]{0, 0, 0};
		gbl_panel.rowHeights = new int[]{0, 0, 0, 0, 0, 0, 0, 0, 0, 0};
		gbl_panel.columnWeights = new double[]{0.0, 1.0,  Double.MIN_VALUE};
		gbl_panel.rowWeights = new double[]{0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, 0.0, Double.MIN_VALUE};
		panel.setLayout(gbl_panel);
		
		JLabel label = new JLabel("Config "+s);
		label.setFont(new Font("Gulim", Font.BOLD, 12));
		GridBagConstraints gbc_label = new GridBagConstraints();
		gbc_label.anchor = GridBagConstraints.WEST;
		gbc_label.gridwidth = 3;
		gbc_label.insets = new Insets(0, 0, 5, 5);
		gbc_label.gridx = 0;
		gbc_label.gridy = 0;
		label.setName("label");
		panel.add(label, gbc_label);
		
		JLabel lblDatabaseName = new JLabel("Datastore Name:");
		GridBagConstraints gbc_lblDatabaseName = new GridBagConstraints();
		gbc_lblDatabaseName.anchor = GridBagConstraints.WEST;
		gbc_lblDatabaseName.insets = new Insets(0, 5, 5, 5);
		gbc_lblDatabaseName.gridx = 0;
		gbc_lblDatabaseName.gridy = 2;
		lblDatabaseName.setName("lblDatabaseName");
		panel.add(lblDatabaseName, gbc_lblDatabaseName);
		
		JTextField textFieldDatabaseName = new JTextField();
		GridBagConstraints gbc_textFieldDatabaseName = new GridBagConstraints();
		gbc_textFieldDatabaseName.insets = new Insets(0, 5, 5, 0);
		gbc_textFieldDatabaseName.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldDatabaseName.gridx = 1;
		gbc_textFieldDatabaseName.gridy = 2;
		panel.add(textFieldDatabaseName, gbc_textFieldDatabaseName);
		textFieldDatabaseName.setName("textFieldDatabaseName");
		textFieldDatabaseName.setColumns(10);
		
		JLabel lblAddress = new JLabel("Address:");
		GridBagConstraints gbc_lblAddress = new GridBagConstraints();
		gbc_lblAddress.anchor = GridBagConstraints.WEST;
		gbc_lblAddress.insets = new Insets(0, 5, 5, 5);
		gbc_lblAddress.gridx = 0;
		gbc_lblAddress.gridy = 3;
		lblAddress.setName("lblAddress");
		panel.add(lblAddress, gbc_lblAddress);
		
		JTextField textFieldAddress = new JTextField();
		GridBagConstraints gbc_textFieldAddress = new GridBagConstraints();
		gbc_textFieldAddress.insets = new Insets(0, 5, 5, 0);
		gbc_textFieldAddress.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldAddress.gridx = 1;
		gbc_textFieldAddress.gridy = 3;
		panel.add(textFieldAddress, gbc_textFieldAddress);
		textFieldAddress.setName("textFieldAddress");
		textFieldAddress.setColumns(10);
		
		JLabel lblDatabaseType = new JLabel("Database Type:");
		GridBagConstraints gbc_lblDatabaseType = new GridBagConstraints();
		gbc_lblDatabaseType.anchor = GridBagConstraints.WEST;
		gbc_lblDatabaseType.insets = new Insets(0, 5, 5, 5);
		gbc_lblDatabaseType.gridx = 0;
		gbc_lblDatabaseType.gridy = 4;
		lblDatabaseType.setName("lblDatabaseType");
		panel.add(lblDatabaseType, gbc_lblDatabaseType);
		
		JComboBox comboBoxDatabaseType = new JComboBox();
		for(String dsType:DatastoreUtil.datastoreTypes)
			comboBoxDatabaseType.addItem(dsType);
		
		GridBagConstraints gbc_comboBoxDatabaseType = new GridBagConstraints();
		gbc_comboBoxDatabaseType.insets = new Insets(0, 5, 5, 0);
		gbc_comboBoxDatabaseType.fill = GridBagConstraints.HORIZONTAL;
		gbc_comboBoxDatabaseType.gridx = 1;
		gbc_comboBoxDatabaseType.gridy = 4;
		comboBoxDatabaseType.setName("comboBoxDatabaseType");
		panel.add(comboBoxDatabaseType, gbc_comboBoxDatabaseType);
		
		JLabel lblUsername = new JLabel("Username:");
		GridBagConstraints gbc_lblUsername = new GridBagConstraints();
		gbc_lblUsername.anchor = GridBagConstraints.WEST;
		gbc_lblUsername.insets = new Insets(0, 5, 5, 5);
		gbc_lblUsername.gridx = 0;
		gbc_lblUsername.gridy = 5;
		lblUsername.setName("lblUsername");
		panel.add(lblUsername, gbc_lblUsername);
		
		JTextField textFieldUsername = new JTextField();
		GridBagConstraints gbc_textFieldUsername = new GridBagConstraints();
		gbc_textFieldUsername.insets = new Insets(0, 5, 5, 0);
		gbc_textFieldUsername.fill = GridBagConstraints.HORIZONTAL;
		gbc_textFieldUsername.gridx = 1;
		gbc_textFieldUsername.gridy = 5;
		panel.add(textFieldUsername, gbc_textFieldUsername);
		textFieldUsername.setName("textFieldUsername");
		textFieldUsername.setColumns(10);
		
		JLabel lblPassword = new JLabel("Password:");
		GridBagConstraints gbc_lblPassword = new GridBagConstraints();
		gbc_lblPassword.anchor = GridBagConstraints.WEST;
		gbc_lblPassword.insets = new Insets(0, 5, 5, 5);
		gbc_lblPassword.gridx = 0;
		gbc_lblPassword.gridy = 6;
		lblPassword.setName("lblPassword");
		panel.add(lblPassword, gbc_lblPassword);
		
		JPasswordField passwordFieldPassword = new JPasswordField();
		GridBagConstraints gbc_passwordFieldPassword = new GridBagConstraints();
		gbc_passwordFieldPassword.insets = new Insets(0, 5, 5, 0);
		gbc_passwordFieldPassword.fill = GridBagConstraints.HORIZONTAL;
		gbc_passwordFieldPassword.gridx = 1;
		gbc_passwordFieldPassword.gridy = 6;
		passwordFieldPassword.setName("passwordFieldPassword");
		panel.add(passwordFieldPassword, gbc_passwordFieldPassword);
		
		JButton btnDelete = new JButton("Delete");
		GridBagConstraints gbc_btnDelete = new GridBagConstraints();
		gbc_btnDelete.insets = new Insets(0, 5, 5, 0);
		gbc_btnDelete.anchor = GridBagConstraints.EAST;
		gbc_btnDelete.gridx = 1;
		gbc_btnDelete.gridy = 7;
		btnDelete.setName("btnDelete");
		panel.add(btnDelete, gbc_btnDelete);
		
		btnDelete.addActionListener(new ActionListener() {
			public void actionPerformed(ActionEvent e) {
				configPane.remove(panel);
				configPane.repaint();
				configPane.revalidate();
				configList.remove(panel);
				configNameList.remove(s);
			}
		});
		
		JSeparator separator = new JSeparator();
		separator.setPreferredSize(new Dimension(100,1));
		GridBagConstraints gbc_separator = new GridBagConstraints();
		gbc_separator.gridwidth = 3;
		gbc_separator.fill = GridBagConstraints.HORIZONTAL;
		gbc_separator.insets = new Insets(0, 5, 0, 5);
		gbc_separator.gridx = 0;
		gbc_separator.gridy = 8;
		separator.setName("separator");
		panel.add(separator, gbc_separator);
		
		configList.add(panel);
	}
	
	private void initLoad() {
		configPane.removeAll();
		configList = new ArrayList<>();
		configNameList = (ArrayList<String>) DatastoreUtil.getAllDatastoreID();
		
		for(int i=0;i<configNameList.size();i++)
			addDsConfig(configNameList.get(i));
		fillFields();
		configPane.repaint();
		configPane.revalidate();
	}
	
	private void fillFields() {
		/* label--
		 * lblDatabaseName--
		 * textFieldDatabaseName
		 * lblAddress--
		 * textFieldAddress
		 * lblDatabaseType--
		 * comboBoxDatabaseType
		 * lblUsername--
		 * textFieldUsername
		 * lblPassword--
		 * passwordFieldPassword
		 * btnDelete--
		 * separator--
		 */
		for(int i=0;i<configNameList.size();i++) {
			JPanel panel = configList.get(i);
			for(Component comp:panel.getComponents()) {
				if(comp.getName()=="textFieldDatabaseName") {
					JTextField tf = (JTextField)comp;
					String text = DatastoreUtil.getDbName(configNameList.get(i));
					if(text!=null)
						tf.setText(text);
				}
				else if(comp.getName()=="textFieldAddress") {
					JTextField tf = (JTextField)comp;
					String text = DatastoreUtil.getAddress(configNameList.get(i));
					if(text!=null)
						tf.setText(text);
				}else if(comp.getName()=="comboBoxDatabaseType") {
					JComboBox cb = (JComboBox)comp;
					String text = DatastoreUtil.getDatastoreName(configNameList.get(i));
					if(text!=null)
						cb.setSelectedItem(text);
				}
				else if(comp.getName()=="textFieldUsername") {
					JTextField tf = (JTextField)comp;
					String text = DatastoreUtil.getUsername(configNameList.get(i));
					if(text!=null)
						tf.setText(text);
				}
				else if(comp.getName()=="passwordFieldPassword") {
					JPasswordField pf = (JPasswordField)comp;
					String text = DatastoreUtil.getUsername(configNameList.get(i));
					if(text!=null)
						pf.setText(text);
				}
					
			}
			
		}
		configPane.repaint();
		configPane.revalidate();
			
	}
	
	private void iterateFields() {
		datastoreConfigList = new ArrayList<Map<String,String>>();
		
		for(int i=0;i<configNameList.size();i++) {
			Map<String,String> datastoreConfig = new HashMap<String,String>();
			/*
			 * ds.setID(datastoreConfig.get("ID"));*
			 * ds.setDatabase(datastoreConfig.get("Database"));
			 * ds.setDbName(datastoreConfig.get("Name"));*
			 * ds.setAddress(datastoreConfig.get("Address"));
			 * ds.setUser(datastoreConfig.get("Username"));
			 * ds.setPass(datastoreConfig.get("Password"));
			 */
			datastoreConfig.put("ID", configNameList.get(i));
			JPanel panel = configList.get(i);
			
			for(Component comp:panel.getComponents()) {
				if(comp.getName()=="textFieldDatabaseName") {
					JTextField tf = (JTextField)comp;
					datastoreConfig.put("Name", tf.getText());
				}
				else if(comp.getName()=="textFieldAddress") {
					JTextField tf = (JTextField)comp;
					datastoreConfig.put("Address", tf.getText());
				}else if(comp.getName()=="comboBoxDatabaseType") {
					JComboBox cb = (JComboBox)comp;
					datastoreConfig.put("Database", cb.getSelectedItem().toString());
				}
				else if(comp.getName()=="textFieldUsername") {
					JTextField tf = (JTextField)comp;
					datastoreConfig.put("Username", tf.getText());
				}
				else if(comp.getName()=="passwordFieldPassword") {
					JPasswordField pf = (JPasswordField)comp;
					datastoreConfig.put("Password", pf.getText());
				}
					
			}
			String s = "";
			s = s + "ID: "+(datastoreConfig.get("ID"))+" ";
			s = s + "Database: "+(datastoreConfig.get("Database"))+" ";
			s = s + "Name: "+(datastoreConfig.get("Name"))+" ";
			s = s + "Address: "+(datastoreConfig.get("Address"))+" ";
			s = s + "Username: "+(datastoreConfig.get("Username"))+" ";
			s = s + "Password: "+(datastoreConfig.get("Password"))+" ";
			datastoreConfigList.add(datastoreConfig);
		}
	}
	
	private void saveConfig() {
		iterateFields();
		DatastoreUtil.renewDatastore(datastoreConfigList);
		DatastoreUtil.SaveConfig();
	}
	
	private void addNewDsConfig() {
		int lastNumber = Integer.parseInt(configNameList.get(configNameList.size()-1).substring(2));
		configNameList.add("DB"+(lastNumber+1));
		addDsConfig(configNameList.get(configNameList.size()-1));

		configPane.repaint();
		configPane.revalidate();
	}

}

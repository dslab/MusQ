package integrator;

import java.util.ArrayList;
import java.util.List;

public class Mapper {
	public static void printMappings(List<SimilarityScore> scorelist){
		List<String> maps = createMapping(scorelist);
		for(String map:maps) {
			System.out.println(map);
		}
	}
	
	public static List<String> createMapping(List<SimilarityScore> scorelist){
		List<String> maps = new ArrayList<String>();
		int ctr = 1;
		for(SimilarityScore sc : scorelist){
			System.out.println("> Similarity : " + sc.toString());
			String table1 = sc.getSchema1().getTable();
			String table2 = sc.getSchema2().getTable();
			
			String db1 = sc.getSchema1().getDatastore().getID();
			String db2 = sc.getSchema2().getDatastore().getID();
			
			String mapping = "";
			mapping += "M" + ctr + "(" + getCombinedColumn(sc) + ") :-\n";
			mapping += table1 + "(" + getColumns(sc.getSchema1()) + ")@" + db1 + ",\n";
			mapping += table2 + "(" + getColumns(sc.getSchema2()) + ")@" + db2 + ",\n";
			mapping += sc.getSchema1().getTable().concat(".").concat(sc.getCol1()).concat(" = ") 
			.concat(sc.getSchema2().getTable()).concat(".").concat(sc.getCol2()).concat(";");
			ctr++;
			
			maps.add(mapping);
		}
		return maps;
	}
	
	public static String createMapping(String globalSchemaId, SimilarityScore sc){

		
		String table1 = sc.getSchema1().getTable();
		String table2 = sc.getSchema2().getTable();
			
		String db1 = sc.getSchema1().getDatastore().getID();
		String db2 = sc.getSchema2().getDatastore().getID();
			
		String mapping = "";
		mapping += globalSchemaId + "(" + Mapper.getCombinedColumn(sc) + ") :-\n";
		mapping += table1 + "(" + getColumns(sc.getSchema1()) + ")@" + db1 + ",\n";
		mapping += table2 + "(" + getColumns(sc.getSchema2()) + ")@" + db2 + ",\n";
		mapping += sc.getSchema1().getTable().concat(".").concat(sc.getCol1()).concat(" = ") 
		.concat(sc.getSchema2().getTable()).concat(".").concat(sc.getCol2()).concat(";");

		
		return mapping;
	}
	
	private static String getCombinedColumn(SimilarityScore sc) {
		String cols = "";
		cols = String.join(",", sc.getSchema1().getColumns().keySet());
		for(String col : sc.getSchema2().getColumns().keySet()){
			if(!sc.getCol2().equalsIgnoreCase(col)){
				cols += "," + col;
			}
		}
		return cols;
	}

	private static String getColumns(SourceSchema schema){
		String cols = "";
//		System.out.println(schema.getColumns().keySet());
		cols = String.join(",", schema.getColumns().keySet());
		return cols;
	}
}

package integrator;

import java.text.DecimalFormat;

public class SimilarityScore implements Comparable<SimilarityScore>{
	private SourceSchema schema1;
	private SourceSchema schema2;
	private String col1;
	private String col2;
	private double simScore;
	
	public SourceSchema getSchema1() {
		return schema1;
	}
	
	public void setSchema1(SourceSchema schema1) {
		this.schema1 = schema1;
	}

	public SourceSchema getSchema2() {
		return schema2;
	}

	public void setSchema2(SourceSchema schema2) {
		this.schema2 = schema2;
	}

	public double getSimScore() {
		return simScore;
	}
	
	public String getCol1() {
		return col1;
	}

	public void setCol1(String col1) {
		this.col1 = col1;
	}

	public String getCol2() {
		return col2;
	}

	public void setCol2(String col2) {
		this.col2 = col2;
	}
	
	public void setSimScore(double simScore) {
		this.simScore = simScore;
	}
	
	public String toString(){
		DecimalFormat df = new DecimalFormat("#.#####");
		return ("( ").concat(schema1.getTable()).concat(".").concat(col1).concat(", ")
				.concat(schema2.getTable()).concat(".").concat(col2).concat(", ")
				.concat(" ").concat(df.format(simScore)).concat(" )");
	}

	@Override
	public int compareTo(SimilarityScore s) {
		return this.getSimScore() > s.getSimScore() ? -1 : (this.getSimScore() < s.getSimScore() ? 1 : 0);
	}
}

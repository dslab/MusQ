package integrator;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.Map;

import org.apache.commons.collections4.CollectionUtils;
import org.json.JSONArray;
import org.json.JSONObject;

public class InstanceMatcher{
	double threshold = 0.0;
	List<SimilarityScore> simScore = new ArrayList<SimilarityScore>();
	public List<JSONObject> datalist;
	
	public InstanceMatcher(){
		Matcher matcher = Matcher.getInstance();
		datalist = matcher.jsonList;
	}
	
	public List<SimilarityScore> match(SourceSchema S1, SourceSchema S2) {	
		double score = 0.0;
		simScore = new ArrayList<SimilarityScore>();
		
		String db1 = S1.getDatastore().getDbName();
		String table1 = S1.getTable();
		
		String db2 = S2.getDatastore().getDbName();
		String table2 = S2.getTable();
//		System.out.println(table1 + " <> " + table2);
		JSONArray data1 = getInstanceData(db1, table1);
		JSONArray data2 = getInstanceData(db2, table2);
		
		Iterator<String> keys1 = data1.getJSONObject(0).keys();
		
		while(keys1.hasNext()){
			String key1 = keys1.next().toString();
			Map<String, List<String>> str1 = new HashMap<String, List<String>>();
			List<String> val1 = new ArrayList<>();
			for(int i = 0; i < data1.length(); i++){
				val1.add(data1.getJSONObject(i).get(key1).toString());
			}
//			System.out.println(key1 + " " + val1.toString());
			str1.put(key1, val1);
			
			Iterator<String> keys2 = data2.getJSONObject(0).keys();
			while(keys2.hasNext()){
				
				String key2 = keys2.next().toString();
				Map<String, List<String>> str2 = new HashMap<String, List<String>>();
				List<String> val2 = new ArrayList<String>();
				for(int i = 0; i < data2.length(); i++){
					val2.add(data2.getJSONObject(i).get(key2).toString());
				}
//				System.out.println(key2 + " " + val2.toString());
				List<String> sim = (List<String>) CollectionUtils.retainAll(val1, val2);
				if(sim.size() > 0){
					score = sim.size() / (double) (val1.size() + val2.size());
//					double score1 = sim.size() / (double) val1.size();
//					double score2 = sim.size() / (double) val2.size();
//					System.out.println(score + " " + score1 + " " + score2);
//					System.out.println(" >> " + table1 + "." + key1 + "  " + table2 + "."+ key2 + " = " + sim.toString());
					SimilarityScore sc = new SimilarityScore();
					sc.setSchema1(S1);
					sc.setSchema2(S2);
					sc.setCol1(key1);
					sc.setCol2(key2);
					sc.setSimScore(score);
					simScore.add(sc);
//					System.out.println(sc);
					
				}
				str2.put(key2, val2);
			}
		}
		
		

		return simScore;
	}

	public List<SimilarityScore> constraintMatch(SourceSchema S1, SourceSchema S2) {
		// TODO Auto-generated method stub
		return null;
	}
	
	private JSONArray getInstanceData(String db, String table){
		for(JSONObject data: datalist){
			if(data.getJSONObject("meta").get("table").equals(table)
					&& data.getJSONObject("meta").get("dbName").equals(db)){
				return data.getJSONArray("result");
			}
		}
		return null;
	}
	
//	private String getTable(String col){
//		String table = "";
//		for(JSONObject data: datalist){
//			if(data.getJSONArray("result").getJSONObject(0).keys().equals(col)){
//				System.out.println("");
//			}
//		}
//		return table;
//	}
}

package integrator;

import integrator.config.InstanceLevel;
import integrator.config.IntegratorUtil;

import java.util.List;
import utils.DatastoreUtil;
import data.schema.Datastore;

public class SchemaConstructor {	
	public static void main(String[] args){
		DatastoreUtil.init();
		IntegratorUtil.readConfig();
		
		InstanceLevel conf = IntegratorUtil.confData.getInstanceLevel();
		boolean getInstance = false;
		if(conf.getEnabled()){
			getInstance = true;
		}
		
		//get schema from data stores
		Matcher matcher = Matcher.getInstance();
		
		
		Datastore ds;

		ds = DatastoreUtil.getDatastore("DB1");
		matcher.getCassandraSchema(ds, getInstance);
		
		ds = DatastoreUtil.getDatastore("DB2");
		matcher.getMySQLSchema(ds, getInstance);
//		

//		ds = DatastoreUtil.getDatastore("DB3");
//		matcher.getMySQLSChema(ds, getInstance);
		
//		ds = DatastoreUtil.getDatastore("DB4");
//		matcher.getHDFSSchema(ds, getInstance);
//		
//		ds = DatastoreUtil.getDatastore("DB6");
//		matcher.getMySQLSChema(ds, getInstance);
		
//
//		ds = DatastoreUtil.getDatastore("DB5");
//		matcher.getMongoSchema(ds, getInstance);
		
		List<SourceSchema> schemas = matcher.combineSchema();
		List<SimilarityScore> combinedScore = matcher.calculate(schemas);
		
		Mapper.printMappings(combinedScore);
	}
}
